package com.appmobileos.freetoiletsrussiatrial.map;

/**
 * The class define name city from user location.
 * Task starting and run every 3 seconds (timeout second floor).
 * Task will run 5 attempts, if result null show dialog NO_CITY
 * 
 * 
 * */
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import com.appmobileos.freetoiletsrussiatrial.MainInfoDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

public class DefinitionCityUserSingleton {
	private static DefinitionCityUserSingleton instance;
	private Location mLocation;
	private Geocoder geocoder;
	private String cityName = null;
	private int numberOfAttempts = 0;
	private Timer mTimer;
	private Context mContext;
	private static final int ATTEMPTS = 8;
	private boolean visibleDialog = false;

	private DefinitionCityUserSingleton() {

	}

	public synchronized static DefinitionCityUserSingleton getInstance() {
		if (instance == null)
			instance = new DefinitionCityUserSingleton();
		return instance;
	}

	public void setUserLocation(Location userLocation) {
		this.mLocation = userLocation;
	}

	public void setNumberAttempts(int attemps) {
		numberOfAttempts = attemps;
	}

	public void initGeocoder() {
		if(geocoder==null)
		geocoder = new Geocoder(mContext, Locale.getDefault());

	}

	public void setContext(Context mContext) {
		this.mContext = mContext;
	}

	public void disableTimer() {
		if (mTimer != null)
			mTimer.cancel();
	}

	public boolean isVisibleDialog() {
		return visibleDialog;
	}

	public void startTimer() {
		mTimer = new Timer();
		mTimer.schedule(new GeocoderTask(), 500, 1000);
	}

	public boolean isDisableTimer() {
		if (mTimer != null)
			return true;
		return false;
	}
	public void setVisibleDialog(boolean visible){
		visibleDialog = visible;
	}

	private class GeocoderTask extends TimerTask {

		@Override
		public void run() {
			numberOfAttempts++;
			if (mLocation == null && numberOfAttempts < ATTEMPTS)
				return;
			if (mLocation != null) {
				double latitude = mLocation.getLatitude();
				double longitude = mLocation.getLongitude();
				List<Address> addresses = null;
				try {
					addresses = geocoder
							.getFromLocation(latitude, longitude, 1);
					if (addresses.size() > 0) {
						cityName = addresses.get(0).getLocality();

					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (mTimer != null)
				mTimer.cancel();
			setPreferenceFindCity(cityName);
		}

	}

	private String translateCity(String cityNamelocal) {
		String rusCityName = cityNamelocal;
		if (cityNamelocal.equals("Moscow")) {
			rusCityName = "Москва";
		} else if (cityNamelocal.equals("Saint-Petersburg")) {
			rusCityName = "Санкт-Петербург";
		}
		return rusCityName;

	}

	private void showResultDialog(int idDialog, String dinamicMessage) {
		MainInfoDialog info = MainInfoDialog.newInstance(idDialog,
				dinamicMessage);
		if ((GoogleMaps2Activity) mContext != null)
		info.show(((GoogleMaps2Activity) mContext).getSupportFragmentManager(),
				MainInfoDialog.DIALOG_TAG);
		visibleDialog = true;

	}

	public void setPreferenceFindCity(String nameCity) {
		if (nameCity == null) {
			showResultDialog(MainInfoDialog.NO_FIND_CITY, nameCity);

		} else {
			showResultDialog(MainInfoDialog.FIND_CITY, translateCity(nameCity));
		}
	}

}
