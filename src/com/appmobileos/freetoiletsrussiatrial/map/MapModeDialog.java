package com.appmobileos.freetoiletsrussiatrial.map;

import com.appmobileos.freetoiletsrussiatrial.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class MapModeDialog extends DialogFragment implements OnClickListener {
	public static final String KEY_DIALOG_MAP = "key_dialog_map";

	public static MapModeDialog newInstance(int selectMode) {
		MapModeDialog mode = new MapModeDialog();
		Bundle args = new Bundle();
		args.putInt(KEY_DIALOG_MAP, selectMode);
		mode.setArguments(args);
		return mode;

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle(R.string.dialog_title_map_type);
		String[] mapType = {getString(R.string.map_normal),
				getString(R.string.map_satellite),
				getString(R.string.map_terrain) };
		int selectTypeMap = getArguments().getInt(KEY_DIALOG_MAP);
		dialog.setSingleChoiceItems(mapType, selectTypeMap-1, this);

		return dialog.create();
	}
    
	@Override
	public void onClick(DialogInterface dialog, int item) {
		String typeMap = getString(R.string.map_normal);
		if (item == 1) {
			typeMap = getString(R.string.map_satellite);
		} else if (item ==2) {
			typeMap = getString(R.string.map_terrain);
		}
		((GoogleMaps2Activity) getActivity()).setLayer(typeMap);
		dismiss();

	}

}
