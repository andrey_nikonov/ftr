package com.appmobileos.freetoiletsrussiatrial.map;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.appmobileos.freetoiletsrussiatrial.Constants;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerParcelable;
import com.appmobileos.freetoiletsrussiatrial.preferences.Identificator;
import com.appmobileos.freetoiletsrussiatrial.utils.InternetUtils;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import stixiya.freetoilets.modal.PostAction;

import static com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils.parseJson2;

/**
 * Created by andrey on 17/09/13.
 */
public class MarkersIntentService extends IntentService {
    private ArrayList<MarkerParcelable> mMarkers = null;
    private ArrayList<MarkerParcelable> mMarkersUser = null;

    public MarkersIntentService() {
        super("MarkersIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Bundle dataIntent = intent.getExtras();
            if (dataIntent != null) {
                try {
                    Location location = dataIntent.getParcelable(Constants.KEY_CURRENT_LOCATION);
                    mMarkers = downloadMarkersCurrentLocation(location, PostAction.QUERY_LOCATION);
                   // mMarkersUser = downloadMarkersCurrentLocation(location, PostAction.QUERY_LOCATION_USER);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Intent resultIntent = new Intent(Constants.ACTION_SEND_FIND_MARKERS);
            resultIntent.putParcelableArrayListExtra(Constants.KEY_CURRENT_LIST_MARKERS, mMarkers);
            resultIntent.putParcelableArrayListExtra(Constants.KEY_CURRENT_LIST_MARKERS_USER, mMarkersUser);

            sendBroadcast(resultIntent);
        }
    }

    private ArrayList<MarkerParcelable> downloadMarkersCurrentLocation(Location location, PostAction action) throws IOException {
        if (location == null) return null;
        ArrayList<MarkerParcelable> markers = null;
        String dataS = "currentLat=" + location.getLatitude() + "&currentLng=" + location.getLongitude() +
                "&radius=" + 5 + "&action=" + action + "&user_account=" + Identificator.id(this);
        HttpURLConnection mConnection = InternetUtils.createHttpConnection(dataS);
        if (mConnection.getResponseCode() < 300) {
            markers = parseJson2(new BufferedInputStream(mConnection.getInputStream()));
        }
        mConnection.disconnect();
        return markers;
    }
}
