package com.appmobileos.freetoiletsrussiatrial.map;

import android.graphics.Bitmap;

import com.appmobileos.freetoiletsrussiatrial.utils.ImagesUtils;

/**
 * Created by andrey on 29/09/13.
 */
public interface ICollectionIcons {
    public Bitmap getFreeIcon(ImagesUtils.MapIcons type);
    public Bitmap getPaidIcon(ImagesUtils.MapIcons type);
    public Bitmap getIcon(ImagesUtils.MapIcons type, int price);

}
