package com.appmobileos.freetoiletsrussiatrial.map;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by andrey on 09/11/13.
 */
public class GeocoderCustom {
    ArrayList<OnGeocoderResult> observers = new ArrayList<OnGeocoderResult>();
    private Context mContext;
    private Geocoder mGeocoder;
    private List<Address> list;

    public GeocoderCustom(Context context) {
        mContext = context;
        mGeocoder = new Geocoder(mContext, Locale.getDefault());
    }

    public void registerObserver(OnGeocoderResult observer) {
        observers.add(observer);
    }

    public void unregisterObserver(OnGeocoderResult observer) {
        observers.remove(observer);
    }

    public void findCoordinates(String address) {
        new FindCoordinates().execute(address);
    }

    public void findAddress(LatLng location) {
        new FindAddress().execute(location);
    }

    private class FindCoordinates extends AsyncTask<String, Void, Address> {

        @Override
        protected Address doInBackground(String... params) {
            Address address = null;
            try {
                list = mGeocoder.getFromLocationName(params[0], 1);
                if (list.size() > 0) {
                    address = list.get(0);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return address;
        }

        @Override
        protected void onPostExecute(Address result) {
            for (OnGeocoderResult resultFinal : observers) {
                resultFinal.foundCoordinates(result == null ? null : new LatLng(result.getLatitude(), result.getLongitude()));
            }
        }

    }

    private class FindAddress extends AsyncTask<LatLng, Void, Address> {

        @Override
        protected Address doInBackground(LatLng... params) {
            Address address = null;
            try {
                LatLng location = params[0];
                if (location != null) {
                    list = mGeocoder.getFromLocation(location.latitude, location.longitude, 1);
                    if (list.size() > 0) {
                        address = list.get(0);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return address;
        }

        @Override
        protected void onPostExecute(Address result) {
            for (OnGeocoderResult resultFinal : observers) {
                resultFinal.foundAddress(result);
            }
        }

    }
}
