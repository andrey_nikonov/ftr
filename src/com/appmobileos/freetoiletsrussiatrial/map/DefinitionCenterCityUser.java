package com.appmobileos.freetoiletsrussiatrial.map;

/**
 * The class define center coordinates city (from user settings)
 * and moves focus map to get coordinates from geocoder.
 * 
 * 
 * */
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import com.appmobileos.freetoiletsrussiatrial.R;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.widget.Toast;

public class DefinitionCenterCityUser {
	private Context mContext;
	private Geocoder mGeocoder;
	private List<Address> list;
	private Address address;
	public DefinitionCenterCityUser(Context context, String cityName) {
		mContext = context;
		mGeocoder = new Geocoder(mContext, Locale.getDefault());
		new DefinitionCenterCity().execute(cityName);

	}

	private class DefinitionCenterCity extends AsyncTask<String, Void, Address> {

		@Override
		protected Address doInBackground(String... params) {
			try {
				list = mGeocoder.getFromLocationName(params[0], 1);
				if (list.size() > 0) {
					address = list.get(0);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			return address;
		}

		@Override
		protected void onPostExecute(Address result) {
			if (result != null) {
				//((GoogleMaps2Activity) mContext).moveToLocation(result.getLatitude(),
					//	result.getLongitude());
			} else {
				Toast.makeText(mContext,
						mContext.getString(R.string.toast_no_center_city_user),
						Toast.LENGTH_LONG).show();
			}
		}

	}

}
