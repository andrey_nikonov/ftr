package com.appmobileos.freetoiletsrussiatrial.map;

import android.location.Address;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by andrey on 05/11/13.
 */
public interface OnGeocoderResult {
    public void foundAddress(Address address);

    public void foundCoordinates(LatLng location);

}
