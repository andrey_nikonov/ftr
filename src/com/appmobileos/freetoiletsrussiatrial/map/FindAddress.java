package com.appmobileos.freetoiletsrussiatrial.map;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by andrey on 05/11/13.
 */
public class FindAddress {
    private Context mContext;

    public FindAddress(Context context) {
        mContext = context;
    }

    private class FindAddressByCoordinates extends AsyncTask<LatLng, Void, String> {
        @Override
        protected String doInBackground(LatLng... latLng) {
            List<Address> addresses;
            StringBuilder builder = new StringBuilder();
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latLng[0].latitude, latLng[0].longitude, 1);

                if (addresses.size() > 0) {
                    Address ad = addresses.get(0);
                    for (int i = 0; i < ad.getMaxAddressLineIndex(); i++) {
                        builder.append(ad.getAddressLine(i));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return builder.toString();
        }
    }

    private class FindCoorinatesByAddress extends AsyncTask<String, Void, LatLng> {
        @Override
        protected LatLng doInBackground(String... strings) {
            List<Address> addresses;
            LatLng position = null;
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocationName(strings[0], 1);
                if (addresses.size() > 0) {
                    Address ad = addresses.get(0);
                    position = new LatLng(ad.getLatitude(), ad.getLongitude());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return position;
        }
    }
}
