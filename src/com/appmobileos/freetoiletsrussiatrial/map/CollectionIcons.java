package com.appmobileos.freetoiletsrussiatrial.map;

import android.content.Context;
import android.graphics.Bitmap;

import com.appmobileos.freetoiletsrussiatrial.utils.ImagesUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * This is class create collections icon, free and paid (different circle color)
 */
public class CollectionIcons implements ICollectionIcons {
    private Map<ImagesUtils.MapIcons, Bitmap> mFreeIcons;
    private Map<ImagesUtils.MapIcons, Bitmap> mPaidIcons;
    private Context mContext;

    public CollectionIcons(Context context) {
        mFreeIcons = new HashMap<ImagesUtils.MapIcons, Bitmap>();
        mPaidIcons = new HashMap<ImagesUtils.MapIcons, Bitmap>();
        mContext = context;
        initFreeCollection();
        initPaidCollection();
    }

    @Override
    public Bitmap getFreeIcon(ImagesUtils.MapIcons type) {
        return mFreeIcons.get(type);
    }

    @Override
    public Bitmap getPaidIcon(ImagesUtils.MapIcons type) {
        return mPaidIcons.get(type);
    }

    @Override
    public Bitmap getIcon(ImagesUtils.MapIcons type, int price) {
        Bitmap result;
        if (price == 0) {
            result = mFreeIcons.get(type);
        } else {
            result = mPaidIcons.get(type);
        }
        return result;
    }

    private void initFreeCollection() {
        for (ImagesUtils.MapIcons typeIcon : ImagesUtils.MapIcons.values()) {
            Bitmap createdBitmap = ImagesUtils.createIcon(typeIcon, mContext, 0);
            mFreeIcons.put(typeIcon, createdBitmap);
        }
    }

    private void initPaidCollection() {
        for (ImagesUtils.MapIcons typeIcon : ImagesUtils.MapIcons.values()) {
            Bitmap createdBitmap = ImagesUtils.createIcon(typeIcon, mContext, 10);
            mPaidIcons.put(typeIcon, createdBitmap);
        }
    }
}
