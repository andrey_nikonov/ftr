package com.appmobileos.freetoiletsrussiatrial.map;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.appmobileos.freetoiletsrussiatrial.Constants;
import com.appmobileos.freetoiletsrussiatrial.MainInfoDialog;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import com.appmobileos.freetoiletsrussiatrial.adapters.DrawerAdapter;
import com.appmobileos.freetoiletsrussiatrial.controller.MapVisibleController;
import com.appmobileos.freetoiletsrussiatrial.controller.SlidePanelController;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerMain;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerParcelable;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerUser;
import com.appmobileos.freetoiletsrussiatrial.preferences.HelpActivity;
import com.appmobileos.freetoiletsrussiatrial.preferences.HelpFragment;
import com.appmobileos.freetoiletsrussiatrial.preferences.Identificator;
import com.appmobileos.freetoiletsrussiatrial.ui.AppendToiletFragment;
import com.appmobileos.freetoiletsrussiatrial.ui.InfoWindowMarkerFragment;
import com.appmobileos.freetoiletsrussiatrial.ui.MarkersViewPagesFragment;
import com.appmobileos.freetoiletsrussiatrial.ui.SlideInfoWindow;
import com.appmobileos.freetoiletsrussiatrial.ui.dialogs.ErrorMapDialogFragment;
import com.appmobileos.freetoiletsrussiatrial.ui.dialogs.RatingBarDialogFragment;
import com.appmobileos.freetoiletsrussiatrial.utils.DisplayUtils;
import com.appmobileos.freetoiletsrussiatrial.utils.InternetUtils;
import com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import static com.appmobileos.freetoiletsrussiatrial.ui.Logger.*;

import org.apache.commons.collections4.CollectionUtils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import stixiya.freetoilets.modal.PostAction;

import static com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST;
import static com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS;
import static com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils.UPDATE_INTERVAL_IN_SECONDS;
import static com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils.createJsonNewPlace;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN;

public class GoogleMaps2Activity extends FragmentActivity implements
        OnMarkerDragListener, OnMapLongClickListener, OnMarkerClickListener,
        OnCameraChangeListener,
        OnMapClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        MarkersViewPagesFragment.onViewPagesActions, SlideInfoWindow.onAnimationSlideInfoWindow,
        AppendToiletFragment.onAppendWC, RatingBarDialogFragment.OnRatingDialogChangeListener {
    public static final String TAG = "GoogleMaps2Activity";
    public static final int UPDATE_SERVICE = 0;
    public static final String PENDING_INTENT_EMULATION_ACTIVITY = "pending_intent_em_activity";
    public static final String PENDING_INTENT_CITY = "pending_intent_city";
    public static final String PENDING_INTENT_LOCAL_IDS = "pending_intent_local_ids";
    public static final int UPDATE_SERVICE_RESULT_OK = 0;
    public static final int UPDATE_SERVICE_RESULT_ERROR = 1;
    public static final String PENDING_INTENT_LAST_USER_UPDATE = "pending_intent_last_user_data_update";
    public static final String INTENT_DESCRIPTION_ADDRESS = "local_id";
    public static final String INTENT_DESCRIPTION_TABLE = "local_id_table";
    public static final String START_DATA_UPDATE_ADDRESS = "2013-05-18 00:00:00";
    public static final String UPDATE_REMOTE_SERVICE_ACTION = "action";
    public static final String UPDATE_COORDINATES_REMOTE_ID = "remote_id";
    // work with local database
    private static final int LOADER_START = 0;
    private static final int LOADER_USER_OBJECT = 2;
    private static final String FLAG_USER_TOUCH = "flagUserTouch";
    private static final String ZOOM_MAP_VALUE = "zoomMapValue";
    private static final String TYPE_MAP_VALUE = "typeMapValue";
    boolean flagUserTouch;
    float mMapZoom;
    int selectPosition = -1;
    boolean resultDeleteInfoFragment = false;
    private GoogleMap mMap;
    /*Fragments*/
    private InfoWindowMarkerFragment mWindowInfoMarkerFragment;
    // private HashMap<String, MarkerUser> mMarkersOnMapUser = new HashMap<String, MarkerUser>();
    /*---END---*/
    //private int mMapType = 0;
    private MarkersViewPagesFragment mMarkersViewPagesFragment;
    /**
     * Working with location user
     */
    private DefinitionCityUserSingleton defineCityUser;
    /**
     * Don't save, if activity recreate add places mActualPlaces data and id google markers will be new
     */
    private HashMap<Marker, MarkerParcelable> mMarkersOnMapDefault;
    private ArrayList<MarkerParcelable> mActualPlaces;
    private GoogleApiClient mGoogleLocationApiClient;
    private LocationRequest mLocationRequest;
    private SlideInfoWindow mInfoWindowContainer;
    private RelativeLayout mMainContainer;
    // private float mDisplayDensity = 0;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ImageButton mDrawerMenu;
    private ImageButton mLocationBtn;
    private FoundMarkersBroadcastReceiver mMarkersReceiver;
    private ChangeStatusNetworkBroadcastReceiver mChangeNetworkReceiver;
    private IntentFilter mFilterMarkersReceiver;
    private IntentFilter mFilterChangeStatusNetworkReceiver;
    private int mCountError = 0;
    private int mCountEmptyMarkers = 0;
    private int mCurrentItemViewPager = 0;
    private String idTempMarker;
    private Location mCurrentLocation;
    private ICollectionIcons mCollectionMapIcons;
    private AsyncTask mTaskInitMapIcons;
    private GoogleMap.CancelableCallback mAnimationCallback = new GoogleMap.CancelableCallback() {
        @Override
        public void onFinish() {
            // sendCurrentLocation(mCurrentLocation, 0);
        }

        @Override
        public void onCancel() {
            // sendCurrentLocation(mCurrentLocation, 0);
        }
    };
    private Marker mTempAppendMarker = null;
    private AppendToiletFragment mAppendToiletFragment;
    private int mCurrentRating;
    private SlidePanelController mSlidePanelController;
    private FrameLayout mContentSlidePanel;
    private MapVisibleController mMapVisibleController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps2);
        mInfoWindowContainer = (SlideInfoWindow) findViewById(R.id.maps_frame_container_info_window);
        mContentSlidePanel = (FrameLayout) findViewById(R.id.maps_frame_content_info_window);
        if (mInfoWindowContainer != null) {
            mInfoWindowContainer.setShadow(findViewById(R.id.info_wc_shadow));
        }
        mSlidePanelController = new SlidePanelController(this);
        mMainContainer = (RelativeLayout) findViewById(R.id.maps_linear_main);
        mDrawerMenu = (ImageButton) findViewById(R.id.maps_drawer_menu);
        mLocationBtn = (ImageButton) findViewById(R.id.maps_locationBtn);
        mMarkersOnMapDefault = new HashMap<Marker, MarkerParcelable>();
        mTaskInitMapIcons = new InitIconsMap().execute();
        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            mWindowInfoMarkerFragment = (InfoWindowMarkerFragment) fragmentManager.findFragmentByTag(getString(R.string.tag_fragment_info_window_market));
            if (mWindowInfoMarkerFragment != null) {
                hideFragment(mWindowInfoMarkerFragment);
            }
            mActualPlaces = new ArrayList<MarkerParcelable>();
        } else {
            flagUserTouch = savedInstanceState.getBoolean(FLAG_USER_TOUCH, false);
            mMapZoom = savedInstanceState.getFloat(ZOOM_MAP_VALUE);
            mActualPlaces = savedInstanceState.getParcelableArrayList(Constants.KEY_CURRENT_LIST_MARKERS);
            mCurrentItemViewPager = savedInstanceState.getInt(Constants.KEY_CURRENT_SELECTED_ITEM, 0);
            mMarkersViewPagesFragment = (MarkersViewPagesFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_fragment_markers_view_pages));
        }
        initDrawerNavigation();
        initLocation();
        setUpMapIfNeeded();
    }

    private void initDrawerNavigation() {
        String[] places = getResources().getStringArray(R.array.drawer_navigation);
        TypedArray icons = getResources().obtainTypedArray(R.array.drawer_navigation_images);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new DrawerAdapter(this, R.layout.drawer_list_item, places, icons));
        mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                selectPosition = position;
                // Highlight the selected item, update the title, and close the drawer
                mDrawerList.setItemChecked(position, true);
                //  setTitle(mDrawerItems[position]);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
        mDrawerLayout.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                selectDrawerMenuItem(selectPosition);
            }
        });
    }

    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    private void initLocation() {
        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_SECONDS);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FAST_INTERVAL_CEILING_IN_MILLISECONDS);
        // mLocationRequest.setSmallestDisplacement(5);
        mGoogleLocationApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void initBroadcastReceiver() {
        mFilterMarkersReceiver = new IntentFilter(Constants.ACTION_SEND_FIND_MARKERS);
        mMarkersReceiver = new FoundMarkersBroadcastReceiver();
        mFilterChangeStatusNetworkReceiver = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mChangeNetworkReceiver = new ChangeStatusNetworkBroadcastReceiver();
        registerReceiver(mMarkersReceiver, mFilterMarkersReceiver);
        registerReceiver(mChangeNetworkReceiver, mFilterChangeStatusNetworkReceiver);
    }

    private void selectDrawerMenuItem(int position) {
        switch (position) {
            case 0:
                showInfoPlaces(new ArrayList<MarkerParcelable>());
                if (mInfoWindowContainer.isVisible() && mMarkersViewPagesFragment != null) {
                    mMarkersViewPagesFragment.showAppendWC();
                } else {
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    Fragment frg = MarkersViewPagesFragment.newInstance(new ArrayList<MarkerParcelable>());
                    String tag = getString(R.string.tag_fragment_markers_view_pages);
                    transaction.add(mInfoWindowContainer.getId(), frg, tag);
                    transaction.commit();
                    manager.executePendingTransactions();
                    mInfoWindowContainer.actionShowInfoWindow();
                }
                break;
            case 1:
                if (isOnline()) {
                    MapModeDialog.newInstance(mMap.getMapType()).show(
                            getSupportFragmentManager(), "map_mode");
                } else {
                    Toast.makeText(this,
                            R.string.toast_error_internet_button_click,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case 2:
                mSlidePanelController.showLocalLocationSettings();
                break;
            case 3:
                startActivity(new Intent(this, HelpActivity.class));
                break;

        }
    }

    private void checkCorrectStatusDevice() {
        if (!isOnline()) {
            mSlidePanelController.showInfo(R.string.toast_error_internet, R.string.check_internet_settings, R.color.ap_red);
        } else if (!LocationUtils.isSystemLocationSettingsEnable(this)) {
            mSlidePanelController.showSystemLocationSettings();
        } else if (!RepeatMethods.getPreferenceLocation(this)) {
            mSlidePanelController.showLocalLocationSettings();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(FLAG_USER_TOUCH, flagUserTouch);
        outState.putFloat(ZOOM_MAP_VALUE, mMapZoom);
        outState.putParcelableArrayList(Constants.KEY_CURRENT_LIST_MARKERS, mActualPlaces);
        outState.putInt(Constants.KEY_CURRENT_SELECTED_ITEM, mCurrentItemViewPager);
        super.onSaveInstanceState(outState);
    }

 /*   public void remoteNotification() {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();
    }*/

    private Marker findGoogleMarker(MarkerParcelable marker) {
        if (marker == null) return null;
        for (Map.Entry<Marker, MarkerParcelable> empty : mMarkersOnMapDefault.entrySet()) {
            if (empty.getValue().getId() == marker.getId()) {
                return empty.getKey();
            }
        }
        return null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleLocationApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initBroadcastReceiver();
        checkCorrectStatusDevice();

    }

    public void startUpdates() {
        if (servicesConnected()) {
            startPeriodicUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleLocationApiClient.isConnected()) {
            stopPeriodicUpdates();
        }
        mGoogleLocationApiClient.disconnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMarkersReceiver);
        unregisterReceiver(mChangeNetworkReceiver);
        if (defineCityUser != null)
            defineCityUser.disableTimer();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
/*        if (isMyServiceRunning(UpdateLocalService.class)) {
            stopService(UpdateLocalService.class);
            remoteNotification();
        }

        if (isMyServiceRunning(UpdateRemoteService.class)) {
            stopService(UpdateRemoteService.class);
            remoteNotification();
        }*/

    }

    /*private void createAndShowHelpToast() {
        View view = getLayoutInflater().inflate(R.layout.toast_help_view_pages, null);
        Toast toast = new Toast(this);
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, -16, 350);
        toast.show();
    }*/

    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleLocationApiClient, mLocationRequest, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mCurrentLocation = location;
                if (!flagUserTouch) {
                    updateWithNewLocation(location);
                }
            }
        });
    }

    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleLocationApiClient, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            }
        });
    }

    /**
     * Check internet *
     */

    private boolean isOnline() {
        ConnectivityManager connectManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo infoNet = connectManager.getActiveNetworkInfo();
        return (infoNet != null && infoNet.isConnected());
    }

    /**
     * This is first method when start map activity.
     * *
     */
    private boolean servicesConnected() {
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                ErrorMapDialogFragment errorFragment = new ErrorMapDialogFragment();
                errorFragment.setDialog(errorDialog);
                errorFragment.show(getSupportFragmentManager(),
                        "Location Updates");
            }
        }
        return false;
    }

    /**
     * This is second method
     */

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

  /*  public void eventAppendWC(View viewClicked) {
        switch (viewClicked.getId()) {
            case R.id.append_wc_slide_objects:
                /*AppendToiletFragment fr = mMarkersViewPagesFragment.getAppendWCFragment();
                fr.slideObjectsName();
                break;
        }
    }
     */

    public void eventUiMapElements(View viewClicked) {
        switch (viewClicked.getId()) {
            case R.id.maps_drawer_menu:
                mDrawerLayout.openDrawer(Gravity.LEFT);
                //testProjectMao();
                break;
            case R.id.maps_locationBtn:
                if (!checkReady()) {
                    return;
                }
                if (RepeatMethods.getPreferenceLocation(this)) {
                    if (LocationUtils.isSystemLocationSettingsEnable(this)) {
                        Location currentLocation = mMap.getMyLocation();
                        if (currentLocation != null) {
                            flagUserTouch = false;
                            updateWithNewLocation(currentLocation);
                            sendCurrentLocation(currentLocation, 0);
                        }
                    } else {
                        mSlidePanelController.showSystemLocationSettings();
                    }
                } else {
                    mSlidePanelController.showLocalLocationSettings();
                }
                break;
        }
    }

    private void setUpMap() {
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        //  mMap.setOnInfoWindowClickListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setOnCameraChangeListener(this);
        mMap.setOnMapClickListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(PreferenceManager.getDefaultSharedPreferences(this)
                .getInt(getString(R.string.preference_key_map_type), 1));
        addNewPlacesMap(mActualPlaces, true);
        mMapVisibleController = new MapVisibleController(this);
    }

    public void setLayer(String layerName) {
        if (!checkReady())
            return;
        if (layerName.equals(getString(R.string.map_normal))) {
            mMap.setMapType(MAP_TYPE_NORMAL);
        } else if (layerName.equals(getString(R.string.map_satellite))) {
            mMap.setMapType(MAP_TYPE_SATELLITE);
        } else if (layerName.equals(getString(R.string.map_terrain))) {
            mMap.setMapType(MAP_TYPE_TERRAIN);
        }
        setMapTypeSetting(mMap.getMapType());
    }

    private void setMapTypeSetting(int mapType) {
        // use when recreate activity
        // use when new start application
        SharedPreferences.Editor mMapTypePref = PreferenceManager
                .getDefaultSharedPreferences(this).edit();
        mMapTypePref.putInt(getString(R.string.preference_key_map_type),
                mapType);
        mMapTypePref.apply();
    }

    /**
     * @return true- map OK, false - map not ready
     */
    private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, R.string.map_not_ready, Toast.LENGTH_SHORT)
                    .show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        flagUserTouch = true;
        marker.hideInfoWindow();
        MarkerParcelable markerParcelable = mMarkersOnMapDefault.get(marker);
        Log.i(getLocalClassName(), "OnClick marker find = " + markerParcelable.getId());
        mMarkersViewPagesFragment = (MarkersViewPagesFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_fragment_markers_view_pages));
        if (mMarkersViewPagesFragment != null) {
            mMarkersViewPagesFragment.showSmallInfoPlace(markerParcelable, mMap.getMyLocation());
            Log.i(getLocalClassName(), "CURRENT HEIGHT SLIDE  = " + mInfoWindowContainer.getHeight());
            if (!mInfoWindowContainer.isSmallSizeVisible()) {
                mInfoWindowContainer.actionShowInfoWindow();
            }
        } else {
            if (mWindowInfoMarkerFragment.isHidden()) {
                showFragment(mWindowInfoMarkerFragment);
            }
            mWindowInfoMarkerFragment.addInfoObject(markerParcelable);
            String distance = LocationUtils.calculateDistance(markerParcelable, mMap.getMyLocation());
            mWindowInfoMarkerFragment.updateDistance(distance);
        }
        return true;
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.alpha_visible, 0);
        fragmentTransaction.show(fragment);
        fragmentTransaction.commit();
    }

    private void hideFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(0, R.anim.alpha_gone);
        ft.hide(fragment);
        ft.commit();
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        // unused
    }

/*    @Override
    public void onInfoWindowClick(Marker marker) {
        // MarkerDescription infoMarker = mMarkersOnMapDefault.get(marker.getId());
        // if (infoMarker == null){
        //infoMarker = mMarkersOnMapUser.get(marker.getId());
        // }
        if (!checkReady()) {
            return;
        }
        Location myLocation = mMap.getMyLocation();
        if (myLocation != null) {

        }

    }*/

    @Override
    public void onMarkerDragEnd(Marker marker) {
        if (mAppendToiletFragment != null) {
            mAppendToiletFragment.setNewPlacePosition(marker.getPosition());
        }
        /*Intent mUpdateCoordinatesIntent = new Intent(this,
                UpdateRemoteService.class);

        mUpdateCoordinatesIntent
                .putExtra(UPDATE_REMOTE_SERVICE_ACTION, "update");*/
/*        if (!isMyServiceRunning(UpdateRemoteService.class))
            startService(mUpdateCoordinatesIntent);*/

    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        // unused
    }

    private void showInfoDialog(int idDialog) {
        DialogFragment ds = (DialogFragment) getSupportFragmentManager()
                .findFragmentByTag(MainInfoDialog.DIALOG_TAG);
        if (ds != null)
            ds.dismiss();
        MainInfoDialog info_dialog = MainInfoDialog.newInstance(idDialog, null);
        info_dialog
                .show(getSupportFragmentManager(), MainInfoDialog.DIALOG_TAG);

    }

    private void updateWithNewLocation(final Location location) {
        Log.i(getLocalClassName(), "updateWithNewLocation ");
        if (mMapZoom < 5.0)
            mMapZoom = 14.0f;
        LatLng latLng = new LatLng(location.getLatitude(),
                location.getLongitude());
        CameraPosition position = new CameraPosition.Builder()
                .zoom(mMapZoom).target(latLng)
                .tilt(mMap.getCameraPosition().tilt)
                .bearing(mMap.getCameraPosition().bearing).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), mAnimationCallback);
    }

    /**
     * Using DefinitionCenterCityUser *
     *//*
    public void moveToLocation(double latitude, double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        CameraPosition position = new CameraPosition.Builder().zoom(14.0f)
                .target(latLng).build();
        if (!checkReady()) {
            return;
        }
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
    }*/

    /*   private void stopUpdate() {
           stopUpdateUserAction();
           if (!checkReady()) {
               return;
           }
           mMap.setLocationSource(null);
       }
   */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                    /*
                     * Try the request again
                     */
                }
                break;
            default:
                break;
        }
    }

    private void showAboutFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(new HelpFragment(), "help");
        ft.commit();
    }

    @Override
    public void onMapLongClick(LatLng point) {
        flagUserTouch = true;
    }

    public void checkDataPlaces(List<MarkerParcelable> currentDataRemoteDatabase) {

        /**TEST DATA**/
        //markersFromRemoteDatabase = TestUtils.createMarkers();
        if (currentDataRemoteDatabase == null) {
            mCountError++;
            if (mCountError > 3) {
                //диалог что ошибка
                Toast.makeText(this, "Ошибка сервера", Toast.LENGTH_LONG).show();
            }
        } else if (currentDataRemoteDatabase.size() == 0) {
            mCountEmptyMarkers++;
            if (mCountEmptyMarkers == 1) {
                sendCurrentLocation(mMap.getMyLocation(), 15);
            } else if (mCountEmptyMarkers == 2) {
                sendCurrentLocation(mMap.getMyLocation(), 30);
            } else {
                //showDialog
            }
        } else {
            boolean sameData = CollectionUtils.isEqualCollection(currentDataRemoteDatabase, mActualPlaces);
            Log.i(TAG, "updateDataPlaces sameData = " + sameData);
            Log.i(TAG, "NEW PLACES = " + currentDataRemoteDatabase + "NEW PLACE SIZE = " + currentDataRemoteDatabase.size() + "\n"
                    + "ACTUAL PLACES = " + mActualPlaces + "ACTUAL PLACE SIZE =" + mActualPlaces.size());
            if (!sameData) {
                List<MarkerParcelable> deleted = (List<MarkerParcelable>) CollectionUtils.subtract(mActualPlaces, currentDataRemoteDatabase);
                List<MarkerParcelable> newResult = (List<MarkerParcelable>) CollectionUtils.subtract(currentDataRemoteDatabase, mActualPlaces);
                addNewPlacesMap(newResult, false);
                remoteOldPlacesMap(deleted);
            }
            //Show new places!
            showInfoPlaces(mActualPlaces);
            //updateDataPlaces(currentDataRemoteDatabase, mActualPlaces);
        }
    }

    /**
     * Add new place on the map
     *
     * @param newPlaces places which will be add map
     * @param startInit when activity created in method setUpMap() add new (restore) places and don't need add it's in mActualPlaces.
     *                  It's will be restored. If this is first start mActualPlaces size == 0.
     */
    private void addNewPlacesMap(List<MarkerParcelable> newPlaces, boolean startInit) {
        for (MarkerParcelable marker : newPlaces) {
            MarkerOptions options = createMarkerOptions(marker);
            Marker markerMap = mMap.addMarker(options);
            mMarkersOnMapDefault.put(markerMap, marker);
            if (!startInit) mActualPlaces.add(marker);
        }
        Log.i(TAG, " (AFTER REMOTE ALL ACTUAL PLACES ) NEW PLACES = " + mActualPlaces + "NEW PLACE SIZE = " + newPlaces.size());

    }

    private void remoteOldPlacesMap(List<MarkerParcelable> oldPlaces) {
        for (MarkerParcelable marker : oldPlaces) {
            Marker oldMarker = findGoogleMarker(marker);
            oldMarker.remove();
            mMarkersOnMapDefault.remove(oldMarker);
            mActualPlaces.remove(marker);
        }
        Log.i(TAG, " (AFTER REMOTE ALL NEW PLACES ) OLD PLACES = " + mActualPlaces + "OLD PLACE SIZE = " + oldPlaces.size());
    }


/*    private void startUpdateService(ArrayList<Integer> localDatabaseIds,
                                    String lastDataUpdate) {
        ///if (isMyServiceRunning(UpdateLocalService.class))
        // return;
        Intent intent = new Intent(this, UpdateLocalService.class);
        PendingIntent pendingIntent = createPendingResult(UPDATE_SERVICE,
                intent, 0);
        intent.putExtra(PENDING_INTENT_EMULATION_ACTIVITY, pendingIntent);
        intent.putExtra(PENDING_INTENT_LAST_USER_UPDATE, lastDataUpdate);
        intent.putIntegerArrayListExtra(PENDING_INTENT_LOCAL_IDS,
                localDatabaseIds);
        intent.putExtra(PENDING_INTENT_CITY, RepeatMethods.getCity(this));
        // startService(intent);
    }*/

    private void showFragmentDialog() {
        String tagRatingDialog = getString(R.string.tag_dialog_fragment_rating);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment checkFragmentDialog = getSupportFragmentManager().findFragmentByTag(tagRatingDialog);
        if (checkFragmentDialog != null) {
            transaction.remove(checkFragmentDialog);
        }
        transaction.addToBackStack(null);
        DialogFragment ratingFragment = RatingBarDialogFragment.newInstance();
        ratingFragment.show(transaction, tagRatingDialog);
    }

    private void showInfoPlaces(List<MarkerParcelable> actualMarkers) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        if (mWindowInfoMarkerFragment == null) {
            String viewPagesTag = getString(R.string.tag_fragment_markers_view_pages);
            mMarkersViewPagesFragment = (MarkersViewPagesFragment) fragmentManager.findFragmentByTag(viewPagesTag);
            if (mMarkersViewPagesFragment == null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                ArrayList<MarkerParcelable> places = new ArrayList<MarkerParcelable>(actualMarkers.size());
                places.addAll(actualMarkers);
                fragmentTransaction.add(R.id.maps_frame_content_info_window, MarkersViewPagesFragment.newInstance(places), viewPagesTag);
                fragmentTransaction.commit();
                Log.i(getLocalClassName(), " initSmallDescriptionViewMarkers - CREATE");
            } else {
                Log.i(getLocalClassName(), " initSmallDescriptionViewMarkers - UPDATE");
                // mCollectionPagesAdapter.updateMarkers(actualMarkers);
            }
        }
    }

    private MarkerOptions createMarkerOptions(MarkerParcelable currentMarker) {
        MarkerOptions marker = new MarkerOptions();
        //Log.i(getClass().getName(), "CURRENT MARKER = " + currentMarker);
        LatLng position = new LatLng(currentMarker.getLatitude(),
                currentMarker.getLongitude());
        marker.position(position);
        String object = currentMarker.getObject();
        marker.icon(LocationUtils.selectImagePlace(mCollectionMapIcons, this, object, currentMarker.getPrice()));
        String title = currentMarker.getDescription();
        marker.title((title != null && title.length() > 1) ? title : object);
        return marker;
    }

    @Override
    public void onCameraChange(CameraPosition arg0) {
        if (arg0.zoom == mMapZoom) {
            flagUserTouch = true;
        }
        Log.i(getLocalClassName(), "onCameraChange and flagTouch = " + flagUserTouch);
        mMapZoom = arg0.zoom;
    }

    @Override
    public void onMapClick(LatLng point) {
        flagUserTouch = true;
    }

    /**
     * Send location in intentService. Start auto.
     *
     * @param currentLocation user location now
     * @param radius          this is radius (0 == default value and will be change 5)
     */
    private void sendCurrentLocation(Location currentLocation, int radius) {
        Intent intent = new Intent(this, MarkersIntentService.class);
        intent.putExtra(Constants.KEY_CURRENT_LOCATION, currentLocation);
        intent.putExtra(Constants.KEY_RADIUS, radius == 0 ? 5 : radius);
        startService(intent);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleLocationApiClient);
        if (lastLocation != null) {
            startPeriodicUpdates();
        }
    }

/*    @Override
    public void onLocationChanged(Location location) {
        //sendCurrentLocation(location);
        mCurrentLocation = location;
        if (!flagUserTouch) {
            updateWithNewLocation(location);
        }
        //Log.i(getLocalClassName(), "Location Update " + location.getLatitude() + " " + location.getLongitude() + "flag = " + flagUserTouch);
    }*/

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    connectionResult.getErrorCode(),
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                ErrorMapDialogFragment errorFragment = new ErrorMapDialogFragment();
                errorFragment.setDialog(errorDialog);
                errorFragment.show(getSupportFragmentManager(), "errordialog");
            }
        }
    }

    private int sendDataPlace(MarkerUser markerUser, MarkerMain markerMain, PostAction action) throws IOException {
        int resultSqlOperation = 0;
        String actionSection = "action=" + action;
        String data;
        switch (action) {
            case APPEND:
                data = actionSection + "&newPlace=" + createJsonNewPlace(markerUser);
                break;
            case USER_OPINION_DENY:
                data = actionSection + "&id_wc=" + markerMain.getId() + "&user_account=" + Identificator.id(this);
                break;
            case RATING:
                data = actionSection + "&id_wc=" + markerMain.getId() + "&user_account=" + Identificator.id(this) + "&rating=" + mCurrentRating;
                break;
            default:
                throw new UnsupportedOperationException("Don't support action = " + action);
        }
        HttpURLConnection mConnection = InternetUtils.createHttpConnection(data);
        int resultService = mConnection.getResponseCode();
        if (resultService < 300) {
            DataInputStream responseService = new DataInputStream(new BufferedInputStream(mConnection.getInputStream()));
            resultSqlOperation = responseService.readInt();
            responseService.close();
        }
        mConnection.disconnect();
        return resultSqlOperation;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mInfoWindowContainer != null) {
                Log.i(TAG, "onKeyUp infoWindowHeight = " + mInfoWindowContainer.getHeight());
                if (mInfoWindowContainer.getHeight() != 0) {
                    mInfoWindowContainer.actionHideInfoWindow();
                    return true;
                }
            }
            resultDeleteInfoFragment = deleteFragmentSlidePanel(SlidePanels.INFO);
            if (resultDeleteInfoFragment) {
                return true;
            } else {
                return super.onKeyUp(keyCode, event);
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    private boolean deleteFragmentSlidePanel(SlidePanels action) {
        Fragment fragment = null;
        boolean result = false;
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        switch (action) {
            case INFO:
                fragment = manager.findFragmentById(R.id.auto_slide);
                transaction.setCustomAnimations(R.anim.slide_top_in, R.anim.slide_top_out);
                break;
            case PLACES:
                fragment = manager.findFragmentById(R.id.maps_frame_content_info_window);
                break;
        }
        if (fragment != null) {
            transaction.remove(fragment);
            result = true;
        }
        transaction.commit();
        return result;
    }

    public void eventUiContentDescription(View view) {
        switch (view.getId()) {
            case R.id.action_place_deny:
                if (mMarkersViewPagesFragment != null) {
                    MarkerMain place = (MarkerMain) mMarkersViewPagesFragment.findCurrentPlace();
                    Log.i(getLocalClassName(), "OPIONION PLACE ID = " + place.getId());
                    new OpinionPlace().execute(place);
                }
                break;
            case R.id.action_user_rating:
                showFragmentDialog();
                break;
        }
    }

    @Override
    public void onPageViewChanged(MarkerParcelable visibleMarker, int position) {
        //  Log.i(getLocalClassName(), "onPageViewChanged with marker" + visibleMarker.getId());
        Location location;
        Location markerLocation = null;
        location = mMap.getMyLocation();
        if (visibleMarker == null) {
            if (location != null) {
                updateWithNewLocation(location);
                mAppendToiletFragment = (AppendToiletFragment) mMarkersViewPagesFragment.findAppendWCFragment();
                showTempAppendPlace(location);
            }
        } else {
            markerLocation = new Location("manual");
            markerLocation.setLongitude(visibleMarker.getLongitude());
            markerLocation.setLatitude(visibleMarker.getLatitude());
            updateWithNewLocation(markerLocation);
            if (mTempAppendMarker != null) mTempAppendMarker.remove();
        }
        if (markerLocation != null && mMarkersViewPagesFragment != null) {
            mMarkersViewPagesFragment.updateDistanceValue(visibleMarker, location, position);
        } else {
            Toast.makeText(this, "ERRRRRR" + markerLocation + " FRAG = " + mMarkersViewPagesFragment, Toast.LENGTH_SHORT).show();
        }
    }

    private void showTempAppendPlace(Location location) {
        MarkerOptions options = new MarkerOptions();
        LatLng locationNewFormat = new LatLng(location.getLatitude(), location.getLongitude());
        options.position(locationNewFormat);
        options.draggable(true);
        options.title("Это место будет добавлено\nДля перемещения маркера используйте долгое нажатие на нем");
        mTempAppendMarker = mMap.addMarker(options);
        mTempAppendMarker.showInfoWindow();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mTempAppendMarker.getPosition(),
                17f), 1000, null);
    }

    @Override
    public void onFlipperNextObject(String object, int price) {
        if (mTempAppendMarker != null) {
            mTempAppendMarker.setIcon(LocationUtils.selectImagePlace(mCollectionMapIcons, this, object, price));
            mTempAppendMarker.setTitle("Дополнительная информация");
            mTempAppendMarker.showInfoWindow();
        }

    }

    /* private void zoomToPlace(MarkerParcelable marker,Marker googleMarker) {
         LatLng goPosition = null;
         if(marker==null){
             goPosition = new
         }
         mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(marker.getLatitude(), marker.getLongitude()),
                 18f), 350, null);
         // XXX Need independent animation ms for place zoom in/out
     }*/
    @Override
    public void onActionTop() {
        if (mInfoWindowContainer.isSmallSizeVisible()) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(mLocationBtn, "alpha", 1f, 0f);
            ObjectAnimator animator2 = ObjectAnimator.ofFloat(mDrawerMenu, "alpha", 1f, 0f);
            AnimatorSet setA = new AnimatorSet();
            setA.setDuration(50);
            setA.playTogether(animator, animator2);
            setA.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mInfoWindowContainer.actionShowInfoWindow();
                    if (mInfoWindowContainer != null) {
                        MarkerParcelable currentMarker;
                        Marker googleMarker;
                        if (mMarkersViewPagesFragment != null) {
                            currentMarker = mMarkersViewPagesFragment.findCurrentPlace();
                            googleMarker = findGoogleMarker(currentMarker);
                           /* int height = DisplayUtils.getMinHeightGoogleMap(GoogleMaps2Activity.this);
                            int width = DisplayUtils.getMaxDisplayWidth(GoogleMaps2Activity.this);
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            builder.include(googleMarker.getPosition()); */
                            mMapVisibleController.setCurrentGoogleMarker(googleMarker);
                            mMapVisibleController.setMapProjection(mMap.getProjection());
                            mMap.animateCamera(CameraUpdateFactory.scrollBy(mMapVisibleController.calculateGoX(), mMapVisibleController.calculateGoY()));
                            //Log.i(TAG, "GO SCREEEN HEIGHT = " + height + "  WIDTH = " + width);
                        }

                    }
                }
            });
            setA.start();
        } else {
            mInfoWindowContainer.actionShowInfoWindow();
        }
    }

    private void testProjectMao() {
        int heightMax = DisplayUtils.getMaxHeightWindowInfoMarker(this);
        int heightMaxDisplay = DisplayUtils.getMaxDisplayHeight(this);
        int maxYRect = DisplayUtils.getMinHeightGoogleMap(this);
        MarkerParcelable currentMarker = mMarkersViewPagesFragment.findCurrentPlace();
        Marker googleMarker = findGoogleMarker(currentMarker);
        Projection mm = mMap.getProjection();
        Point point = mm.toScreenLocation(googleMarker.getPosition());
        int go = point.y - maxYRect;
        Log.i(TAG, "MARKER SCREEN POSITION x= " + point.x + "y = " + point.y + "MAX RECT SLIDE PANEL = " + heightMax + "GO = " + go + " MIN = " + maxYRect);

    }

    @Override
    public void onActionBack(final int newHeight) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(mLocationBtn, "alpha", 0f, 1f);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(mDrawerMenu, "alpha", 0f, 1f);
        AnimatorSet setA = new AnimatorSet();
        setA.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                mLocationBtn.setVisibility(View.VISIBLE);
                mDrawerMenu.setVisibility(View.VISIBLE);
            }
        });
        setA.setDuration(50);
        setA.playTogether(animator, animator2);
        setA.start();
    }

    @Override
    public void onTouchEventCustom(MotionEvent motionEvent) {
        if (mInfoWindowContainer != null) {
            mInfoWindowContainer.onTouchEvent(motionEvent);
        }
    }

    @Override
    public void onRatingInstalled(int result) {
        mCurrentRating = result;
        new InstallRating().execute();
    }

    @Override
    public void foundCoordinates(Location location) {
        updateWithNewLocation(location);
    }

    @Override
    public void onFullScreenUsed() {
        Log.i(TAG, "onFullScreenUsed called");
        mLocationBtn.setVisibility(View.GONE);
        mDrawerMenu.setVisibility(View.GONE);
    }

    @Override
    public void changeStatusVisible(boolean show) {
        Log.i(TAG, " changeStatusVisible show param = " + show);
        if (show) {
            mContentSlidePanel.setVisibility(View.VISIBLE);
        } else {
            deleteFragmentSlidePanel(SlidePanels.PLACES);
            mContentSlidePanel.removeAllViews();
            mContentSlidePanel.setVisibility(View.GONE);
        }

    }

    enum SlidePanels {
        INFO, PLACES
    }

    private class AppendPlace extends AsyncTask<MarkerUser, Void, Integer> {
        @Override
        protected Integer doInBackground(MarkerUser... markers) {
            int result = 0;
            try {
                result = sendDataPlace(markers[0], null, PostAction.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            Log.i(getLocalClassName(), "Добавление результат = " + result);
            int toastShowUser = R.string.notification_add_data_result_fail;
            if (result > 0) {
                toastShowUser = R.string.notification_add_data_result_ok;
            }
            Toast.makeText(GoogleMaps2Activity.this, toastShowUser, Toast.LENGTH_SHORT).show();
        }
    }

    private class OpinionPlace extends AsyncTask<MarkerMain, Void, Integer> {
        private MarkerParcelable mOpinionMarker;

        @Override
        protected void onPreExecute() {
            if (mMarkersViewPagesFragment != null) {
                mMarkersViewPagesFragment.updateUserOpinionProgressing(true);
            }
        }

        @Override
        protected Integer doInBackground(MarkerMain... actions) {
            int result = 0;
            try {
                mOpinionMarker = actions[0];
                result = sendDataPlace(null, (MarkerMain) mOpinionMarker, PostAction.USER_OPINION_DENY);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            Log.i(getLocalClassName(), "Мнение результат = " + result);
            InfoWindowMarkerFragment.UserOpinionResult resultUserOpinion;
            if (result > 0) {
                resultUserOpinion = InfoWindowMarkerFragment.UserOpinionResult.OK;
            } else {
                resultUserOpinion = InfoWindowMarkerFragment.UserOpinionResult.FAILED;
            }
            if (mMarkersViewPagesFragment != null) {
                mMarkersViewPagesFragment.updateUserOpinionProgressing(false);
                mMarkersViewPagesFragment.updateUserOpinion(mOpinionMarker, resultUserOpinion);
            }
        }
    }

    private class InstallRating extends AsyncTask<Void, Void, Integer> {
        private MarkerMain mPlace;

        @Override
        protected void onPreExecute() {
            if (mMarkersViewPagesFragment != null) {
                mMarkersViewPagesFragment.updateUserOpinionProgressing(true);
            }
        }

        @Override
        protected Integer doInBackground(Void... integers) {
            int result = 0;
            mPlace = (MarkerMain) mMarkersViewPagesFragment.findCurrentPlace();
            try {
                result = sendDataPlace(null, mPlace, PostAction.RATING);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            InfoWindowMarkerFragment.UserOpinionResult resultUserOpinion;
            if (result > 0) {
                resultUserOpinion = InfoWindowMarkerFragment.UserOpinionResult.OK;
            } else {
                resultUserOpinion = InfoWindowMarkerFragment.UserOpinionResult.FAILED;
            }
            if (mMarkersViewPagesFragment != null) {
                mMarkersViewPagesFragment.updateUserOpinionProgressing(false);
                mMarkersViewPagesFragment.updateUserOpinion(mPlace, resultUserOpinion);
            }
        }
    }

    public class FoundMarkersBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle dataIntent = intent.getExtras();
            if (dataIntent != null && mTaskInitMapIcons.getStatus() == AsyncTask.Status.FINISHED) {
                ArrayList<MarkerParcelable> markers = dataIntent.getParcelableArrayList(Constants.KEY_CURRENT_LIST_MARKERS);
                checkDataPlaces(markers);
            /*    ArrayList<MarkerParcelable> markersUser = dataIntent.getParcelableArrayList(Constants.KEY_CURRENT_LIST_MARKERS_USER);
                addMarkersMap(markersUser);*/
            }
        }
    }

    private class ChangeStatusNetworkBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkCorrectStatusDevice();
        }
    }

    private class InitIconsMap extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            mCollectionMapIcons = new CollectionIcons(GoogleMaps2Activity.this);
            return null;
        }
    }
}



