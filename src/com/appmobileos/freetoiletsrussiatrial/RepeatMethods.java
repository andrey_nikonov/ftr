package com.appmobileos.freetoiletsrussiatrial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

import com.appmobileos.freetoiletsrussiatrial.map.GoogleMaps2Activity;

public class RepeatMethods {
    public static final String SETTINGS_USER = "SETTING_CITY";
    public static final String KEY_CITY = "CITY";

    /**
     * Start home activity and clear history activity stack
     * *
     */
    public static void goHome(Activity activity) {
        Intent intent = new Intent(activity, GoogleMaps2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }

    /**
     * Check internet *
     */
    public static boolean isOnline(Context mContext) {
        ConnectivityManager connecrManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infoNet = connecrManager.getActiveNetworkInfo();
        return (infoNet != null && infoNet.isConnected());
    }

    /**
     * Check wi-fi connect
     *
     * @return true if wi-fi enable
     * *
     */
    public static boolean inWiFiOnline(Context mContext) {
        ConnectivityManager connectManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return networkInfo.isConnected();
    }

    /**
     * Check mobile connect
     *
     * @return true if mobile enable
     * *
     */
    public static boolean inModileDataOnline(Context mContext) {
        ConnectivityManager connectManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return networkInfo.isConnected();
    }

    private static String checkOldSettings(Context context) {
        String cityName = null;
        SharedPreferences myShared = context.getSharedPreferences(
                SETTINGS_USER, Activity.MODE_PRIVATE);
        cityName = myShared.getString(KEY_CITY, null);
        return cityName;
    }

    public static String getCity(Context context) {
        String city = null;
        if (checkOldSettings(context) != null) {
            city = checkOldSettings(context);
            setPreference(city, context);
            deleteOldSettings(context);
        } else {
            SharedPreferences preference = PreferenceManager
                    .getDefaultSharedPreferences(context);
            city = preference.getString(
                    context.getString(R.string.preference_key_city), null);

        }
        // city="Херован";
        return city;

    }

    public static void deleteOldSettings(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                SETTINGS_USER, Activity.MODE_PRIVATE).edit();
        editor.remove(KEY_CITY);
        editor.commit();
    }

    public static boolean setPreference(String cityName, Context context) {
        SharedPreferences.Editor writeNameCity = PreferenceManager
                .getDefaultSharedPreferences(context).edit();
        writeNameCity.putString(
                context.getString(R.string.preference_key_city), cityName);
        return writeNameCity.commit();
    }

    public static void setPreferenceLastUpdate(String lastUpdate,
                                               Context context) {
        SharedPreferences.Editor writeLastUpdateData = PreferenceManager
                .getDefaultSharedPreferences(context).edit();
        writeLastUpdateData.putString(
                context.getString(R.string.preference_key_last_update),
                lastUpdate);
        writeLastUpdateData.commit();
    }

    public static boolean setPreferenceLocation(boolean location, Context context) {
        SharedPreferences.Editor writeLocation = PreferenceManager
                .getDefaultSharedPreferences(context).edit();
        writeLocation.putBoolean(context.getString(R.string.preference_key_location), location);
        return writeLocation.commit();
    }

    public static boolean getPreferenceLocation(Context context) {
        SharedPreferences preference = PreferenceManager
                .getDefaultSharedPreferences(context);
        return preference.getBoolean(
                context.getString(R.string.preference_key_location), false);
    }
    public static boolean setPreferenceHelpAppend(boolean help, Context context) {
        SharedPreferences.Editor writeLocation = PreferenceManager
                .getDefaultSharedPreferences(context).edit();
        writeLocation.putBoolean(context.getString(R.string.preference_key_help_append_wc), help);
        return writeLocation.commit();
    }

    public static boolean getPreferenceHelpAppend(Context context) {
        SharedPreferences preference = PreferenceManager
                .getDefaultSharedPreferences(context);
        return preference.getBoolean(
                context.getString(R.string.preference_key_help_append_wc), false);
    }
    public static String getLastDateUpdate(Context context) {
        SharedPreferences preference = PreferenceManager
                .getDefaultSharedPreferences(context);
        return preference.getString(
                context.getString(R.string.preference_key_last_update), null);
    }

    public static boolean setHideNoAddressHite(boolean hindHite, Context context) {
        SharedPreferences.Editor writeHideHite = PreferenceManager
                .getDefaultSharedPreferences(context).edit();
        writeHideHite.putBoolean(
                context.getString(R.string.preference_key_hite_no_address),
                hindHite);
        return writeHideHite.commit();

    }

    public static boolean getNoAddressHite(Context context) {
        SharedPreferences preference = PreferenceManager
                .getDefaultSharedPreferences(context);
        return preference.getBoolean(
                context.getString(R.string.preference_key_hite_no_address),
                false);
    }


}
