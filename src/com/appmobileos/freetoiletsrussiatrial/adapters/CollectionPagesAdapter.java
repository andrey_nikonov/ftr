package com.appmobileos.freetoiletsrussiatrial.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.appmobileos.freetoiletsrussiatrial.modal.MarkerParcelable;
import com.appmobileos.freetoiletsrussiatrial.ui.AppendToiletFragment;
import com.appmobileos.freetoiletsrussiatrial.ui.InfoWindowMarkerFragment;

import java.util.List;

/**
 * Created by a.nikonov on 18.09.13.
 */
public class CollectionPagesAdapter extends FragmentStatePagerAdapter {
    private List<MarkerParcelable> markers;

    public CollectionPagesAdapter(FragmentActivity activity, List<MarkerParcelable> markers) {
        super(activity.getSupportFragmentManager());
        this.markers = markers;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == markers.size()) {
            return AppendToiletFragment.newInstance();
        } else {
            return InfoWindowMarkerFragment.newInstance(markers.get(position));
        }

    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof InfoWindowMarkerFragment) {
            InfoWindowMarkerFragment infoWindowFragment = (InfoWindowMarkerFragment) object;
            return markers.indexOf(infoWindowFragment.getMarker());
        } else if (object instanceof MarkerParcelable) {
            return markers.indexOf(object);
        } else {
            return super.getItemPosition(object);
        }

    }

    public MarkerParcelable getMarkerParcelable(int position) {
        if (position < markers.size()) {
            return markers.get(position);
        } else {
            return null;
        }
    }

    public Fragment findAppendToiletFragment() {
        return getItem(positionAppendWC());
    }

    @Override
    public int getCount() {
        return markers.size() + 1;
    }

    public int positionAppendWC() {
        return markers.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        return super.instantiateItem(container, position);
    }
}
