package com.appmobileos.freetoiletsrussiatrial.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appmobileos.freetoiletsrussiatrial.R;

/**
 * Created by andrey on 16/09/13.
 */
public class DrawerAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private String[] mTextData;
    private TypedArray mImagesData;

    public DrawerAdapter(Context context, int textViewResourceId, String[] objects, TypedArray images) {
        super(context, textViewResourceId, objects);
        mContext = context;
        mTextData = objects;
        mImagesData = images;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        View viewRoot = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewRoot = inflater.inflate(R.layout.drawer_list_item, null, true);
            holder = new ViewHolder();
            //holder.imageView = (ImageView) viewRoot.findViewById(R.id.drawer_item_icon);
            holder.textView = (TextView) viewRoot.findViewById(R.id.drawer_item_text);
            viewRoot.setTag(holder);

        } else {
            holder = (ViewHolder) viewRoot.getTag();
        }
        holder.textView.setText(mTextData[position]);

        return viewRoot;
    }

    static class ViewHolder {
        //public ImageView imageView;
        public TextView textView;
    }
}
