package com.appmobileos.freetoiletsrussiatrial.database;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import java.util.Locale;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.database.LocalDatabaseContentProvider.CustomSQLHelper;
import com.appmobileos.freetoiletsrussiatrial.ui.AppendToiletFragment;
import com.appmobileos.freetoiletsrussiatrial.map.GoogleMaps2Activity;
import com.appmobileos.freetoiletsrussiatrial.preferences.Identificator;
import com.google.android.gms.maps.model.LatLng;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;


public class UpdateRemoteService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private String mObject;
	private String mValueObject;
	private int mPrice;
	private String mFind;
	private LatLng mPosition;
	private String mAction;
	private NotificationManager mNotificationManager;
	private int idNotification = 0;
	private int idNotificationUpdateCoordinats = 1;
	private int idRemoteDatabase;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
			mObject = intent
					.getStringExtra(AppendToiletFragment.APPENDTOILETACTIVITY_OBJECT);
			mValueObject = intent
					.getStringExtra(AppendToiletFragment.APPENDTOILETACTIVITY_NAME_OBJECT);
			mPrice = intent.getIntExtra(
					AppendToiletFragment.APPENDTOILETACTIVITY_VALUE_COAST, 0);
			mFind = intent
					.getStringExtra(AppendToiletFragment.APPENDTOILETACTIVITY_FIND_WC);
			mPosition = intent
					.getParcelableExtra(AppendToiletFragment.APPENDTOILETACTIVITY_POSITION);
			mAction = intent
					.getStringExtra(GoogleMaps2Activity.UPDATE_REMOTE_SERVICE_ACTION);
			idRemoteDatabase = intent.getIntExtra(
					GoogleMaps2Activity.UPDATE_COORDINATES_REMOTE_ID, 0);
			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			if (mAction.equals("append"))
				addNotification(mValueObject);
			else
				updateCoordinatsNotification();

			mainProcessing();

		}
		return Service.START_STICKY;
	}

	private void addNotification(String title) {
		Notification notification = new NotificationCompat.Builder(this)
				.setSmallIcon(
						com.appmobileos.freetoiletsrussiatrial.R.drawable.notification_content_new)
				.setContentTitle(
						(title != null) ? title : getResources().getString(
								R.string.notification_add_data))
				.setContentText(
						(title != null) ? getResources().getString(
								R.string.notification_add_data) : "")
				.setContentIntent(getPendingIntent()).setProgress(0, 0, true)
				.build();
		mNotificationManager.notify(idNotification, notification);

	}

	private void updateCoordinatsNotification() {
		Notification notification = new NotificationCompat.Builder(this)
				.setSmallIcon(
						com.appmobileos.freetoiletsrussiatrial.R.drawable.notification_refresh)
				.setProgress(0, 0, true)
				.setContentTitle(
						getResources().getString(
								R.string.notification_update_data))
				.setContentIntent(getPendingIntent())
				.setTicker(
						getResources().getString(
								R.string.notification_update_data)).build();
		mNotificationManager.notify(idNotificationUpdateCoordinats,
				notification);
	}

	private PendingIntent getPendingIntent() {
		Intent intent = new Intent(this, GoogleMaps2Activity.class);
		TaskStackBuilder task = TaskStackBuilder.create(this);
		task.addParentStack(GoogleMaps2Activity.class);
		task.addNextIntent(intent);
		return task.getPendingIntent(0, 0);
	}

	private void mainProcessing() {
		Thread mainThread = new Thread(null, doBackgroundThreadProcessing,
				"Background");
		mainThread.start();
	}

	private Runnable doBackgroundThreadProcessing = new Runnable() {

		@Override
		public void run() {
			if (mAction.equals("append")) {
				geocoder(mPosition);
			} else if (mAction.equals("update")) {
				geocoder(mPosition);
			} else if (mAction.equals("delete")) {

			}

		}
	};

	private Connection mConnection;
	private java.sql.PreparedStatement mPreparedStatement;

	/*private void addNewPlace(String address, String city) {
		int result = 0;
		int returnRemoteId = 0;
		try {
			try {
				mConnection = ConnectionDatabaseSingleton.getInstance()
						.getConnection(DatabaseConstants.URL_DATABASE_TEMP);
				if (mConnection != null) {
					mPreparedStatement = mConnection.prepareStatement(
							DatabaseConstants.ADD_WC_TEMP_USER,
							PreparedStatement.RETURN_GENERATED_KEYS);
					mPreparedStatement.setString(1, address);
					mPreparedStatement.setInt(2, mPrice);
					mPreparedStatement.setString(3, mObject);
					mPreparedStatement.setString(4, mValueObject);
					mPreparedStatement.setString(5, mFind);
					mPreparedStatement.setDouble(6, mPosition.latitude);
					mPreparedStatement.setDouble(7, mPosition.longitude);
					mPreparedStatement.setString(8, city);
					mPreparedStatement.setString(9, Identificator.id(this));
					mPreparedStatement.setInt(10, 0);
					mPreparedStatement.setString(11, null);
					result = mPreparedStatement.executeUpdate();
					ResultSet res = mPreparedStatement.getGeneratedKeys();
					if (res.next()) {
						returnRemoteId = res.getInt(1);
					}
				} else {
					result = 0;
				}

			} finally {
				if (mConnection != null)
					mConnection.close();
				if (mPreparedStatement != null)
					mPreparedStatement.close();

			}

		} catch (SQLException e) {
			e.printStackTrace();
			result = 0;
		}
		// send result and this check null
		addNewDataLocal(result, returnRemoteId);
	}*/

	private void addNewDataLocal(int remoteResult, int remoteId) {
		if (remoteResult > 0) {
			ContentValues values = new ContentValues();
			values.put(CustomSQLHelper.KEY_REMOTE_ID, remoteId);
			values.put(CustomSQLHelper.KEY_ADDRESS, address);
			values.put(CustomSQLHelper.KEY_PRICE, mPrice);
			values.put(CustomSQLHelper.KEY_OBJECT, mObject);
			values.put(CustomSQLHelper.KEY_DESCRIPTION, mValueObject);
			values.put(CustomSQLHelper.KEY_FIND_WC, mFind);
			values.put(CustomSQLHelper.KEY_LATITUDE, mPosition.latitude);
			values.put(CustomSQLHelper.KEY_LONGITUDE, mPosition.longitude);
			values.put(CustomSQLHelper.KEY_CITY, city);
			values.put(CustomSQLHelper.KEY_USER_ACCOUNT, Identificator.id(this));
			values.put(CustomSQLHelper.KEY_STATUS_USER_OBJECT, 0);
			values.put(CustomSQLHelper.KEY_ADMIN_COMMENT, "");
			getContentResolver().insert(
					LocalDatabaseContentProvider.CONTENT_URI_ADDRESS_USER,
					values);
			sendResult(remoteResult);
		} else {
			sendResult(0);
		}

	}

	private void updateCoordinats(int remoteResult) {
		if (remoteResult > 0) {
			ContentValues values = new ContentValues();
			values.put(CustomSQLHelper.KEY_ADDRESS, address);
			values.put(CustomSQLHelper.KEY_LATITUDE, mPosition.latitude);
			values.put(CustomSQLHelper.KEY_LONGITUDE, mPosition.longitude);
			values.put(CustomSQLHelper.KEY_CITY, city);
			values.put(CustomSQLHelper.KEY_STATUS_USER_OBJECT, 0);
			getContentResolver().update(
					LocalDatabaseContentProvider.CONTENT_URI_ADDRESS_USER,
					values,
					CustomSQLHelper.KEY_REMOTE_ID + "=" + idRemoteDatabase,
					null);
			sendResult(remoteResult);
		} else {
			sendResult(0);
		}
	}

	String address = null;
	String city = null;

	private void geocoder(LatLng position) {
		List<Address> addresses = null;

		Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
		try {
			addresses = geoCoder.getFromLocation(position.latitude,
					position.longitude, 1);
			if (addresses.size() > 0) {
				address = addresses.get(0).getAddressLine(0);
				city = addresses.get(0).getLocality();

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		// try add new wc remote database
		if (mAction.equals("append")) {
			//addNewPlace(address, city);
		} else {
			//updateCoordinats(idRemoteDatabase, mPosition, address, city);
		}

	}

	/*private void updateCoordinats(int idRemoteDatabase, LatLng newPosition,
			String newArrdess, String newCity) {
		int result = 0;
		try {
			try {
				mConnection = ConnectionDatabaseSingleton.getInstance()
						.getConnection(DatabaseConstants.URL_DATABASE_TEMP);
				if (mConnection != null) {
					mPreparedStatement = mConnection
							.prepareStatement(DatabaseConstants.UPDATE_USER_MARKET);
					mPreparedStatement.setDouble(1, newPosition.latitude);
					mPreparedStatement.setDouble(2, newPosition.longitude);
					mPreparedStatement.setString(3, newArrdess);
					mPreparedStatement.setString(4, newCity);
					mPreparedStatement.setInt(5, idRemoteDatabase);
					result = mPreparedStatement.executeUpdate();
				}

			} finally {
				if (mConnection != null)
					mConnection.close();
				if (mPreparedStatement != null)
					mPreparedStatement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// Update local data
		updateCoordinats(result);

	}
*/
	private void sendResult(int result) {

		if (mAction.equals("append")) {
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					this).setContentIntent(getPendingIntent()).setSmallIcon(
					R.drawable.notification_content_new);
			if (TextUtils.isEmpty(mValueObject)) {
				mBuilder.setContentTitle(mObject);
			} else {
				mBuilder.setContentTitle(mValueObject);
			}
			if (result > 0) {
				mBuilder.setContentText(getResources().getString(
						R.string.notification_add_data_result_ok));
				mBuilder.setAutoCancel(true);
				// тут добавить в локальную БД
			} else {
				mBuilder.setContentText(getResources().getString(
						R.string.notification_add_data_result_fail));

			}
			mNotificationManager.notify(idNotification, mBuilder.build());
		} else if (mAction.equals("update")) {
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					this);
			if (result > 0) {
				mBuilder.setTicker(getResources().getString(
						R.string.notification_update_coordinats_result_ok));
				mBuilder.setSmallIcon(com.appmobileos.freetoiletsrussiatrial.R.drawable.notification_refresh);

			} else {
				mBuilder.setContentTitle(getResources().getString(
						R.string.notification_update_result_fail));
				mBuilder.setContentText(getResources().getString(
						R.string.notification_update_coordinats_result_fail));
				mBuilder.setSmallIcon(R.drawable.notification_warning);
			}
			mBuilder.setContentIntent(getPendingIntent());
			mNotificationManager.notify(idNotificationUpdateCoordinats,
					mBuilder.build());
			if (result > 0)
				mNotificationManager.cancel(idNotificationUpdateCoordinats);

		}
		stopService(new Intent(this, UpdateRemoteService.class));
	}

}
