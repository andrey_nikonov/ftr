package com.appmobileos.freetoiletsrussiatrial.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class LocalDatabaseContentProvider extends ContentProvider {
	private static final String mAuthority = "com.appmobileos.localdatabaseprovider";
	private CustomSQLHelper mDatabaseHelper;
	private static final int ALLROWS_ADDRESS = 1;
	private static final int SINGLE_ROW_ADDRESS = 2;
	private static final int ALLROWS_USER_ADDRESS = 3;
	private static final int SINGLE_ROW_USER_ADDRESS = 4;
	private static final UriMatcher uriMatcher;
	public static final Uri CONTENT_URI_ADDRESS = Uri.parse("content://"
			+ mAuthority + "/addresses");
	public static final Uri CONTENT_URI_ADDRESS_USER = Uri.parse("content://"
			+ mAuthority + "/user_address");
	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(mAuthority, "addresses", ALLROWS_ADDRESS);
		uriMatcher.addURI(mAuthority, "addresses/#", SINGLE_ROW_ADDRESS);
		uriMatcher.addURI(mAuthority, "user_address", ALLROWS_USER_ADDRESS);
		uriMatcher
				.addURI(mAuthority, "user_address/#", SINGLE_ROW_USER_ADDRESS);

	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
		switch (uriMatcher.match(uri)) {
		case SINGLE_ROW_ADDRESS:
			String rowId = uri.getPathSegments().get(1);
			selection = CustomSQLHelper.KEY_ID
					+ "="
					+ rowId
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ")" : "");
			return db.delete(CustomSQLHelper.DATABASE_TABLE_CITY_WC_DEFAULT,
					selection, selectionArgs);
		case ALLROWS_USER_ADDRESS:
			int deleteCount = db.delete(
					CustomSQLHelper.DATABASE_TABLE_TEMP_USER_WC, selection,
					selectionArgs);
			getContext().getContentResolver().notifyChange(uri, null);
			return deleteCount;
		case ALLROWS_ADDRESS:
			int deleteCountDefaultAddress = db.delete(
					CustomSQLHelper.DATABASE_TABLE_CITY_WC_DEFAULT, selection,
					selectionArgs);
			getContext().getContentResolver().notifyChange(uri, null);
			return deleteCountDefaultAddress;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

	}

	@Override
	public String getType(Uri uri) {
		switch (uriMatcher.match(uri)) {
		case ALLROWS_ADDRESS:
			return "vnd.appmodileos.cursor.dir/vnd.appmodileos.addresses";
		case SINGLE_ROW_ADDRESS:
			return "vnd.appmobileos.cursor.item/vnd.appmodileos.addresses";
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}

	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
		switch (uriMatcher.match(uri)) {
		case ALLROWS_USER_ADDRESS:
			long rowID = db.insert(CustomSQLHelper.DATABASE_TABLE_TEMP_USER_WC,
					null, values);
			if (rowID > -1) {
				Uri uriInsert = ContentUris.withAppendedId(
						CONTENT_URI_ADDRESS_USER, rowID);
				getContext().getContentResolver().notifyChange(uriInsert, null);
				return uriInsert;
			} else {
				throw new SQLException("Failed to insert row into " + uri);
			}
		case ALLROWS_ADDRESS:
			long rowIDAddress = db.insert(
					CustomSQLHelper.DATABASE_TABLE_CITY_WC_DEFAULT, null,
					values);
			if (rowIDAddress > -1) {
				Uri uriInsert = ContentUris.withAppendedId(CONTENT_URI_ADDRESS,
						rowIDAddress);
				// No send update cursor - very long data - very long update,
				// update if service OK
				// getContext().getContentResolver().notifyChange(uri, null);
				return uriInsert;
			} else {
				throw new SQLException("Failed to insert row into " + uri);
			}
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public boolean onCreate() {
		mDatabaseHelper = new CustomSQLHelper(getContext(),
				CustomSQLHelper.DATABASE_NAME, null,
				CustomSQLHelper.DATABASE_VERSION);
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
		String groupBy = null;
		String having = null;
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		switch (uriMatcher.match(uri)) {
		case ALLROWS_ADDRESS:
			queryBuilder
					.setTables(CustomSQLHelper.DATABASE_TABLE_CITY_WC_DEFAULT);
			break;
		case SINGLE_ROW_ADDRESS:
			queryBuilder
					.setTables(CustomSQLHelper.DATABASE_TABLE_CITY_WC_DEFAULT);
			String rowID = uri.getPathSegments().get(1);
			queryBuilder.appendWhere(CustomSQLHelper.KEY_ID + "=" + rowID);
			break;
		case SINGLE_ROW_USER_ADDRESS:

			queryBuilder.setTables(CustomSQLHelper.DATABASE_TABLE_TEMP_USER_WC);
			queryBuilder.appendWhere(CustomSQLHelper.KEY_ID + "="
					+ uri.getPathSegments().get(1));
			break;
		case ALLROWS_USER_ADDRESS:
			queryBuilder.setTables(CustomSQLHelper.DATABASE_TABLE_TEMP_USER_WC);
			break;

		default:
			break;

		}
        Cursor cursor = queryBuilder.query(db, projection, selection,
				selectionArgs, groupBy, having, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int updateCount;
		SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();

		switch (uriMatcher.match(uri)) {
		case SINGLE_ROW_ADDRESS:
			String rowID = uri.getPathSegments().get(1);
			selection = CustomSQLHelper.KEY_ID
					+ "="
					+ rowID
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ " )" : "");
			updateCount = db.update(
					CustomSQLHelper.DATABASE_TABLE_CITY_WC_DEFAULT, values,
					selection, selectionArgs);
			getContext().getContentResolver().notifyChange(uri, null);
			return updateCount;
		case ALLROWS_USER_ADDRESS:
			updateCount = db.update(
					CustomSQLHelper.DATABASE_TABLE_TEMP_USER_WC, values,
					selection, selectionArgs);
			getContext().getContentResolver().notifyChange(uri, null);
			return updateCount;
		case ALLROWS_ADDRESS:
			updateCount = db.update(
					CustomSQLHelper.DATABASE_TABLE_CITY_WC_DEFAULT, values,
					selection, selectionArgs);
			getContext().getContentResolver().notifyChange(uri, null);
			return updateCount;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	public static class CustomSQLHelper extends SQLiteOpenHelper {
		// public static final String KEY_AUTO_REMOTE_ID = "id";
		public static final String KEY_REMOTE_ID = "remote_id";
		public static final String KEY_ID = "_id";
		public static final String KEY_ADDRESS = "address";
		public static final String KEY_PRICE = "price";
		public static final String KEY_OBJECT = "object";
		public static final String KEY_DESCRIPTION = "description";
		public static final String KEY_FIND_WC = "find_wc";
		public static final String KEY_LATITUDE = "latitude";
		public static final String KEY_LONGITUDE = "longitude";
		public static final String KEY_CITY = "city";
		public static final String KEY_DISTANCE_METRO = "distance_metro";
		public static final String KEY_LAST_UPDATE = "last_update";
		public static final String KEY_USER_ACCOUNT = "user_account";
		public static final String KEY_STATUS_USER_OBJECT = "status";
		public static final String KEY_ADMIN_COMMENT = "admin_comment";

		/* Database settings* */
		private static final String DATABASE_NAME = "wc_cities.db";
		private static final int DATABASE_VERSION = 1;
		public static final String DATABASE_TABLE_CITY_WC_DEFAULT = "citywcdefault";
		public static final String DATABASE_TABLE_TEMP_USER_WC = "tempuserwc";
		private static final String DATABASE_CREATE_MAIN_TABLE = "create table "
				+ DATABASE_TABLE_CITY_WC_DEFAULT
				+ " ( "
				+ KEY_ID
				+ " integer primary key autoincrement, "
				+ KEY_REMOTE_ID
				+ " integer, "
				+ KEY_ADDRESS
				+ " text, "
				+ KEY_PRICE
				+ " int, "
				+ KEY_OBJECT
				+ " text, "
				+ KEY_DESCRIPTION
				+ " text, "
				+ KEY_FIND_WC
				+ " text, "
				+ KEY_DISTANCE_METRO
				+ " text, "
				+ KEY_LATITUDE
				+ " double,"
				+ KEY_LONGITUDE
				+ " double,"
				+ KEY_CITY + " text," + KEY_LAST_UPDATE + " TIMESTAMP);";

		private static final String DATABSE_CREATE_USER_TEMP_TABLE = "create table "
				+ DATABASE_TABLE_TEMP_USER_WC
				+ " ( "
				+ KEY_ID
				+ " integer primary key autoincrement, "
				+ KEY_ADDRESS
				+ " text, "
				+ KEY_REMOTE_ID
				+ " integer, "
				+ KEY_PRICE
				+ " int, "
				+ KEY_OBJECT
				+ " text, "
				+ KEY_DESCRIPTION
				+ " text, "
				+ KEY_FIND_WC
				+ " text, "
				+ KEY_LATITUDE
				+ " double, "
				+ KEY_LONGITUDE
				+ " double, "
				+ KEY_CITY
				+ " text, "
				+ KEY_USER_ACCOUNT
				+ " text, "
				+ KEY_STATUS_USER_OBJECT
				+ " int, "
				+ KEY_ADMIN_COMMENT
				+ " text, " + KEY_LAST_UPDATE + "TIMESTAMP);";

		public CustomSQLHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE_MAIN_TABLE);
			db.execSQL(DATABSE_CREATE_USER_TEMP_TABLE);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF IT EXISTS "
					+ DATABASE_TABLE_CITY_WC_DEFAULT);
			db.execSQL("DROP TABLE IF IT EXISTS " + DATABASE_TABLE_TEMP_USER_WC);
			// Recreate main or user table
			onCreate(db);

		}

	}

}
