package com.appmobileos.freetoiletsrussiatrial.database;

import com.appmobileos.freetoiletsrussiatrial.database.LocalDatabaseContentProvider.CustomSQLHelper;

public class DatabaseConstants {
	/* Name columns from database wc, table CityWCDefault */
	public static final String TABLE_CITY = "CityWCDefault";
	public static final String TABLE_TEMP_USER_WC = "TempUserWC";
	public static final String AUTO_ID = "id";
	public static final String ADDRESS = "address";
	public static final String PRICE = "price";
	public static final String OBJECT = "object";
	public static final String DESCRIPTION = "description";
	public static final String FIND_WC = "find_wc";
	public static final String DISTANCE_METRO = "distance_metro";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String CITY = "city";
	public static final String LAST_UPDATE_DATA = "data";
	public static final String LAST_UPDATE_DATA2 = "last_update";
	public static final String USER_ACCOUNT = "user_account";
	public static final String ADMIN_COMMENT = "admin_comment";
	public static final String STATUS = "status";
	public static final String URL_DATABASE = "jdbc:mysql://mysql-1215.huid.hoster.ru:3306/huid1215_wc?characterEncoding=UTF-8";
	public static final String URL_DATABASE_TEMP = "jdbc:mysql://mysql-1215.huid.hoster.ru:3306/huid1215_temp_wc?characterEncoding=UTF-8";
	public static final String USERNAME = "huid1215_remote";
	public static final String PASSWORD = "REmOteG";
	public static final String SQL_DEFAULT_CITY = "SELECT * FROM " + TABLE_CITY
			+ " WHERE city =? ";
	public static final String SQL_SELECT_CITY_FROM_LIST = "SELECT DISTINCT city from "
			+ TABLE_CITY + " ORDER BY city";
	public static final String SQL_SELECT_TEMP_USER_WC = "SELECT * FROM "
			+ TABLE_TEMP_USER_WC + " WHERE " + USER_ACCOUNT + "=? AND "
			+ STATUS + "=0";
	public static final String SQL_SELECT_TEMP_USER_WC_LOG = "SELECT * FROM "
			+ TABLE_TEMP_USER_WC + " WHERE " + USER_ACCOUNT + "=? ORDER BY "
			+ LAST_UPDATE_DATA + " DESC";

	public static final String SQL_SELECT_TEMP_USER_WC_LOG_MODERATION = "SELECT * FROM "
			+ TABLE_TEMP_USER_WC
			+ " WHERE "
			+ USER_ACCOUNT
			+ "=? and "
			+ STATUS + "=0 ORDER BY " + LAST_UPDATE_DATA + " DESC";

	public static final String ADD_WC_TEMP_USER = "INSERT INTO "
			+ TABLE_TEMP_USER_WC + " ( " + ADDRESS + "," + PRICE + "," + OBJECT
			+ "," + DESCRIPTION + "," + FIND_WC + "," + LATITUDE + ","
			+ LONGITUDE + "," + CITY + "," + USER_ACCOUNT + "," + STATUS + ","
			+ ADMIN_COMMENT + "," + LAST_UPDATE_DATA
			+ " ) VALUE (?,?,?,?,?,?,?,?,?,?,?, NOW()) ";
	public static final String DELETE_USER_WC = "DELETE FROM "
			+ TABLE_TEMP_USER_WC + " WHERE " + AUTO_ID + "=?";
	public static final String UPDATE_USER_MARKET = "UPDATE "
			+ TABLE_TEMP_USER_WC + " SET " + LATITUDE + "=?" + "," + LONGITUDE
			+ "=? " + "," + ADDRESS + "=? " + "," + CITY + "=? " + " WHERE "
			+ AUTO_ID + "=?";
	public static final String INFO_WC = "Select * from " + TABLE_CITY
			+ " where " + ADDRESS + "=?";
	/** Select LOCAL DATABSE **/
	public static final String MAX_LAST_DATA_UPDATE = "Select max("
			+ LAST_UPDATE_DATA2 + ") AS " + LAST_UPDATE_DATA2 + " from "
			+ CustomSQLHelper.DATABASE_TABLE_TEMP_USER_WC;
	public static final String DATABASE_SQL_DATA_MORE_LAST_UPDATE = "Select * from  "
			+ TABLE_CITY
			+ " where last_update>? and "
			+ CustomSQLHelper.KEY_CITY + "=?";
	public static final String DATABASE_COUNT_REMOTE_ADDRESS = "Select id from "
			+ TABLE_CITY + " where " + CustomSQLHelper.KEY_CITY + "=?";
	public static final String OBJECT_PUBLIC = "Общественный";
	public static final String OBJECT_MARKET = "Торговый центр";
	public static final String OBJECT_KINO = "Кинотеатр";
	public static final String OBJECT_MACDONALDS = "Макдоналдс";
	public static final String OBJECT_BIO = "Биотуалет";
    public static final String OBJECT_FOOD = "Ресторан быстрого питания";
    public static final String OBJECT_OTHER = "Другое";

	// constants status user object
	public static final int USER_OBJECT_STATUS_MODERATION = 0;
	public static final int USER_OBJECT_STATUS_APPROVL = 1;
	public static final int USER_OBJECT_STATUS_REJECT = 2;

}
