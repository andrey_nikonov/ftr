package com.appmobileos.freetoiletsrussiatrial.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.database.LocalDatabaseContentProvider.CustomSQLHelper;
import com.appmobileos.freetoiletsrussiatrial.map.GoogleMaps2Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;


public class UpdateLocalService extends Service {
	private PendingIntent mPendingIntent;
	private ArrayList<Integer> mLocalIds;
	private Connection mConnection;
	private String lastUserUpdate = null;
	private String mCitySetting = null;
	private NotificationManager mNotificationManager;

	// unused
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null) {
            //Log.i(Tag.UpdateLocalService, "--- SERVICE START ---");
			mPendingIntent = intent
					.getParcelableExtra(GoogleMaps2Activity.PENDING_INTENT_EMULATION_ACTIVITY);
			lastUserUpdate = intent
					.getStringExtra(GoogleMaps2Activity.PENDING_INTENT_LAST_USER_UPDATE);
			mLocalIds = intent
					.getIntegerArrayListExtra(GoogleMaps2Activity.PENDING_INTENT_LOCAL_IDS);
			mCitySetting = intent
					.getStringExtra(GoogleMaps2Activity.PENDING_INTENT_CITY);
			// starting main thread
			mainProcessing();
			startNotification(getResources().getString(
					R.string.notification_update_data));
		}

		return Service.START_STICKY;
	}

	private int mIdNotification = 0;
	private Builder notification;

	private void startNotification(String mainAction) {
		notification = new NotificationCompat.Builder(this)
				.setSmallIcon(
						com.appmobileos.freetoiletsrussiatrial.R.drawable.notification_refresh)
				.setContentTitle(
						(lastUserUpdate == null) ? getResources().getString(
								R.string.notification_load_first_address)
								: mainAction)
				.setContentText(
						(lastUserUpdate == null) ? getResources().getString(
								R.string.notification_load_data_for_city)
								+ " " + mCitySetting + "..."
								: getResources()
										.getString(
												R.string.notification_update_data_for_city)
										+ " " + mCitySetting)
				.setContentIntent(getPendingIntent()).setProgress(0, 0, true);
		if (lastUserUpdate == null)
			notification.setTicker(mainAction);
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(mIdNotification, notification.build());

	}

	private PendingIntent getPendingIntent() {
		Intent intent = new Intent(this, GoogleMaps2Activity.class);
		TaskStackBuilder task = TaskStackBuilder.create(this);
		// task.addParentStack(GoogleMaps2Activity.class);
		task.addNextIntent(intent);
		return task.getPendingIntent(0, 0);

	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private void mainProcessing() {
		Thread thread = new Thread(null, doBackgroundThreadProcessing,
				"Background");
		thread.start();
	}

	private Runnable doBackgroundThreadProcessing = new Runnable() {

		@Override
		public void run() {
      //      Log.i(Tag.UpdateLocalService, "Переменная UpdateNow = " + mUpdateNow);
            if (lastUserUpdate == null || lastUserUpdate.equals(GoogleMaps2Activity.START_DATA_UPDATE_ADDRESS) && mLocalIds == null ) {
				backgroundThreadProcessingAppend();
			} else {
				backgroundThreadProcessingUpdate(mLocalIds);
			}

		}
	};
	private int mMaxFirstStart;

	/** This method call if last data update == null **/
	private void backgroundThreadProcessingAppend() {
		Connection mConnection = null;
		ResultSet mResultSet = null;
		PreparedStatement mPreparedStatement = null;
		String lastUpdate = null;
		int mCountAddressRemoteDatabase = 0;

		try {
			try {
				mConnection = getConnection();
				if (mConnection != null) {

					mPreparedStatement = mConnection
							.prepareStatement(DatabaseConstants.SQL_DEFAULT_CITY);
					// testing

					mPreparedStatement.setString(1, mCitySetting);
					mResultSet = mPreparedStatement.executeQuery();
					if (mResultSet != null) {
						mResultSet.last();
						mMaxFirstStart = mResultSet.getRow();
						mResultSet.beforeFirst();
					}
					while (mResultSet.next()) {
						mCountAddressRemoteDatabase++;
						String address = mResultSet
								.getString(DatabaseConstants.ADDRESS);
						int price = mResultSet.getInt(DatabaseConstants.PRICE);
						String object = mResultSet
								.getString(DatabaseConstants.OBJECT);
						String description = mResultSet
								.getString(DatabaseConstants.DESCRIPTION);
						String find_wc = mResultSet
								.getString(DatabaseConstants.FIND_WC);
						double latitude = mResultSet
								.getDouble(DatabaseConstants.LATITUDE);
						double longitude = mResultSet
								.getDouble(DatabaseConstants.LONGITUDE);
						String city = mResultSet
								.getString(DatabaseConstants.CITY);
						lastUpdate = mResultSet
								.getString(DatabaseConstants.LAST_UPDATE_DATA2);

						int remote_id = mResultSet
								.getInt(DatabaseConstants.AUTO_ID);
						addORUpdateNewData(true, remote_id, address, price,
								object, description, find_wc, "Нет данных",
								latitude, longitude, city, lastUpdate);
					}

				} else {
					mCountAddressRemoteDatabase = -1;
				}

			} finally {
				if (mConnection != null)
					mConnection.close();
				if (mPreparedStatement != null)
					mPreparedStatement.close();
				if (mResultSet != null)
					mResultSet.close();
			}
		} catch (SQLException e) {
			mCountAddressRemoteDatabase = -1;
			e.printStackTrace();
		}
		if (mCountAddressRemoteDatabase >= 0) {
			sendResult(GoogleMaps2Activity.UPDATE_SERVICE_RESULT_OK);
		} else {
			sendResult(GoogleMaps2Activity.UPDATE_SERVICE_RESULT_ERROR);
		}

	}

	/** This method calls when need update local database */
	private void backgroundThreadProcessingUpdate(ArrayList<Integer> mLocalIds) {
		Connection mConnection = null;
		ResultSet mResultSet = null;
		PreparedStatement mPreparedStatement = null;
		ArrayList<Integer> mRemoteIds = null;
		int mCountUpdateRows = 0;
		try {
			try {
				mConnection = getConnection();
				if (mConnection != null) {
					mPreparedStatement = mConnection
							.prepareStatement(DatabaseConstants.DATABASE_SQL_DATA_MORE_LAST_UPDATE);
					mPreparedStatement.setString(1, lastUserUpdate);
					mPreparedStatement.setString(2, mCitySetting);
					mResultSet = mPreparedStatement.executeQuery();
					mRemoteIds = new ArrayList<Integer>();
					while (mResultSet.next()) {
						mCountUpdateRows++;
						String address = mResultSet
								.getString(DatabaseConstants.ADDRESS);
						int price = mResultSet.getInt(DatabaseConstants.PRICE);
						String object = mResultSet
								.getString(DatabaseConstants.OBJECT);
						String description = mResultSet
								.getString(DatabaseConstants.DESCRIPTION);
						String find_wc = mResultSet
								.getString(DatabaseConstants.FIND_WC);
						double latitude = mResultSet
								.getDouble(DatabaseConstants.LATITUDE);
						double longitude = mResultSet
								.getDouble(DatabaseConstants.LONGITUDE);
						String city = mResultSet
								.getString(DatabaseConstants.CITY);
						String lastUpdate = mResultSet
								.getString(DatabaseConstants.LAST_UPDATE_DATA2);

						int remote_id = mResultSet
								.getInt(DatabaseConstants.AUTO_ID);
						mRemoteIds.add(remote_id);
						// update or inser new row
						checkDataOnID(mLocalIds, remote_id, address, price,
								object, description, find_wc, "Нет данных",
								latitude, longitude, city, lastUpdate);
					}

				} else {
					mCountUpdateRows = -1;
				}
			} finally {
				if (mConnection != null)
					mConnection.close();
				if (mPreparedStatement != null)
					mPreparedStatement.close();
				if (mResultSet != null)
					mResultSet.close();
			}
		} catch (SQLException e) {
			mCountUpdateRows = -1;
			e.printStackTrace();
		}
		if (mCountUpdateRows >= 0) {
			// check old data
			backgroundThreadProcessingDelete();
		} else {
			sendResult(GoogleMaps2Activity.UPDATE_SERVICE_RESULT_ERROR);
		}
	}

	private void backgroundThreadProcessingDelete() {
		Connection mConnection = null;
		ResultSet mResultSet = null;
		PreparedStatement mPreparedStatement = null;
		ArrayList<Integer> mRemoteIds = null;
		int mCountDeleteRows = 0;
		try {
			try {
				mConnection = getConnection();
				if (mConnection != null) {
					mPreparedStatement = mConnection
							.prepareStatement(DatabaseConstants.DATABASE_COUNT_REMOTE_ADDRESS);
					mPreparedStatement.setString(1, mCitySetting);
                    //Log.i(Tag.UpdateLocalService, mPreparedStatement.toString());
					mResultSet = mPreparedStatement.executeQuery();
					mRemoteIds = new ArrayList<Integer>();
					while (mResultSet.next()) {
						mCountDeleteRows++;
						int remote_id = mResultSet
								.getInt(DatabaseConstants.AUTO_ID);
						mRemoteIds.add(remote_id);

					}
					// Checking broken data and remote from local
					// database
					checkBrokenData(mLocalIds, mRemoteIds);
				} else {
					mCountDeleteRows = -1;
				}

			} finally {
				if (mConnection != null)
					mConnection.close();
				if (mPreparedStatement != null)
					mPreparedStatement.close();
				if (mResultSet != null)
					mResultSet.close();
			}
		} catch (SQLException e) {
			mCountDeleteRows = -1;
			e.printStackTrace();
		}
		if (mCountDeleteRows >= 0) {
			sendResult(GoogleMaps2Activity.UPDATE_SERVICE_RESULT_OK);
		} else {
			sendResult(GoogleMaps2Activity.UPDATE_SERVICE_RESULT_ERROR);
		}
	}

	private void checkDataOnID(ArrayList<Integer> mLocalIds, int remoteID,
			String address, int price, String object, String description,
			String find_wc, String distance_metro, double latitude,
			double longitude, String city, String lastUpdate)
			throws android.database.SQLException {
		if (mLocalIds.contains(remoteID)) {
			addORUpdateNewData(false, remoteID, address, price, object,
					description, find_wc, distance_metro, latitude, longitude,
					city, lastUpdate);
		} else {
			addORUpdateNewData(true, remoteID, address, price, object,
					description, find_wc, distance_metro, latitude, longitude,
					city, lastUpdate);
		}

	}

	private void checkBrokenData(ArrayList<Integer> mLocalIds,
			ArrayList<Integer> mRemoteIds) throws SQLException {
		if (mRemoteIds.size() == mLocalIds.size()) {
			return;
		}
		for (Integer localId : mLocalIds) {
			if (!mRemoteIds.contains(localId)) {
               // Log.i(Tag.UpdateLocalService, "Пришли чтобы удалить запись с id " + localId);
				getContentResolver().delete(
						LocalDatabaseContentProvider.CONTENT_URI_ADDRESS,
						CustomSQLHelper.KEY_REMOTE_ID + "=" + localId, null);
			}
		}

	}

	private int incr = 0;

	private void addORUpdateNewData(boolean actionInsert, int remoteID,
			String address, int price, String object, String description,
			String find_wc, String distance_metro, double latitude,
			double longitude, String city, String lastUpdate)
			throws android.database.SQLException {

		ContentResolver resolver = getContentResolver();
		ContentValues values = new ContentValues();
		values.put(CustomSQLHelper.KEY_REMOTE_ID, remoteID);
		values.put(LocalDatabaseContentProvider.CustomSQLHelper.KEY_ADDRESS,
				address);
		values.put(LocalDatabaseContentProvider.CustomSQLHelper.KEY_PRICE,
				price);
		values.put(LocalDatabaseContentProvider.CustomSQLHelper.KEY_OBJECT,
				object);
		values.put(
				LocalDatabaseContentProvider.CustomSQLHelper.KEY_DESCRIPTION,
				description);
		values.put(LocalDatabaseContentProvider.CustomSQLHelper.KEY_FIND_WC,
				find_wc);
		values.put(
				LocalDatabaseContentProvider.CustomSQLHelper.KEY_DISTANCE_METRO,
				distance_metro);
		values.put(LocalDatabaseContentProvider.CustomSQLHelper.KEY_LATITUDE,
				latitude);
		values.put(LocalDatabaseContentProvider.CustomSQLHelper.KEY_LONGITUDE,
				longitude);
		values.put(LocalDatabaseContentProvider.CustomSQLHelper.KEY_CITY, city);
		values.put(
				LocalDatabaseContentProvider.CustomSQLHelper.KEY_LAST_UPDATE,
				lastUpdate);
		if (actionInsert) {
			resolver.insert(LocalDatabaseContentProvider.CONTENT_URI_ADDRESS,
                    values);
			if (lastUserUpdate == null) {
				incr += 1;
				notification.setProgress(mMaxFirstStart, incr, false);
				mNotificationManager.notify(mIdNotification,
						notification.build());
			}

		} else {
			resolver.update(LocalDatabaseContentProvider.CONTENT_URI_ADDRESS,
                    values, CustomSQLHelper.KEY_REMOTE_ID + "=" + remoteID,
                    null);

		}
	}

	private Connection getConnection() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			mConnection = DriverManager.getConnection(
					DatabaseConstants.URL_DATABASE, DatabaseConstants.USERNAME,
					DatabaseConstants.PASSWORD);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return mConnection;
	}

	private void sendResult(int result) {
		if (mPendingIntent != null) {
			try {
				Intent intentResult = new Intent();
				mPendingIntent.send(this, result, intentResult);
			} catch (CanceledException e) {
				e.printStackTrace();
			}
		}
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setContentIntent(getPendingIntent());
		if (result == GoogleMaps2Activity.UPDATE_SERVICE_RESULT_OK) {
			mBuilder.setSmallIcon(R.drawable.notification_refresh);
			mBuilder.setTicker(getResources().getString(
					R.string.notification_ok_update_data));
		} else if (result == GoogleMaps2Activity.UPDATE_SERVICE_RESULT_ERROR) {
			mBuilder.setTicker(getResources().getString(
					R.string.notification_error_update_data));
			mBuilder.setContentTitle(getResources().getString(
					R.string.notification_update_result_fail));
			mBuilder.setContentText(getResources().getString(
					R.string.notification_error_update_data));
			mBuilder.setSmallIcon(R.drawable.notification_warning);
		}
		mNotificationManager.notify(mIdNotification, mBuilder.build());
		if (result == GoogleMaps2Activity.UPDATE_SERVICE_RESULT_OK)
			mNotificationManager.cancel(mIdNotification);

	}

}
