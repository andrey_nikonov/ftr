package com.appmobileos.freetoiletsrussiatrial.utils;

import android.util.Log;

import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by andrey on 17/09/13.
 */
public class InternetUtils {
    public static HttpURLConnection createHttpConnection(String dataSend) throws IOException {
        URL mUrl = new URL("http://192.168.1.15:8888/android");
        HttpURLConnection mConnection = (HttpURLConnection) mUrl.openConnection();
        mConnection.setRequestMethod("POST");
        mConnection.setUseCaches(false);
        mConnection.setDoOutput(true);
        mConnection.setDoInput(true);
        mConnection.setFixedLengthStreamingMode(dataSend.getBytes().length);
        mConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        mConnection.setRequestProperty("Content-Length", Integer.toString(dataSend.getBytes().length));
        mConnection.connect();
        OutputStream dataOS = new BufferedOutputStream(mConnection.getOutputStream());
        dataOS.write(dataSend.getBytes("UTF-8"));
        dataOS.flush();
        dataOS.close();
        Log.i("InternetUtils (createHttpConnection)", "Запрос который отправили " + dataOS.toString());
        return mConnection;
    }
    public static long daysLeft( String lastDateUpdate) {
        Date nowDate = Calendar.getInstance().getTime();
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm",
                Locale.getDefault());
         if (lastDateUpdate != null) {
            Date olderDate;
            try {
                olderDate = dataFormat.parse(lastDateUpdate);
                return ((nowDate.getTime() - olderDate.getTime()) / (1000 * 60 * 60 * 24));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return -1;

    }
}
