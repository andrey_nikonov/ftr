package com.appmobileos.freetoiletsrussiatrial.utils;

import android.view.animation.Animation;
/**
 * Created by andrey on 26/10/13.
 */
public class AnimationListenerAdapter implements Animation.AnimationListener {
    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
