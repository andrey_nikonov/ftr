package com.appmobileos.freetoiletsrussiatrial.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.appmobileos.freetoiletsrussiatrial.R;

/**
 * Created by andrey on 28/09/13.
 */
public class ImagesUtils {
    private static final int DEFAULT_MAP_ICON_WIDTH = 32;
    private static final int DEFAULT_MAP_ICON_HEIGHT = 32;

    public static Bitmap createIcon(MapIcons icon, Context context, int price) {
        int dpWidth = LocationUtils.convertToDip(DEFAULT_MAP_ICON_WIDTH, context);
        int dpHeight = LocationUtils.convertToDip(DEFAULT_MAP_ICON_HEIGHT, context);
        Paint currentPain = createIconPaint();
        Bitmap newIcon = Bitmap.createBitmap(dpWidth, dpHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newIcon);
        canvas.drawColor(Color.TRANSPARENT);
        canvas.drawCircle(dpWidth / 2, dpHeight / 2, dpWidth / 2 - 3, currentPain);
        canvas.drawCircle(dpWidth / 2, dpHeight / 2, dpWidth / 2 - 3, createStrokePaint(price));
        Bitmap image;
        switch (icon) {
            case FOOD:
                image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_map_holo_food);
                break;
            case CINEMA:
                image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_map_holo_cinema);
                break;
            case BIO:
                image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_map_holo_bio);
                break;
            case SHOP:
                image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_map_holo_shopping);
                break;
            default:
                image = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_map_holo_wc);
        }
        canvas.drawBitmap(image, (dpWidth / 2) / 2, (dpHeight / 2) / 2, currentPain);
        return newIcon;
    }

    public static Paint createIconPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        return paint;
    }

    public static Paint createStrokePaint(int price) {
        Paint paint = new Paint();
        paint.setColor(price == 0 ? Color.parseColor("#ff99cc00") : Color.parseColor("#ffff4444"));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setAntiAlias(true);
        return paint;
    }

    public enum MapIcons {DEFAULT, FOOD, CINEMA, BIO, SHOP}
}
