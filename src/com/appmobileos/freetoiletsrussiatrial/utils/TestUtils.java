package com.appmobileos.freetoiletsrussiatrial.utils;

import com.appmobileos.freetoiletsrussiatrial.modal.MarkerParcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.nikonov on 18.09.13.
 */
public class TestUtils {
    public static MarkerParcelable createTestMarker(int id,String address, String object, String description, String findWC, double lat, double lng) {
        MarkerParcelable marker = new MarkerParcelable();
        marker.setId(id);
        marker.setAddress(address);
        marker.setObject(object);
        marker.setDescription(description);
        marker.setFindWC(findWC);
        marker.setLatitude(lat);
        marker.setLongitude(lng);
        return marker;
    }

    public static List<MarkerParcelable> createMarkers() {
        List<MarkerParcelable> list = new ArrayList<MarkerParcelable>();
        list.add(createTestMarker(1,"Dvincev 12", "Cloud","Shop", "when true", 55.796223, 37.59984));
        list.add(createTestMarker(2, "Dvincev 33", "Cloud2","wc", "HI! it's test data", 55.753585, 37.620905));
        list.add(createTestMarker(3, "Dvincev 44", "Cloud3","mac", "mother like to cook", 55.767686, 37.600918));
        list.add(createTestMarker(4, "Dvincev 55", "Cloud4","BC", "all", 55.765417, 37.605984));
        list.add(createTestMarker(5, "Dvincev 666", "Cloud5","medic", "Moscow", 55.612329, 37.745205));
        return list;
    }
}
