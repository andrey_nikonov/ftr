package com.appmobileos.freetoiletsrussiatrial.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.util.TypedValue;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.database.DatabaseConstants;
import com.appmobileos.freetoiletsrussiatrial.map.ICollectionIcons;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerMain;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerParcelable;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerUser;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrey on 08/09/13.
 */
public class LocationUtils {
    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    // Milliseconds per second
    public static final int MILLISECONDS_PER_SECOND = 1000;
    // The update interval
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // A fast interval ceiling
    public static final int FAST_CEILING_IN_SECONDS = 1;
    // Update interval in milliseconds
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // A fast ceiling of update intervals, used when the app is visible
    public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS =
            MILLISECONDS_PER_SECOND * FAST_CEILING_IN_SECONDS;

    public static ArrayList<MarkerParcelable> parseJsonFile(BufferedInputStream jsFile) {
        JsonFactory factory;
        JsonParser jsonParser = null;
        ArrayList<MarkerParcelable> returnMarkers;
        returnMarkers = new ArrayList<MarkerParcelable>();
        try {
            try {
                factory = new JsonFactory();
                jsonParser = factory.createJsonParser(jsFile);
                MarkerParcelable markerDescription = null;
                while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                    String token = jsonParser.getCurrentName();
                    JsonToken currentToken = jsonParser.getCurrentToken();
                    Log.i("LocationUtils", "JSON TOKEN = " + currentToken);
                    Log.i("LocationUtils", "Token name = " + token);
                    if (DatabaseConstants.AUTO_ID.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        markerDescription = new MarkerParcelable();
                        //markerDescription.setmIdRemoteDatabase(jsonParser.getIntValue());
                    } else if (DatabaseConstants.ADDRESS.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setAddress(jsonParser.getText());
                    } else if (DatabaseConstants.PRICE.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setPrice(jsonParser.getNumberValue().intValue());
                    } else if (DatabaseConstants.OBJECT.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setObject(jsonParser.getText());
                    } else if (DatabaseConstants.DESCRIPTION.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setDescription(jsonParser.getText());
                    } else if (DatabaseConstants.FIND_WC.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setFindWC(jsonParser.getText());
                    } else if (DatabaseConstants.LATITUDE.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setLatitude(jsonParser.getNumberValue().doubleValue());
                    } else if (DatabaseConstants.LONGITUDE.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setLongitude(jsonParser.getDoubleValue());
                    } else if (DatabaseConstants.CITY.equalsIgnoreCase(token) && currentToken != JsonToken.FIELD_NAME) {
                        if (markerDescription != null)
                            markerDescription.setCity(jsonParser.getText());
                    }
                    if (currentToken == JsonToken.END_OBJECT) {
                        returnMarkers.add(markerDescription);
                    }
                }

            } finally {
                if (jsonParser != null) jsonParser.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnMarkers;
    }

    public static ArrayList<MarkerParcelable> parseJson2(BufferedInputStream jsFile) {
        ArrayList<MarkerParcelable> markers = null;
        try {
            try {
                ObjectMapper mapper = new ObjectMapper();
                markers = mapper.readValue(jsFile, new TypeReference<List<MarkerMain>>() {
                });
            } finally {
                jsFile.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return markers;
    }

    public static String createJsonNewPlace(MarkerUser marker) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputStream, marker);
        return outputStream.toString("UTF-8");
    }

    public static int convertToDip(int value, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value,
                context.getResources().getDisplayMetrics());
    }

    public static BitmapDescriptor selectImagePlace(ICollectionIcons mMapIcons, Context context, String userObject, int price) {
        BitmapDescriptor drawable;
        Bitmap currentIcon;
        if (mMapIcons != null) {
            if (userObject.equals(DatabaseConstants.OBJECT_MACDONALDS)
                    || userObject.equals(context.getString(R.string.mcdonalds))) {
                currentIcon = mMapIcons.getIcon(ImagesUtils.MapIcons.FOOD, price);
            } else if (userObject.equals(DatabaseConstants.OBJECT_KINO)
                    || userObject.equals(context.getString(R.string.cinema))) {
                currentIcon = mMapIcons.getIcon(ImagesUtils.MapIcons.CINEMA, price);
            } else if (userObject.equals(DatabaseConstants.OBJECT_MARKET)
                    || userObject.equals(context.getString(R.string.market))) {
                currentIcon = mMapIcons.getIcon(ImagesUtils.MapIcons.SHOP, price);
            } else if (userObject.equals(DatabaseConstants.OBJECT_BIO)
                    || userObject.equals(context.getString(R.string.bio))) {
                currentIcon = mMapIcons.getIcon(ImagesUtils.MapIcons.BIO, price);
            } else if (userObject.equals(DatabaseConstants.OBJECT_FOOD)
                    || userObject.equals(context.getString(R.string.food))) {
                currentIcon = mMapIcons.getIcon(ImagesUtils.MapIcons.FOOD, price);
            } else {
                currentIcon = mMapIcons.getIcon(ImagesUtils.MapIcons.DEFAULT, price);
            }
        } else {
            currentIcon = ImagesUtils.createIcon(ImagesUtils.MapIcons.DEFAULT, context, 0);
        }
        drawable = BitmapDescriptorFactory.fromBitmap(currentIcon);
        return drawable;
    }

    public static String calculateDistance(MarkerParcelable marker, Location myLocation) {
        String distanceFinal;
        if (marker == null || myLocation == null) {
            return "";
        }
        Location markerLocation = new Location("my");
        markerLocation.setLatitude(marker.getLatitude());
        markerLocation.setLongitude(marker.getLongitude());
        float distance = myLocation.distanceTo(markerLocation);
        if (distance < 1000) {
            distanceFinal = Integer.toString((int) distance) + " м.";
        } else {
            distanceFinal = Integer.toString((int) distance / 1000) + " км."
                    + Integer.toString((int) distance % 1000) + " м.";
        }
        return distanceFinal;
    }

    public static boolean isSystemLocationSettingsEnable(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

}
