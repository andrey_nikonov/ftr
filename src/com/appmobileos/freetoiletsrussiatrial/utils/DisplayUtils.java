package com.appmobileos.freetoiletsrussiatrial.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * Created by andrey on 06/10/13.
 */
public class DisplayUtils {
    private static final String TAG = "DisplayUtils";

    private static DisplayMetrics getDisplayInfo(Context context) {
        return context.getResources().getDisplayMetrics();
    }
    public static int getMaxHeightWindowInfoMarker(Context context) {
        DisplayMetrics display = getDisplayInfo(context);
        Log.i(TAG, "DISPLAY heightPixels = " + display.heightPixels + " widthPixels = " + display.widthPixels + " display density = "
                + display.density + "display densityDPI  = " + display.densityDpi);
        return (int) ((display.heightPixels * 0.7));
    }
    public static int getMaxDisplayHeight(Context context) {
        DisplayMetrics display = getDisplayInfo(context);
        Log.i(TAG, "DISPLAY heightPixels = " + display.heightPixels + " widthPixels = " + display.widthPixels + " display density = "
                + display.density + "display densityDPI  = " + display.densityDpi);
        return display.heightPixels;
    }
    public static int getMaxDisplayWidth(Context context) {
        DisplayMetrics display = getDisplayInfo(context);
        Log.i(TAG, "DISPLAY heightPixels = " + display.heightPixels + " widthPixels = " + display.widthPixels + " display density = "
                + display.density + "display densityDPI  = " + display.densityDpi);
        return display.widthPixels;
    }
    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static int getMinHeightGoogleMap(Context context) {
        DisplayMetrics display = getDisplayInfo(context);
        int minHeight = (int) ((display.heightPixels * 0.3));
        return minHeight;
    }

    public static boolean isPortrait(Context context) {
        int orientation = context.getResources().getConfiguration().orientation;
        return orientation == Configuration.ORIENTATION_PORTRAIT;
    }
    public static int getStatusBarHeight(Context contex) {
        int result = 0;
        Resources res = contex.getResources();
        if (res != null) {
            int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = res.getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }
}
