package com.appmobileos.freetoiletsrussiatrial.modal;


import android.os.Parcel;
import android.os.Parcelable;

import stixiya.freetoilets.modal.Marker;

/**
 * Created by andrey on 12/09/13.
 */
public class MarkerParcelable extends Marker implements Parcelable {
    public static final Parcelable.Creator<MarkerParcelable> CREATOR
            = new Parcelable.Creator<MarkerParcelable>() {
        public MarkerParcelable createFromParcel(Parcel in) {
            return new MarkerParcelable(in);
        }

        public MarkerParcelable[] newArray(int size) {
            return new MarkerParcelable[size];
        }
    };
    private int id;
    private String address;
    private int price;
    private String object;
    private String description;
    private String findWC;
    private double latitude;
    private double longitude;
    private String city;
    private int user_opinion;

    public MarkerParcelable() {

    }

    protected MarkerParcelable(Parcel parcel) {
        id = parcel.readInt();
        address = parcel.readString();
        price = parcel.readInt();
        object = parcel.readString();
        description = parcel.readString();
        findWC = parcel.readString();
        latitude = parcel.readDouble();
        longitude = parcel.readDouble();
        city = parcel.readString();
        user_opinion = parcel.readInt();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;

    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String getObject() {
        return object;
    }

    @Override
    public void setObject(String object) {
        this.object = object;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getFindWC() {
        return findWC;
    }

    @Override
    public void setFindWC(String findWC) {
        this.findWC = findWC;

    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeInt(id);
        parcel.writeString(address);
        parcel.writeInt(price);
        parcel.writeString(object);
        parcel.writeString(description);
        parcel.writeString(findWC);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeString(city);
        parcel.writeInt(user_opinion);

    }



    public int getUserOpinion() {
        return user_opinion;
    }


    public void setUserOpinion(int user_opinion) {
        this.user_opinion = user_opinion;
    }


    @Override
    public String toString() {
        return "MarkerParcelable{" +
                "id=" + id +
                '}';
    }
}
