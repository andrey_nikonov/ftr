package com.appmobileos.freetoiletsrussiatrial.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by andrey on 13/09/13.
 */
public class MarkerMain extends MarkerParcelable {
    public static final Parcelable.Creator<MarkerMain> CREATOR
            = new Parcelable.Creator<MarkerMain>() {
        public MarkerMain createFromParcel(Parcel in) {
            return new MarkerMain(in);
        }

        public MarkerMain[] newArray(int size) {
            return new MarkerMain[size];
        }
    };

    private MarkerMain(Parcel parcel) {
        super(parcel);
    }
    /**
     * Using JSON
     */
    public MarkerMain() {

    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        super.writeToParcel(parcel, flag);
   }


}
