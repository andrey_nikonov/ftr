package com.appmobileos.freetoiletsrussiatrial.modal;

import android.os.Parcel;
import android.os.Parcelable;



/**
 * Created by andrey on 13/09/13.
 */
public class MarkerUser extends MarkerParcelable  {
    public static final Parcelable.Creator<MarkerUser> CREATOR
            = new Parcelable.Creator<MarkerUser>() {
        public MarkerUser createFromParcel(Parcel in) {
            return new MarkerUser(in);
        }

        public MarkerUser[] newArray(int size) {
            return new MarkerUser[size];
        }
    };
    private int idMain;
    private int status;
    private String userAccount;
    private String adminComment;

    public MarkerUser() {
    }

    protected MarkerUser(Parcel parcel) {
        super(parcel);
        idMain = parcel.readInt();
        status = parcel.readInt();
        userAccount = parcel.readString();
        adminComment = parcel.readString();
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flag) {
        super.writeToParcel(parcel, flag);
        parcel.writeInt(idMain);
        parcel.writeInt(status);
        parcel.writeString(userAccount);
        parcel.writeString(adminComment);
    }

    public int getIdMain() {
        return idMain;
    }

    public void setIdMain(int idMain) {
        this.idMain = idMain;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getAdminComment() {
        return adminComment;
    }

    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }
}
