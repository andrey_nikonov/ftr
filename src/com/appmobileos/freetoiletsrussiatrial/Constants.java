package com.appmobileos.freetoiletsrussiatrial;

/**
 * Created by andrey on 13/09/13.
 */
public class Constants {
    public static final String APP_PACKAGE = "com.appmobileos.freetoiletsrussiatrial";
    public static final String KEY_CURRENT_MARKER = APP_PACKAGE + "key_current_marker";
    public static final String KEY_CURRENT_LIST_MARKERS = APP_PACKAGE + "key_current_list_markers";
    public static final String KEY_CURRENT_LIST_MARKERS_USER = APP_PACKAGE + "key_current_list_markers_user";
    public static final String KEY_CURRENT_LOCATION = APP_PACKAGE + "key_current_location";
    public static final String KEY_RADIUS = APP_PACKAGE + ".radius";
    public static final String ACTION_SEND_FIND_MARKERS = APP_PACKAGE + "send_find_markers";
    public static final String KEY_CURRENT_SELECTED_ITEM = APP_PACKAGE + "cur_select_item";
    public static final String KEY_MAP_USER_AND_MAP_MARKERS = APP_PACKAGE + "user_and_map";
    public static final String KEY_HEIGHT_INFO_WINDOW = APP_PACKAGE + "height.window";

}
