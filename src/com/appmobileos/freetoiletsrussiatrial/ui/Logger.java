package com.appmobileos.freetoiletsrussiatrial.ui;

import android.util.Log;

import com.appmobileos.freetoiletsrussiatrial.BuildConfig;

/**
 * Created by andrey on 24.11.15.
 */
public class Logger {

    public static boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    public static void logDebug(String tag, String message) {
        Log.d(tag, message);
    }
}
