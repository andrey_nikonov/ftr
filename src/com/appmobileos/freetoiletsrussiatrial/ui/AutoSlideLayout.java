package com.appmobileos.freetoiletsrussiatrial.ui;


import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import java.util.ArrayList;

/**
 * Created by a.nikonov on 07.11.13.
 */
public class AutoSlideLayout extends RelativeLayout {
    private static final String TAG = "AutoSlideLayout";
    private static final int DEFAULT_ANIMATION_DURATION = 300;
    private int mDuration;
    private Context mContext;
    private Animator mAnimator;
    private AnimatorSet mAnimatorTwoAnimation;
    private ArrayList<AnimatorListener> mObservers = new ArrayList<AnimatorListener>();

    public AutoSlideLayout(Context context) {
        super(context);
        initParams(context);
    }

    public AutoSlideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initParams(context);
    }

    public AutoSlideLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initParams(context);
    }

    private void initParams(Context context) {
        mContext = context;
        Resources res = mContext.getResources();
        if (res != null) {
            mDuration = res.getInteger(android.R.integer.config_shortAnimTime);
        } else {
            mDuration = DEFAULT_ANIMATION_DURATION;
        }
    }

    private Animator hide() {
        return ObjectAnimator.ofFloat(this, "translationY", 0, -getHeight());
    }

    private Animator show() {
        return ObjectAnimator.ofFloat(this, "translationY", -getHeight(), 0);
    }

    public void showPanel() {
        if (getVisibility() == View.VISIBLE) {
            twoAnimation();
        } else {
            oneAnimator(PanelActions.SHOW);
        }
    }

    public void hidePanel() {
        oneAnimator(PanelActions.HIDE);
    }

    public boolean isVisible() {
        if (getVisibility() == View.VISIBLE) return true;
        return false;
    }

    private void oneAnimator(final PanelActions actions) {
        if (mAnimator != null && mAnimator.isRunning()) return;
        Log.i(TAG, "animationActionPanel and params show = " + actions);
        switch (actions) {
            case SHOW:
                mAnimator = show();
                break;
            case HIDE:
                mAnimator = hide();
                break;
        }
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                switch (actions) {
                    case SHOW:
                        setVisibility(View.VISIBLE);
                        break;
                }
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationStart(animation);
                }
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationStart(animation,AnimatorQueue.EMPTY, actions);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                switch (actions) {
                    case HIDE:
                        setVisibility(View.GONE);
                        for (AnimatorListener observer : mObservers) {
                            observer.clearBodySlidePanel();
                        }
                        break;
                }
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationEnd(animation, AnimatorQueue.EMPTY, actions);
                }
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationEnd(animation);
                }
                mAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                switch (actions) {
                    case HIDE:
                        setVisibility(View.GONE);
                        break;
                }
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationCancel(animation);
                }
                mAnimator = null;

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationRepeat(animation);
                }
            }
        });
        mAnimator.setDuration(mDuration);
        mAnimator.start();
    }

    private void twoAnimation() {
        if (mAnimatorTwoAnimation != null && mAnimatorTwoAnimation.isRunning()) return;
        Animator hideAP = hide();
        hideAP.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationStart(animation);
                }
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationStart(animation,AnimatorQueue.NEXT, PanelActions.HIDE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationEnd(animation, AnimatorQueue.NEXT, PanelActions.HIDE);
                }
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationEnd(animation);
                }
                for (AnimatorListener observer : mObservers) {
                    observer.reconfigureSlidePanel();
                }
                mAnimatorTwoAnimation = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationCancel(animation);
                }
                mAnimatorTwoAnimation = null;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                for (AnimatorListener observer : mObservers) {
                    observer.onAnimationRepeat(animation);
                }
            }
        });
        mAnimatorTwoAnimation = new AnimatorSet();
        AnimatorSet.Builder builder = mAnimatorTwoAnimation.play(hideAP);
        if (builder != null) builder.before(show());
        mAnimatorTwoAnimation.setDuration(mDuration);
        mAnimatorTwoAnimation.start();
    }

    public void registerObserver(AnimatorListener observer) {
        mObservers.add(observer);
    }

    public void unregisterObserver(AnimatorListener observer) {
        int index = mObservers.indexOf(observer);
        if (index != -1) {
            mObservers.remove(index);
        }
    }

    public enum AnimatorQueue {
        EMPTY, NEXT
    }

    public enum PanelActions {
        SHOW, HIDE
    }

    public interface AnimatorListener extends Animator.AnimatorListener {
        void onAnimationEnd(Animator animation, AnimatorQueue queue, PanelActions status);

        void onAnimationStart(Animator animation, AnimatorQueue queue, PanelActions status);

        void clearBodySlidePanel();

        void reconfigureSlidePanel();
    }

}
