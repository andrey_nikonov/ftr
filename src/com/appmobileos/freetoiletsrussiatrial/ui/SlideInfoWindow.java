package com.appmobileos.freetoiletsrussiatrial.ui;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.OverScroller;
import android.widget.RelativeLayout;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.utils.AnimationListenerAdapter;
import com.appmobileos.freetoiletsrussiatrial.utils.DisplayUtils;

/**
 * Created by andrey on 13/09/13.
 */
public class SlideInfoWindow extends RelativeLayout {
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final String TAG = "SlideInfoWindow";
    private final GestureDetector.SimpleOnGestureListener mGestureListener
            = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                //  Log.i(getClass().getName(), "swipe MAXOFFPATH  " +Math.abs(event1.getX() - event2.getX())  + "МОЕ ЗНАЧЕНИЕ" + swipeMaxOffPath);
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Log.i(getClass().getName(), "left swipe");
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    Log.i(getClass().getName(), "Right Swipe");
                } else if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                    mCallback.onActionTop();
                    Log.i(getClass().getName(), "swipe ВВЕРХ");
                } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                    actionHideInfoWindow();
                    Log.i(getClass().getName(), "swipe ВНИЗ");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.i(TAG, "onSingleTapConfirmed getHeight =" + getHeight());
            if (getHeight() != mSmallSizeInfoWindow) {
                actionHideInfoWindow();
            } else {
                mCallback.onActionTop();
            }
            return super.onSingleTapConfirmed(e);
        }
    };
    private int mMaxHeightPortrait = 0;
    private int mSmallSizeInfoWindow = 0;
    private int mMaxHeightLandscape = 0;
    private int mMinHeightGoogleMap;
    private OverScroller mScroller;
    private GestureDetectorCompat mGestureDetector;
    private int mSpeed;
    private int mTargetHeight;
    private Context mContext;
    private onAnimationSlideInfoWindow mCallback;
    private View mShadow;
    private boolean mStatusVisible;
    public SlideInfoWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SlideInfoWindow, 0, 0);
        if (typedArray != null) {
            mSpeed = typedArray.getInt(R.styleable.SlideInfoWindow_speed, 300);
        }
        initParams(context);
    }

    public SlideInfoWindow(Context context) {
        super(context);
        initParams(context);
    }

    private void initParams(Context context) {
        mCallback = (onAnimationSlideInfoWindow) context;
        mGestureDetector = new GestureDetectorCompat(context, mGestureListener);
        mScroller = new OverScroller(context);
        mContext = context;
        initInfoWindowSize();
    }

    public void setShadow(View shadow) {
        this.mShadow = shadow;
    }

    private void initInfoWindowSize() {
        Log.i(TAG, "INIT INFO WINDOW PORTRATE = " + DisplayUtils.isPortrait(mContext));
        if (DisplayUtils.isPortrait(mContext)) {
            mMaxHeightPortrait = DisplayUtils.getMaxHeightWindowInfoMarker(mContext);
        } else {
            mMaxHeightLandscape = DisplayUtils.getMaxDisplayHeight(mContext) - DisplayUtils.getStatusBarHeight(mContext);
            Log.i(TAG, "INIT INFO WINDOW LANDSCAPE = " + mMaxHeightLandscape + "STATUS BAR SIZE = " + DisplayUtils.getStatusBarHeight(mContext));
        }
        mSmallSizeInfoWindow = mContext.getResources().getDimensionPixelOffset(R.dimen.view_pager_header_container);

        //  mMinHeightGoogleMap = DisplayUtils.getMinHeightGoogleMap(mContext);

    }

    public void toggle(int targetHeightDipConvert, SwipeAction action) {
        mTargetHeight = targetHeightDipConvert;
        Log.i(getClass().getName(), "START \n TARGET VALUE = " + targetHeightDipConvert + " CURRENT ACTION = " + action + " PROGRAM HEIGHT " + getHeight());
        switch (action) {
            case TOP:
                setNewLayoutParamY(mTargetHeight);
                executeAnimation(targetHeightDipConvert - getHeight(), 0, SwipeAction.TOP);
                break;
            case BACK:
                executeAnimation(0, targetHeightDipConvert, SwipeAction.BACK);
                break;
            default:
                throw new UnsupportedOperationException(" Unsupported action with name = " + action);
        }
    }

    public void setNewLayoutParamY(int newYParam) {
        RelativeLayout.LayoutParams currentParams = (RelativeLayout.LayoutParams) getLayoutParams();
        if (currentParams != null) {
            currentParams.height = newYParam;
            setLayoutParams(currentParams);
            if (newYParam < 0) {
                hideShadow();
            } else {
                showShadow();
            }
        }
        Log.i(getClass().getName(), " setNewLayoutParamY \n PROGRAM HEIGHT " + getHeight() + "NEW VALUE = " + newYParam);
    }

    public void hideShadow() {
        if (mShadow != null) {
            mShadow.setVisibility(View.GONE);
        }
    }

    public boolean isVisible() {
        return getHeight() > 0;
    }

    private void showShadow() {
        if (mShadow != null) {
            Log.i(TAG, "SHOW SHADOW");
            mShadow.setVisibility(View.VISIBLE);
        }
    }

    private void executeAnimation(final int fromYDelta, final int toYDelta, final SwipeAction action) {
        Log.i(TAG, "executeAnimation fromYDelta = " + fromYDelta + " toYDelta = " + toYDelta + " action =" + action);
        TranslateAnimation mTranslateAnimation = new TranslateAnimation(0.0f, 0.0f, fromYDelta, toYDelta);
        mTranslateAnimation.setDuration(mSpeed);
        mTranslateAnimation.setInterpolator(new AccelerateInterpolator(1.0f));
        mTranslateAnimation.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                if (action == SwipeAction.BACK) {
                    int newHeight = getHeight() - mTargetHeight;
                    setNewLayoutParamY(newHeight);
                    mCallback.onActionBack(newHeight);
                    Log.i(TAG, "onAnimationEnd PROGRAM HEIGHT = " + getHeight() + "mTarget =  " + mTargetHeight + "NEW VIEW = " + newHeight);
                   if (newHeight <= 0) {
                       mCallback.changeStatusVisible(false);
                   }
                }
                clearAnimation();
            }
        });
        startAnimation(mTranslateAnimation);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.i("SlideInfoWindow", "onTouchEvent");
        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }



    public void actionHideInfoWindow() {
        Log.i(TAG, "actionHideInfoWindow getHeight = " + getHeight() + "mMaxHEIGHT_PORTRAIT = " + mMaxHeightPortrait + " mMaxHEIGHT_LANDSCAPE = " + mMaxHeightLandscape);
        final int smallWindowInfoMarker;
        if (DisplayUtils.isPortrait(mContext)) {
            smallWindowInfoMarker = mMaxHeightPortrait - mSmallSizeInfoWindow;
        } else {
            smallWindowInfoMarker = mMaxHeightLandscape - mSmallSizeInfoWindow;
        }
        /*if (getHeight() - smallWindowInfoMarker <= 0) {
            mStatusVisible = false;
            mCallback.changeStatusVisible(false);
        }*/
        toggle(smallWindowInfoMarker, SwipeAction.BACK);
    }

    public boolean isStatusVisible() {
        return mStatusVisible;
    }

    public void actionShowInfoWindow() {
        mStatusVisible = true;
        mCallback.changeStatusVisible(mStatusVisible);
        if (!isSmallSizeVisible()) {
            toggle(mSmallSizeInfoWindow, SwipeAction.TOP);
            showShadow();
        } else {
            if (DisplayUtils.isPortrait(mContext)) {
                toggle(mMaxHeightPortrait, SwipeAction.TOP);
                showShadow();
            } else {
                toggle(mMaxHeightLandscape, SwipeAction.TOP);
                hideShadow();
            }
        }
    }

    public boolean isSmallSizeVisible() {
        return getHeight() == mSmallSizeInfoWindow;
    }

    public int calculateDifferentDistance(Point placeLocation) {
        Log.i(TAG, "calculateDifferentDistance placeY = " + placeLocation.y
                + " mMin =" + mMinHeightGoogleMap +
                " mMax =" + mMaxHeightPortrait);
        if (placeLocation.y < mMinHeightGoogleMap) {
            return 0;
        } else {
            Log.i(TAG, "calculateDifferentDistance RETURN = " + (mMaxHeightPortrait - placeLocation.y));
            int result = mMaxHeightPortrait - placeLocation.y;
            if (result > mMinHeightGoogleMap) {
                result -= mMinHeightGoogleMap / 2;
            }
            return result > 0 ? result : mMaxHeightPortrait;
        }

    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superStatue = super.onSaveInstanceState();
        SavedState ss = new SavedState(superStatue);
        //Log.i(TAG, "onSaveInstanceState INFO WINDOW PORTRATE = " + DisplayUtils.isPortrait(mContext) + "SHADOW HEIGTH = " + mShadow.getHeight());
        ss.currentHeight = getHeight();
        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        int restoredHeight = ss.currentHeight;
        Log.i(TAG, "RESTORE VALUE HEIGHT  = " + mMaxHeightPortrait);
        if (restoredHeight == 0 || restoredHeight == mSmallSizeInfoWindow) {
            setNewLayoutParamY(restoredHeight);
        } else {
            mCallback.onFullScreenUsed();
            if (DisplayUtils.isPortrait(mContext)) {
                Log.i(TAG, "RESTORE VALUE HEIGHT PORTRAIT  = " + mMaxHeightPortrait);
                setNewLayoutParamY(mMaxHeightPortrait);
            } else {
                Log.i(TAG, "RESTORE VALUE HEIGHT  LANDSCAPE = " + mMaxHeightLandscape);
                setNewLayoutParamY(mMaxHeightLandscape);

            }
        }

        if (!DisplayUtils.isPortrait(mContext) && restoredHeight != mSmallSizeInfoWindow) {
            hideShadow();
        }
    }

    public enum SwipeAction {TOP, BACK}

    public interface onAnimationSlideInfoWindow {
        public void onActionBack(int currentHeight);

        public void onActionTop();

        public void onFullScreenUsed();

        public void changeStatusVisible(boolean show);

    }

    static class SavedState extends BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
        private int currentHeight;

        SavedState(Parcelable superState) {
            super(superState);
        }

        SavedState(Parcel source) {
            super(source);
            this.currentHeight = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(this.currentHeight);
        }
    }
}
