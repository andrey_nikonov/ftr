package com.appmobileos.freetoiletsrussiatrial.ui;

import com.nineoldandroids.animation.Animator;

/**
 * Created by andrey on 09/11/13.
 */
public class AutoSlideLayoutAdapter implements AutoSlideLayout.AnimatorListener {
    @Override
    public void onAnimationEnd(Animator animation, AutoSlideLayout.AnimatorQueue queue, AutoSlideLayout.PanelActions status) {

    }

    @Override
    public void clearBodySlidePanel() {

    }

    @Override
    public void reconfigureSlidePanel() {

    }

    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationEnd(Animator animator) {

    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }

    @Override
    public void onAnimationStart(Animator animation, AutoSlideLayout.AnimatorQueue queue, AutoSlideLayout.PanelActions status) {

    }
}
