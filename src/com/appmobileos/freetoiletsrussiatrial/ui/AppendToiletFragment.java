
package com.appmobileos.freetoiletsrussiatrial.ui;

import android.app.Activity;
import android.content.res.Resources;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import com.appmobileos.freetoiletsrussiatrial.database.DatabaseConstants;
import com.appmobileos.freetoiletsrussiatrial.map.GeocoderCustom;
import com.appmobileos.freetoiletsrussiatrial.map.GoogleMaps2Activity;
import com.appmobileos.freetoiletsrussiatrial.map.OnGeocoderResult;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerUser;
import com.appmobileos.freetoiletsrussiatrial.utils.InternetUtils;
import com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils;
import com.google.android.gms.maps.model.LatLng;


import java.io.IOException;

import stixiya.freetoilets.modal.PostAction;

public class AppendToiletFragment extends Fragment implements View.OnClickListener, OnGeocoderResult {
    public static final String APPENDTOILETACTIVITY_POSITION = "position";
    public static final String APPENDTOILETACTIVITY_NAME_OBJECT = "name_object";
    public static final String APPENDTOILETACTIVITY_OBJECT = "object";
    public static final String APPENDTOILETACTIVITY_VALUE_COAST = "value_coast";
    public static final String APPENDTOILETACTIVITY_FIND_WC = "find_wc";
    public static final String APPENDTOILETACTIVITY_ADDRESS = "place_address";
    public static final String APPENDTOILETACTIVITY_ACTION = "com.appmobileos.freetoiletsrussiatrial.append.wc";
    private static final String TAG = "AppendToiletFragment";
    private static final String ACTION_SEARCH = "search";
    private static final String ACTION_NOW_LOCATION = "nowLocation";
    private static final String KEY_START_POSITION = "start_position";
    private Spinner mCoastArray;
    private AutoCompleteTextView mNameObject;
    private EditText mFindWC;
    private TextView rub;
    private EditText mValueCoast;
    private ArrayAdapter<String> mNameObjectsArray;
    private ViewFlipper mObjects;
    private ImageView mSlideNextObject;
    private onAppendWC mCallback;
    private LinearLayout mNameContainer;
    private RelativeLayout mMainDescription;
    private int mSystemShortTimeAnimation;
    private TextView mAppendNewPlaceBtn;
    private String mCurrentObjectSelected;
    private LatLng mNewPlacePosition;
    private LinearLayout mAddressContainer;
    private Resources mResources;
    private TextView mTextLocation;
    private TextView mtextSearch;
    private ImageView mLocationBtn;
    private GeocoderCustom mFindAddress;
    private EditText mAddressValue;
    private ProgressBar mSearchProgress;
    private String mCity;
    private String mCountry;

    public static AppendToiletFragment newInstance() {
        AppendToiletFragment fragment = new AppendToiletFragment();
      /*  Bundle arg = new Bundle();
        arg.putParcelable(KEY_START_POSITION, nowLocation);
        fragment.setArguments(arg); */
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (onAppendWC) activity;
        mNameObjectsArray = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, activity.getResources().getStringArray(R.array.name_objects));
        mResources = getResources();
        mSystemShortTimeAnimation = mResources.getInteger(android.R.integer.config_shortAnimTime);
        mFindAddress = new GeocoderCustom(activity);
        mFindAddress.registerObserver(this);
        Location location = ((GoogleMaps2Activity) activity).getCurrentLocation();
        if (location != null) {
            mNewPlacePosition = new LatLng(location.getLatitude(), location.getLongitude());
        }

    }

    public void setNewPlacePosition(LatLng newPlacePosition) {
        this.mNewPlacePosition = newPlacePosition;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_append_toilet, container, false);
        mMainDescription = (RelativeLayout) rootView.findViewById(R.id.add_for_main_description);
        mAppendNewPlaceBtn = (TextView) rootView.findViewById(R.id.append_wc_addBtn);
        mAppendNewPlaceBtn.setOnClickListener(new AppendNewPlace());
        mTextLocation = (TextView) rootView.findViewById(R.id.add_form_address_location);
        mtextSearch = (TextView) rootView.findViewById(R.id.add_form_address_search);
        mTextLocation.setOnClickListener(this);
        mtextSearch.setOnClickListener(this);
        mLocationBtn = (ImageView) rootView.findViewById(R.id.add_form_locationBtn);
        mLocationBtn.setOnClickListener(this);
        mLocationBtn.setTag(ACTION_NOW_LOCATION);
        mObjects = (ViewFlipper) rootView.findViewById(R.id.append_wc_objectsFlipper);
        if (!RepeatMethods.getPreferenceHelpAppend(getActivity())) {
            TextView helpText = createHelpObject(getActivity());
            mObjects.addView(helpText);
            mObjects.setDisplayedChild(mObjects.indexOfChild(helpText));
            mAppendNewPlaceBtn.setEnabled(false);
        }
        mNameContainer = (LinearLayout) rootView.findViewById(R.id.add_form_name_container);
        mCoastArray = (Spinner) rootView.findViewById(R.id.add_form_coast_valueSpn);
        mNameObject = (AutoCompleteTextView) rootView.findViewById(R.id.add_form_name_value);
        mFindWC = (EditText) rootView.findViewById(R.id.add_form_description_value);
        mSlideNextObject = (ImageView) rootView.findViewById(R.id.append_wc_slide_objects);
        mSlideNextObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mObjects.showPrevious();
                if (!mAppendNewPlaceBtn.isEnabled()) mAppendNewPlaceBtn.setEnabled(true);
                TextView textView = (TextView) mObjects.getCurrentView();
                if (textView != null) {
                    CharSequence currentText = textView.getText();
                    if (currentText != null) {
                        mCurrentObjectSelected = currentText.toString();
                        mCallback.onFlipperNextObject(mCurrentObjectSelected, 0);
                        if (currentText.equals(DatabaseConstants.OBJECT_OTHER)) {
                            animationNameObject(true);
                        } else {
                            if (mNameContainer.getVisibility() == View.VISIBLE) {
                                animationNameObject(false);
                            }
                        }
                    }
                }
            }
        });
        mAddressValue = (EditText) rootView.findViewById(R.id.add_form_address_value);
        rub = (TextView) rootView.findViewById(R.id.add_form_rub);
        mValueCoast = (EditText) rootView.findViewById(R.id.add_form_coast_valueEdt);
        mAddressContainer = (LinearLayout) rootView.findViewById(R.id.add_form_address_container);
        mSearchProgress = (ProgressBar) rootView.findViewById(R.id.add_from_address_search_progress);
        seeSizeChangeNameContainer();
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFindAddress.unregisterObserver(this);
    }

    private void seeSizeChangeNameContainer() {
        final ViewTreeObserver viewTreeObserver = mNameContainer.getViewTreeObserver();
        if (viewTreeObserver != null) {
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (mNameContainer.getHeight() != 0) {
                            mNameContainer.setVisibility(View.GONE);
                            mNameContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        Log.i(getClass().getName(), "Высота === " + mNameContainer.getHeight());
                    }
                });
            }
        }
    }

    private TextView createHelpObject(Activity activity) {
        TextView textView = new TextView(activity);
        textView.setText(R.string.text_hint_select_place);
        textView.setTextAppearance(activity, android.R.attr.textAppearanceSmall);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    private void animationNameObject(final boolean show) {
        TranslateAnimation animation;
        if (show) {
            animation = new TranslateAnimation(0.0f, 0.0f, -mNameContainer.getHeight(), 0.0f);
        } else {
            animation = new TranslateAnimation(0.0f, 0.0f, 0.0f, -mNameContainer.getHeight());
        }
        animation.setDuration(mSystemShortTimeAnimation);
        if (show) mNameContainer.setVisibility(View.VISIBLE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //unused
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!show) {
                    mNameContainer.setVisibility(View.GONE);
                    mMainDescription.clearAnimation();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //unused
            }
        });
        mMainDescription.startAnimation(animation);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFindAddress.findAddress(mNewPlacePosition);
        mObjects.setInAnimation(AnimationUtils.loadAnimation(getActivity(),
                R.anim.push_left_in));
        mObjects.setOutAnimation(AnimationUtils.loadAnimation(getActivity(),
                R.anim.push_left_out));
        String[] names = getResources().getStringArray(R.array.names_other_places);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, names);
        mNameObject.setAdapter(adapter);

        mCoastArray.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                visibleValueCoast();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // unused
            }
        });
    }

    private void nowLocation(View view) {
        TextView text = (TextView) view;
        text.setBackgroundColor(mResources.getColor(android.R.color.white));
        text.setTextColor(mResources.getColor(android.R.color.black));
        mAddressValue.setText("");
        mAddressValue.setHint(R.string.text_address_found_help);

    }

 /*   private String checkObjects() {
        String object = null;
        long position = mObjectsArray.getSelectedItemId();
        if (position == 0) {
            object = DatabaseConstants.OBJECT_BIO;
        } else if (position == 1) {
            object = DatabaseConstants.OBJECT_MARKET;
        } else if (position == 2) {
            object = DatabaseConstants.OBJECT_PUBLIC;
        } else if (position == 3) {
            object = DatabaseConstants.OBJECT_MACDONALDS;
        } else if (position == 4) {
            object = DatabaseConstants.OBJECT_KINO;
        } else {
            object = DatabaseConstants.OBJECT_PUBLIC;
        }git
        return object;
    }*/

    private void searchLocation(View view) {
        TextView text = (TextView) view;
        text.setBackgroundColor(mResources.getColor(R.color.android_blue));
        text.setTextColor(mResources.getColor(android.R.color.white));
        mAddressValue.setText("");

    }

    private void visibleValueCoast() {
        AlphaAnimation alpha;
        if (mCoastArray.getSelectedItemId() == 1) {
            alpha = new AlphaAnimation(0.0f, 1.0f);
            mValueCoast.setVisibility(View.VISIBLE);
            rub.setVisibility(View.VISIBLE);
        } else {
            alpha = new AlphaAnimation(1.0f, 0.0f);
            mValueCoast.setVisibility(View.GONE);
            rub.setVisibility(View.GONE);
        }
        alpha.setDuration(mSystemShortTimeAnimation);
        mValueCoast.setAnimation(alpha);
        rub.setAnimation(alpha);
        alpha.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_form_address_search:
                Log.i(TAG, "CLICK SEARCH HEADER");
                searchLocation(view);
                nowLocation(mAddressContainer.findViewById(R.id.add_form_address_location));
                mLocationBtn.setImageResource(R.drawable.ic_action_search);
                mLocationBtn.setTag(ACTION_SEARCH);
                break;
            case R.id.add_form_address_location:
                searchLocation(view);
                nowLocation(mAddressContainer.findViewById(R.id.add_form_address_search));
                mLocationBtn.setImageResource(R.drawable.ic_action_refresh);
                mLocationBtn.setTag(ACTION_NOW_LOCATION);
                mAddressValue.setHint("");
                mFindAddress.findAddress(mNewPlacePosition);
                startSearch(true);
                break;
            case R.id.add_form_locationBtn:
                if (view.getTag().equals(ACTION_NOW_LOCATION)) {
                    startSearch(true);
                    mFindAddress.findAddress(mNewPlacePosition);
                } else if (view.getTag().equals(ACTION_SEARCH)) {
                    Editable text = mAddressValue.getText();
                    if (text != null) {
                        if (!TextUtils.isEmpty(text)) {
                            startSearch(false);
                            String requestSearch = text.toString();
                            if (mCity != null) {
                                boolean resultCheck = requestSearch.contains(mCity);
                                if (!resultCheck) requestSearch += "," + mCity;
                            }
                            if (mCountry != null) {
                                boolean resultCheck = requestSearch.contains(mCountry);
                                if (!resultCheck) requestSearch += "," + mCountry;
                            }
                            Log.i(TAG, "SEARCH REQUEST =" + requestSearch);
                            mFindAddress.findCoordinates(requestSearch);
                        } else {
                            shakeAnimation();
                        }
                    } else {
                        shakeAnimation();
                    }
                }
                break;
        }
    }

    private void shakeAnimation() {
        Activity activity = getActivity();
        if (activity != null) {
            Animation shake = AnimationUtils.loadAnimation(activity, R.anim.shake);
            mAddressValue.startAnimation(shake);
        }
    }

    private void startSearch(boolean nowLocation) {
        if (nowLocation) mAddressValue.setText("");
        mLocationBtn.setEnabled(false);
        mSearchProgress.setVisibility(View.VISIBLE);
    }

    private void finishSearch() {
        mLocationBtn.setEnabled(true);
        mSearchProgress.setVisibility(View.GONE);
    }

    @Override
    public void foundAddress(Address address) {
        String foundAddress = getString(R.string.text_address_not_found);
        if (address != null) {
            Log.i(TAG, "FIND_ADDRESS (index " + address.getMaxAddressLineIndex() + ") = " + address.toString());
            if (address.getMaxAddressLineIndex() > 0) {
                foundAddress = address.getAddressLine(0);
                mCity = address.getLocality();
                mCountry = address.getCountryName();
            }

        }
        mAddressValue.setText(foundAddress);
        finishSearch();
    }

    @Override
    public void foundCoordinates(LatLng location) {
        if (location != null) {
            mNewPlacePosition = location;
            Location conLocation = new Location("custom");
            conLocation.setLatitude(location.latitude);
            conLocation.setLongitude(location.longitude);
            mCallback.foundCoordinates(conLocation);
        } else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, R.string.text_address_not_found, Toast.LENGTH_SHORT).show();
            }
        }
        finishSearch();
    }

    public interface onAppendWC {
        void onFlipperNextObject(String object, int price);

        void foundCoordinates(Location location);
    }



	/*private void startAppendService() {
        int valueCoast = 0;
		Intent intent = new Intent(this, UpdateRemoteService.class);
		// add position from maps activity
		intent.putExtra(APPENDTOILETACTIVITY_POSITION, getIntent()
				.getParcelableExtra(APPENDTOILETACTIVITY_POSITION));
		intent.putExtra(APPENDTOILETACTIVITY_OBJECT, checkObjects());
		if (!TextUtils.isEmpty(mNameObject.getText().toString().trim()))
			intent.putExtra(APPENDTOILETACTIVITY_NAME_OBJECT, mNameObject
					.getText().toString().trim());
		if (!TextUtils.isEmpty(mValueCoast.getText()) && mValueCoast.getVisibility()==View.VISIBLE)
			valueCoast = Integer.parseInt(mValueCoast.getText().toString()
					.trim());
		intent.putExtra(GoogleMaps2Activity.UPDATE_REMOTE_SERVICE_ACTION,
				"append");
		intent.putExtra(APPENDTOILETACTIVITY_VALUE_COAST, valueCoast);
		intent.putExtra(APPENDTOILETACTIVITY_FIND_WC, mFind_WC.getText()
				.toString().trim());

		startService(intent);
		// close activity
		finish();

	}*/

    private class AppendNewPlace implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            MarkerUser newPlace = new MarkerUser();
            newPlace.setObject(mCurrentObjectSelected);
            if (mCurrentObjectSelected.equals(DatabaseConstants.OBJECT_OTHER)) {
                Editable objectName = mNameObject.getText();
                if (objectName != null) {
                    newPlace.setDescription(objectName.toString());
                }
            }
            Editable valueCoast = mValueCoast.getText();
            if (valueCoast != null && !TextUtils.isEmpty(mValueCoast.getText()) && mValueCoast.getVisibility() == View.VISIBLE) {
                newPlace.setPrice(Integer.parseInt(mValueCoast.getText().toString().trim()));
            }
            Editable findWc = mFindWC.getText();
            if (findWc != null && !TextUtils.isEmpty(mFindWC.getText())) {
                newPlace.setFindWC(findWc.toString().trim());
            }
            if (mNewPlacePosition != null) {
                newPlace.setLatitude(mNewPlacePosition.latitude);
                newPlace.setLongitude(mNewPlacePosition.longitude);
            }
            Activity activity = getActivity();
            if (activity != null) {
                if (!RepeatMethods.getPreferenceHelpAppend(activity)) {
                    RepeatMethods.setPreferenceHelpAppend(true, activity);
                }
            }
            try {
                String json = LocationUtils.createJsonNewPlace(newPlace);
                String dataS = "newPlace=" + json + "&action=" + PostAction.APPEND;
                InternetUtils.createHttpConnection(dataS);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

