package com.appmobileos.freetoiletsrussiatrial.ui.slidepanels;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import com.appmobileos.freetoiletsrussiatrial.map.GoogleMaps2Activity;
import com.appmobileos.freetoiletsrussiatrial.ui.AutoSlideLayout;
import com.appmobileos.freetoiletsrussiatrial.ui.AutoSlideLayoutAdapter;
import com.nineoldandroids.animation.Animator;

/**
 * Created by andrey on 09/11/13.
 */
public class SPLocationPreference extends Fragment implements View.OnClickListener {
    private static final String KEY_STATUS_PREFERENCE_LOCATION = "preference_location";
    private CheckedTextView mLocationEnableText;
    private AutoSlideLayout mAutoSlideLayout;
    private EventsAnimator eventsAnimator;
    private boolean mCurrentStatusPreference;

    public static SPLocationPreference newInstance(boolean statusPreferenceLocation) {
        SPLocationPreference fragment = new SPLocationPreference();
        Bundle args = new Bundle();
        args.putBoolean(KEY_STATUS_PREFERENCE_LOCATION, statusPreferenceLocation);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sp_local_location_setting, container, false);
        if (rootView != null) {
            mAutoSlideLayout = (AutoSlideLayout) rootView.findViewById(R.id.auto_slide_container);
            mLocationEnableText = (CheckedTextView) rootView.findViewById(R.id.ap_check_text);
            mLocationEnableText.setOnClickListener(this);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle arg = getArguments();
        if (arg != null) {
            mCurrentStatusPreference = arg.getBoolean(KEY_STATUS_PREFERENCE_LOCATION, false);
            if (mCurrentStatusPreference) {
                preferenceEnable();
            } else {
                preferenceDisable();
            }
        }
        mAutoSlideLayout.showPanel();
        eventsAnimator = new EventsAnimator();
        mAutoSlideLayout.registerObserver(eventsAnimator);

    }

    public void updateConfiguration(boolean statusPreference) {
        mCurrentStatusPreference = statusPreference;
        mAutoSlideLayout.showPanel();
    }

    private void preferenceEnable() {
        mAutoSlideLayout.setBackgroundColor(getResources().getColor(R.color.ap_green));
        mLocationEnableText.setText(R.string.ap_location_enable);
        mLocationEnableText.setChecked(true);
    }

    public void hideSlidePanel() {
        mAutoSlideLayout.hidePanel();
    }

    private void preferenceDisable() {
        mAutoSlideLayout.setBackgroundColor(getResources().getColor(R.color.ap_red));
        mLocationEnableText.setText(R.string.ap_location);
        mLocationEnableText.setChecked(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ap_check_text:
                CheckedTextView statusText = ((CheckedTextView) view);
                boolean statusPreference;
                if (statusText.isChecked()) {
                    statusPreference = false;
                    statusText.setChecked(false);
                } else {
                    statusPreference = true;
                    statusText.setChecked(true);
                }
                Activity activity = getActivity();
                if (activity != null) {
                    RepeatMethods.setPreferenceLocation(statusPreference, activity);
                }
                mAutoSlideLayout.hidePanel();
                break;
        }
    }

    private void deleteFragment() {
        Activity activity = getActivity();
        if (activity != null) {
            ((GoogleMaps2Activity) activity).getSupportFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    private class EventsAnimator extends AutoSlideLayoutAdapter {
        @Override
        public void reconfigureSlidePanel() {
            if (mCurrentStatusPreference) {
                preferenceEnable();
            } else {
                preferenceDisable();
            }
        }

        @Override
        public void clearBodySlidePanel() {
            mAutoSlideLayout.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        }

        @Override
        public void onAnimationStart(Animator animation, AutoSlideLayout.AnimatorQueue queue, AutoSlideLayout.PanelActions status) {
            switch (queue) {
                case EMPTY:
                    switch (status) {
                        case SHOW:
                            if (mCurrentStatusPreference) {
                                preferenceEnable();
                            } else {
                                preferenceDisable();
                            }
                            break;
                    }
            }
        }

        @Override
        public void onAnimationEnd(com.nineoldandroids.animation.Animator animation, AutoSlideLayout.AnimatorQueue queue, AutoSlideLayout.PanelActions status) {
            switch (queue) {
                case EMPTY:
                    switch (status) {
                        case HIDE:
                            deleteFragment();
                            break;
                    }
            }
        }

    }
}
