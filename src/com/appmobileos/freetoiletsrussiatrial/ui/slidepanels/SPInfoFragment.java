package com.appmobileos.freetoiletsrussiatrial.ui.slidepanels;

import android.app.Activity;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.map.GoogleMaps2Activity;
import com.appmobileos.freetoiletsrussiatrial.ui.AutoSlideLayout;
import com.appmobileos.freetoiletsrussiatrial.ui.AutoSlideLayoutAdapter;

/**
 * Created by a.nikonov on 07.11.13.
 */
public class SPInfoFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "SPInfoFragment";
    private static final String KEY_MAIN_TEXT = "main_text";
    private static final String KEY_SECOND_TEXT = "second_text";
    private static final String KEY_COLOR_CONTAINER = "color_container";
    private TextView mMainText;
    private TextView mSecondText;
    private AutoSlideLayout mAutoSlideLayout;
    private int mResMainText;
    private int mResSecondText;
    private int mResAPColor;
    private EventsAnimator eventsAnimator;

    public static SPInfoFragment newInstance(int resourceMainText, int resourceSecondText, int resourceColor) {
        SPInfoFragment fr = new SPInfoFragment();
        Bundle arg = new Bundle();
        arg.putInt(KEY_MAIN_TEXT, resourceMainText);
        arg.putInt(KEY_SECOND_TEXT, resourceSecondText);
        arg.putInt(KEY_COLOR_CONTAINER, resourceColor);
        fr.setArguments(arg);
        return fr;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sp_info_fragment, container, false);
        if (rootView != null) {
            mAutoSlideLayout = (AutoSlideLayout) rootView.findViewById(R.id.auto_slide_container);
            mMainText = (TextView) rootView.findViewById(R.id.sp_info_main_text);
            mSecondText = (TextView) rootView.findViewById(R.id.sp_info_second_text);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mResMainText = arguments.getInt(KEY_MAIN_TEXT, 0);
            mResSecondText = arguments.getInt(KEY_SECOND_TEXT, 0);
            mResAPColor = arguments.getInt(KEY_COLOR_CONTAINER, 0);
        }
        showInfoText(mResMainText, mResSecondText, mResAPColor);
        eventsAnimator = new EventsAnimator();
        mAutoSlideLayout.registerObserver(eventsAnimator);
    }

    /**
     * @param resourceMainText   string resources use 0 if don't see resources
     * @param resourceSecondText string resource use 0 if don't see resources
     * @param resourceColor      color resources use 0 if don't see resources
     */
    public void showInfoText(final int resourceMainText, final int resourceSecondText, final int resourceColor) {
        mResMainText = resourceMainText;
        mResSecondText = resourceSecondText;
        mResAPColor = resourceColor;
        if (!mAutoSlideLayout.isVisible()) configInfoText();
        mAutoSlideLayout.showPanel();
    }

    private void configInfoText() {
        if (mResMainText != 0) {
            mMainText.setText(mResMainText);
            mMainText.setVisibility(View.VISIBLE);
        }
        if (mResSecondText != 0) {
            mSecondText.setText(mResSecondText);
            mSecondText.setVisibility(View.VISIBLE);
        }
        if (mResAPColor != 0) mAutoSlideLayout.setBackgroundResource(mResAPColor);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ap_main:
                mAutoSlideLayout.hidePanel();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAutoSlideLayout.unregisterObserver(eventsAnimator);
    }

    public void hideSlidePanelInfo() {
        mAutoSlideLayout.hidePanel();
    }

    private void deleteFragment() {
        Activity activity = getActivity();
        if (activity != null) {
            ((GoogleMaps2Activity) activity).getSupportFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    private class EventsAnimator extends AutoSlideLayoutAdapter {
        @Override
        public void reconfigureSlidePanel() {
            configInfoText();
        }

        @Override
        public void clearBodySlidePanel() {
            mMainText.setVisibility(View.GONE);
            mSecondText.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationEnd(com.nineoldandroids.animation.Animator animation, AutoSlideLayout.AnimatorQueue queue, AutoSlideLayout.PanelActions status) {
            switch (queue) {
                case EMPTY:
                    switch (status) {
                        case HIDE:
                            deleteFragment();
                    }
            }
        }

    }
}
