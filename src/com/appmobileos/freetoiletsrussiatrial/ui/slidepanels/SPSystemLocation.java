package com.appmobileos.freetoiletsrussiatrial.ui.slidepanels;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.ui.AutoSlideLayout;

/**
 * Created by andrey on 24/11/13.
 */
public class SPSystemLocation extends Fragment implements View.OnClickListener {
    private AutoSlideLayout mAutoSlideLayout;

    public static SPSystemLocation newInstance() {
        return new SPSystemLocation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sp_system_settings_location, container, false);
        if (rootView != null) {
            mAutoSlideLayout = (AutoSlideLayout) rootView.findViewById(R.id.auto_slide_container);
            TextView actionOpenSettings = (TextView) rootView.findViewById(R.id.sp_system_settingsBtn);
            actionOpenSettings.setOnClickListener(this);
        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sp_system_settingsBtn:
                openSystemLocationSettings();
                break;
        }
    }

    private void openSystemLocationSettings() {
        Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(viewIntent);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showPanel();
    }

    public void showPanel() {
        mAutoSlideLayout.showPanel();
    }

    public void hidePanel() {
        mAutoSlideLayout.hidePanel();
    }
}
