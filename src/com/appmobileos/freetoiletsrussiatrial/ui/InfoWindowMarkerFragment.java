package com.appmobileos.freetoiletsrussiatrial.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appmobileos.freetoiletsrussiatrial.Constants;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerParcelable;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class InfoWindowMarkerFragment extends Fragment implements View.OnClickListener {
    private static final String DISTANCE_VALUE = "distance_value";
    private static final String TAG = "InfoWindowMarkerFragment";
    private TextView mNameObject;
    private TextView mAddressObject;
    private View mAddressLine;
    private TextView mAddressStaticText;
    //private ImageView mIconObject;
    private TextView mDescriptionObject;
    private TextView mCoastStatic;
    private View mDescriptionLine;
    private View mCoastLine;
    private TextView mDescriptionStaticText;
    private TextView mCoastObject;
    private double mLatitude;
    private double mLongitude;
    //private RelativeLayout mGoogleServices;
    private TextView mGoogleRoute;
    private LatLng mDescriptionPlace;
    private LatLng mLocationUser;
    private TextView mDistanceViewValue;
    private View mVerticalHeaderLine;
    private MarkerParcelable mCurrentMarker;
    private LinearLayout mUserOpinionContainer;
    private ProgressBar mUserOpinionProgress = null;
    private String mDistanceValue = null;
    private Intent mNavigationIntent;

    //private LinearLayout mGoogleServiceButtonsContainer;
    public static InfoWindowMarkerFragment newInstance(MarkerParcelable marker) {
        InfoWindowMarkerFragment infoWindowMarkerFragment = new InfoWindowMarkerFragment();
        Bundle arguments = new Bundle();
        arguments.putParcelable(Constants.KEY_CURRENT_MARKER, marker);
        infoWindowMarkerFragment.setArguments(arguments);
        return infoWindowMarkerFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Bundle argument = getArguments();
        if (argument != null) {
            mCurrentMarker = argument.getParcelable(Constants.KEY_CURRENT_MARKER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_info_window_marker, container, false);
        mCoastStatic = (TextView) rootView.findViewById(R.id.info_wc_static_coast);
        mNameObject = (TextView) rootView.findViewById(R.id.info_wc_name_object);
        mAddressObject = (TextView) rootView.findViewById(R.id.info_wc_database_address);
        mAddressLine = rootView.findViewById(R.id.info_wc_line_address);
        mAddressStaticText = (TextView) rootView.findViewById(R.id.info_wc_static_address);
        //mIconObject = (ImageView) rootView.findViewById(R.id.info_wc_icon_object);
        mDistanceViewValue = (TextView) rootView.findViewById(R.id.info_wc_distance2);
        mDescriptionObject = (TextView) rootView.findViewById(R.id.info_wc_database_description);
        mCoastObject = (TextView) rootView.findViewById(R.id.info_wc_database_coast);
        mDescriptionLine = rootView.findViewById(R.id.info_wc_line_description);
        mDescriptionStaticText = (TextView) rootView.findViewById(R.id.info_wc_static_description);
        mCoastLine = rootView.findViewById(R.id.info_wc_line_coast);
        mGoogleRoute = (TextView) rootView.findViewById(R.id.google_service_router);
        mGoogleRoute.setOnClickListener(this);
        mUserOpinionContainer = (LinearLayout) rootView.findViewById(R.id.user_opinion_container);
        mVerticalHeaderLine = rootView.findViewById(R.id.google_service_vertical_line);
        if (mCurrentMarker != null) {
            addInfoObject(mCurrentMarker);
            if (mCurrentMarker.getUserOpinion() <= 0) {
                mUserOpinionContainer.setVisibility(View.VISIBLE);
            }
        }
/*          mGoogleServices = (RelativeLayout) rootView.findViewById(R.id.google_service_main_container);
            mGoogleServiceButtonsContainer = (LinearLayout) rootView.findViewById(R.id.google_service_buttons_container);*/

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            String distance = savedInstanceState.getString(DISTANCE_VALUE);
            if (distance != null) updateDistance(distance);
            Log.i(TAG, "RESTORE DISTANCE VALUE  = " + savedInstanceState.getString(DISTANCE_VALUE));
        }
        initRouteServices();

    }

    public MarkerParcelable getMarker() {
        return mCurrentMarker;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DISTANCE_VALUE, mDistanceValue);
        Log.i(TAG, "SAVE DISTANCE VALUE  = " + mDistanceValue);
    }

    private boolean checkGoogleApp(Intent intent) {
        Activity activity = getActivity();
        List<ResolveInfo> list = null;
        if (activity != null) {
            PackageManager pacManager = activity.getPackageManager();
            if (pacManager != null) {
                list = pacManager.queryIntentActivities(intent, 0);
            }
        }
        return list != null && list.size() > 0;
    }


    private Intent intentNavigation(LatLng place) {
        String uriBegin = "geo:0,0?q=" + place.latitude + "," + place.longitude;
        return new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uriBegin));
    }

    public void userOpinionResult(UserOpinionResult result) {
        final TextView text = new TextView(getActivity());
        text.setGravity(Gravity.CENTER);
        switch (result) {
            case OK:
                mUserOpinionContainer.removeAllViews();
                text.setText(R.string.text_user_opinion_result_ok);
                mUserOpinionContainer.addView(text);
                break;
            case FAILED:
                showOrHideViews(false);
                text.setText(R.string.text_user_opinion_result_error);
                text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_refresh, 0, 0, 0);
                text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mUserOpinionContainer.removeView(text);
                        showOrHideViews(true);
                    }
                });
                mUserOpinionContainer.addView(text);
                break;
        }

    }

    public void userOpinionProcessing(boolean show) {
        if (show) {
            showOrHideViews(false);
            mUserOpinionProgress = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyle);
            mUserOpinionContainer.addView(mUserOpinionProgress);
        } else {
            if (mUserOpinionProgress != null)
                mUserOpinionContainer.removeView(mUserOpinionProgress);
            showOrHideViews(true);
        }

    }

    private void showOrHideViews(boolean showOrHide) {
        for (int i = 0; i < mUserOpinionContainer.getChildCount(); i++) {
            View currentView = mUserOpinionContainer.getChildAt(i);
            if (currentView != null) {
                currentView.setVisibility(showOrHide ? View.VISIBLE : View.GONE);
            }
        }
    }

    private void initRouteServices() {
        if (mCurrentMarker != null) {
            mNavigationIntent = intentNavigation(new LatLng(mCurrentMarker.getLatitude(), mCurrentMarker.getLongitude()));
            boolean fountPackage = checkGoogleApp(mNavigationIntent);
            if (fountPackage) {
                mGoogleRoute.setVisibility(View.VISIBLE);
                mVerticalHeaderLine.setVisibility(View.VISIBLE);
            }
        }
    }

    public void addInfoObject(MarkerParcelable marker) {
        Log.i(getTag(), "Marker info " + marker.getAddress() + " " + marker.getObject());
        int selectDrabagle = 0;
        String object = marker.getObject();
        //mIconObject.setImageResource(selectDrabagle);
        String nameObject = marker.getDescription();
        mNameObject.setText((nameObject == null || nameObject.length() <= 0) ? object : nameObject);
        mNameObject.setVisibility(View.VISIBLE);
        String address = marker.getAddress();
        if (address != null) {
            mAddressStaticText.setVisibility(View.VISIBLE);
            mAddressLine.setVisibility(View.VISIBLE);
            mAddressObject.setText(address);
            mAddressObject.setVisibility(View.VISIBLE);
        }

        String findWC = marker.getFindWC();
        if (findWC != null && findWC.length() > 0) {
            mDescriptionStaticText.setVisibility(View.VISIBLE);
            mDescriptionLine.setVisibility(View.VISIBLE);
            mDescriptionObject.setText(findWC);
            mDescriptionObject.setVisibility(View.VISIBLE);
        }

        int price = marker.getPrice();
        if (price == 0) {
            mCoastObject.setText(R.string.info_free);
        } else {
            mCoastObject
                    .setText(price + " " + getString(R.string.info_rub));
        }
        mCoastStatic.setVisibility(View.VISIBLE);
        mCoastLine.setVisibility(View.VISIBLE);
        mCoastObject.setVisibility(View.VISIBLE);
        mLatitude = marker.getLatitude();
        mLongitude = marker.getLongitude();
    }

    public void updateDistance(String distance) {
        Log.i(getClass().getName(), "METHOD updateDistance CURRENT VALUE = " + distance);
        if (mDistanceViewValue != null) {
            if (mDistanceViewValue.getVisibility() != View.VISIBLE) {
                mDistanceViewValue.setVisibility(View.VISIBLE);
            }
            mDistanceValue = distance;
            mDistanceViewValue.setText(getString(R.string.info_marker_distance) + " " + mDistanceValue);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.google_service_router:
                startActivity(mNavigationIntent);
                break;
        }
    }

    public enum UserOpinionResult {OK, FAILED}
}
