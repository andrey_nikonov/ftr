package com.appmobileos.freetoiletsrussiatrial.ui;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.appmobileos.freetoiletsrussiatrial.Constants;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.adapters.CollectionPagesAdapter;
import com.appmobileos.freetoiletsrussiatrial.modal.MarkerParcelable;
import com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils;

import java.util.ArrayList;

/**
 * Created by andrey on 09/09/13.
 */
public class MarkersViewPagesFragment extends Fragment {
    private static final String TAG = "MarkersViewPagesFragment";
    private CollectionPagesAdapter mPagesAdapter;
    private ViewPager mViewPager;
    private onViewPagesActions mViewPagesActions;

    public static MarkersViewPagesFragment newInstance(ArrayList<MarkerParcelable> markers) {
        MarkersViewPagesFragment fragment = new MarkersViewPagesFragment();
        Bundle argument = new Bundle();
        argument.putParcelableArrayList(Constants.KEY_CURRENT_LIST_MARKERS, markers);
        fragment.setArguments(argument);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mViewPagesActions = (onViewPagesActions) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_markers_view_pages, container, false);
        if (rootView != null) {
            mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
            mViewPager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    mViewPagesActions.onTouchEventCustom(motionEvent);
                    return false;
                }
            });
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayList<MarkerParcelable> markers = getArguments().getParcelableArrayList(Constants.KEY_CURRENT_LIST_MARKERS);
        mPagesAdapter = new CollectionPagesAdapter(getActivity(), markers);
        mViewPager.setAdapter(mPagesAdapter);
        mViewPager.setOnPageChangeListener(new PageListener());
    }

    public void showSmallInfoPlace(MarkerParcelable marker, Location myLocation) {
        int positionInAdapter = mPagesAdapter.getItemPosition(marker);
        updateDistanceValue(marker, myLocation, positionInAdapter);
        mViewPager.setCurrentItem(positionInAdapter);
    }

    public void updateDistanceValue(MarkerParcelable marker, Location myLocation, int position) {
        InfoWindowMarkerFragment infoFragment = findInfoWindowFragment(position);
        infoFragment.updateDistance(LocationUtils.calculateDistance(marker, myLocation));
    }

    private InfoWindowMarkerFragment findInfoWindowFragment(int position) {
        return (InfoWindowMarkerFragment) mPagesAdapter.instantiateItem(mViewPager, position);
    }

    public void updateUserOpinion(MarkerParcelable opinionMarker, InfoWindowMarkerFragment.UserOpinionResult result) {
        int positionInAdapter = mPagesAdapter.getItemPosition(opinionMarker);
        InfoWindowMarkerFragment infoFragment = findInfoWindowFragment(positionInAdapter);
        infoFragment.userOpinionResult(result);
    }

    public void updateUserOpinionProgressing(boolean show) {
        InfoWindowMarkerFragment infoFragment = findInfoWindowFragment(mViewPager.getCurrentItem());
        infoFragment.userOpinionProcessing(show);
    }

    public MarkerParcelable findCurrentPlace() {
        int currentPosition = mViewPager.getCurrentItem();
        return mPagesAdapter.getMarkerParcelable(currentPosition);
    }

    public Fragment findAppendWCFragment() {
        return mPagesAdapter.findAppendToiletFragment();
    }

    public void showAppendWC() {
        int positionAppendWC = mPagesAdapter.positionAppendWC();
        mViewPager.setCurrentItem(positionAppendWC);
    }

    public interface onViewPagesActions {

        void onPageViewChanged(MarkerParcelable visibleMarker, int position);

        void onTouchEventCustom(MotionEvent motionEvent);
    }

    public class PageListener extends ViewPager.SimpleOnPageChangeListener {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            mViewPagesActions.onPageViewChanged(mPagesAdapter.getMarkerParcelable(position), position);
        }

    }
}

