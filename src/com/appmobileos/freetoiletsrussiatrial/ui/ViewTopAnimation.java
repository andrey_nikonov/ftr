package com.appmobileos.freetoiletsrussiatrial.ui;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;

/**
 * Created by andrey on 13/09/13.
 */
public class ViewTopAnimation extends Animation {
    float startY, finishY;
    private int taggetHeight;
    private ViewGroup view;
    public ViewTopAnimation (ViewGroup view, int taggetHeight){
        this.view = view;
        this.taggetHeight = taggetHeight;

    }
    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        view.getLayoutParams().height = (int)(taggetHeight * interpolatedTime);
        view.requestLayout();

    }
}
