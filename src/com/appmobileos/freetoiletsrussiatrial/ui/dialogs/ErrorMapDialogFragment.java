package com.appmobileos.freetoiletsrussiatrial.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by andrey on 05/09/13.
 */
public class ErrorMapDialogFragment extends DialogFragment {

    private Dialog mDialog;

    public ErrorMapDialogFragment() {
        super();
        mDialog = null;
    }

    public void setDialog(Dialog dialog) {
        mDialog = dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return mDialog;
    }
}
