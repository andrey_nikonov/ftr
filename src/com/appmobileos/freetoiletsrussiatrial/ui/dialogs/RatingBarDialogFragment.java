package com.appmobileos.freetoiletsrussiatrial.ui.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.appmobileos.freetoiletsrussiatrial.R;

/**
 * Created by andrey on 27/10/13.
 */
public class RatingBarDialogFragment extends DialogFragment implements RatingBar.OnRatingBarChangeListener, View.OnClickListener {
    public static final int RATING_TERRIBLE = 1;
    public static final int RATING_BAD = 2;
    public static final int RATING_NORMAL = 3;
    public static final int RATING_GOOD = 4;
    public static final int RATING_EXCELLENT = 5;
    private OnRatingDialogChangeListener mRatingCallback;
    private TextView mExplanationText;
    private RatingBar mRatingBar;
    private Button mCancelBtn;
    private Button mEvaluateBtn;
    private int mCurrentRating;

    public static RatingBarDialogFragment newInstance() {
        RatingBarDialogFragment dialogFragment = new RatingBarDialogFragment();
        return dialogFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mRatingCallback = (OnRatingDialogChangeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity " + activity.toString()
                    + " must implement OnRatingDialogChangeListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.text_rating_title_dialog);
        View rootView = inflater.inflate(R.layout.fragment_dialog_rating_bar, container, false);
        if (rootView != null) {
            mExplanationText = (TextView) rootView.findViewById(R.id.rating_dialog_explanation_rating_text);
            mRatingBar = (RatingBar) rootView.findViewById(R.id.rating_dialog_rating_bar);
            mRatingBar.setOnRatingBarChangeListener(this);
            mCancelBtn = (Button) rootView.findViewById(R.id.action_rating_cancel);
            mCancelBtn.setOnClickListener(this);
            mEvaluateBtn = (Button) rootView.findViewById(R.id.action_rating_install);
            mEvaluateBtn.setOnClickListener(this);
        }
        return rootView;
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
        mCurrentRating = (int) v;
        switch (mCurrentRating) {
            case RATING_TERRIBLE:
                if (!mEvaluateBtn.isEnabled()) mEvaluateBtn.setEnabled(true);
                mExplanationText.setText(R.string.rating_bar_terrible);
                break;
            case RATING_BAD:
                if (!mEvaluateBtn.isEnabled()) mEvaluateBtn.setEnabled(true);
                mExplanationText.setText(R.string.rating_bar_bad);
                break;
            case RATING_NORMAL:
                if (!mEvaluateBtn.isEnabled()) mEvaluateBtn.setEnabled(true);
                mExplanationText.setText(R.string.rating_bar_normal);
                break;
            case RATING_GOOD:
                if (!mEvaluateBtn.isEnabled()) mEvaluateBtn.setEnabled(true);
                mExplanationText.setText(R.string.rating_bar_good);
                break;
            case RATING_EXCELLENT:
                if (!mEvaluateBtn.isEnabled()) mEvaluateBtn.setEnabled(true);
                mExplanationText.setText(R.string.rating_bar_excellent);
                break;
            default:
                if (mEvaluateBtn.isEnabled()) mEvaluateBtn.setEnabled(false);
                mExplanationText.setText("");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_rating_cancel:
                getDialog().dismiss();
                break;
            case R.id.action_rating_install:
                mRatingCallback.onRatingInstalled(mCurrentRating);
                getDialog().dismiss();
                break;
        }
    }

    public interface OnRatingDialogChangeListener {
        void onRatingInstalled(int rating);
    }


}
