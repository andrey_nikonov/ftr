package com.appmobileos.freetoiletsrussiatrial.controller;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;

import com.appmobileos.freetoiletsrussiatrial.utils.DisplayUtils;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by andrey on 24/11/13.
 */
public class MapVisibleController {
    private static final String TAG = "MapVisibleController";
    private int mLowerBoundHeight;
    private int mMaxWidth;
    private Marker mCurrentGoogleMarker;
    private Context mContext;
    private Projection mMapProjection;
    private int mCenterX;
    private int mMaxHeightLandscape;

    public MapVisibleController(Context context) {
        mContext = context;
        mLowerBoundHeight = DisplayUtils.getMinHeightGoogleMap(context);
        mMaxWidth = DisplayUtils.getMaxDisplayWidth(context);
        mCenterX = mMaxWidth / 2;
        if (!DisplayUtils.isPortrait(context))
            mMaxHeightLandscape = DisplayUtils.getMaxDisplayHeight(mContext) - DisplayUtils.getStatusBarHeight(context);
    }

    public void setCurrentGoogleMarker(Marker googleMarker) {
        this.mCurrentGoogleMarker = googleMarker;
    }

    public int calculateGoY() {
        int resultGoY = 0;
        if (mCurrentGoogleMarker != null) {
            Point point = mMapProjection.toScreenLocation(mCurrentGoogleMarker.getPosition());
            if (DisplayUtils.isPortrait(mContext)) {
                resultGoY = (point.y - mLowerBoundHeight) + mLowerBoundHeight / 2;
            } else {
                resultGoY = point.y - (mMaxHeightLandscape / 2);
            }
            Log.i(TAG, "GO Y = " + resultGoY + " POINT y = " + point.y + " lower bound = " + mLowerBoundHeight);
        }

        return resultGoY;
    }

    public int calculateGoX() {
        int resultGoX = 0;
        if (mCurrentGoogleMarker != null) {
            Point point = mMapProjection.toScreenLocation(mCurrentGoogleMarker.getPosition());
            if (DisplayUtils.isPortrait(mContext)) {
                resultGoX = point.x - mCenterX;
            } else {
                resultGoX = point.x - (mCenterX / 2);
            }
            Log.i(TAG, "GO X = " + resultGoX + " MAX WIDTH /2 = " + mMaxWidth / 2 + " POINT X = " + point.x);
        }
        return resultGoX;
    }

    public void setMapProjection(Projection mapProjection) {
        this.mMapProjection = mapProjection;
    }
}
