package com.appmobileos.freetoiletsrussiatrial.controller;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import com.appmobileos.freetoiletsrussiatrial.ui.SlideInfoWindow;
import com.appmobileos.freetoiletsrussiatrial.ui.slidepanels.SPInfoFragment;
import com.appmobileos.freetoiletsrussiatrial.ui.slidepanels.SPLocationPreference;
import com.appmobileos.freetoiletsrussiatrial.ui.slidepanels.SPSystemLocation;
import com.appmobileos.freetoiletsrussiatrial.utils.LocationUtils;

/**
 * Created by andrey on 12/11/13.
 */
public class SlidePanelController {
    private static final String TAG = "SlidePanelController";
    private final String mTagLocation;
    private final String mTagSystemLocation;
    private final String mTagAppendToilet;
    private final String mTagInfo;
    private FragmentActivity mActivity;
    private FragmentManager mFragmentManager;
    private SPLocationPreference mSPLocationPreference;
    private SPInfoFragment mInfoFragment;


    public SlidePanelController(FragmentActivity activity) {
        mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
        mTagLocation = mActivity.getString(R.string.tag_fragment_sp_location);
        mTagAppendToilet = mActivity.getString(R.string.tag_fragment_append_toilet);
        mTagSystemLocation = mActivity.getString(R.string.tag_fragment_sp_system_location);
        mTagInfo = mActivity.getString(R.string.tag_fragment_sp_info);
    }

    public void showLocalLocationSettings() {
        boolean statusPreference = RepeatMethods.getPreferenceLocation(mActivity);
        mSPLocationPreference = (SPLocationPreference) mFragmentManager.findFragmentByTag(mTagLocation);
        if (mSPLocationPreference != null) {
            mSPLocationPreference.updateConfiguration(statusPreference);
        } else {
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_top_in, R.anim.slide_top_out);
            ft.replace(R.id.auto_slide, SPLocationPreference.newInstance(statusPreference), mTagLocation);
            ft.commit();
        }
    }

    public void showSystemLocationSettings() {
        SPSystemLocation preference = (SPSystemLocation) mFragmentManager.findFragmentByTag(mTagSystemLocation);
        boolean systemSetting = LocationUtils.isSystemLocationSettingsEnable(mActivity);
        Log.i(TAG, "showSystemLocationSettings");
        if (preference == null) {
            if (!systemSetting) {
                FragmentTransaction ft = mFragmentManager.beginTransaction();
                ft.setCustomAnimations(R.anim.slide_top_in, R.anim.slide_top_out);
                ft.replace(R.id.auto_slide, SPSystemLocation.newInstance(), mTagSystemLocation);
                ft.commit();
            }
        } else {
            if (systemSetting) {
                preference.hidePanel();
            } else {
                preference.showPanel();
            }
        }
    }

    public void showInfo(int resMainText, int resSecondText, int resColor) {
        SPInfoFragment fragment = (SPInfoFragment) mFragmentManager.findFragmentByTag(mTagInfo);
        if (fragment == null) {
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            transaction.add(R.id.auto_slide, SPInfoFragment.newInstance(resMainText, resSecondText, resColor), mTagInfo);
            transaction.setCustomAnimations(R.anim.slide_top_in, R.anim.slide_top_out);
            transaction.commit();
        } else {
            fragment.showInfoText(resMainText, resSecondText, resColor);
        }

    }

}
