
package com.appmobileos.freetoiletsrussiatrial.preferences;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class HelpActivity extends FragmentActivity {
	private TextView mLegal;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_help);
		mLegal = (TextView) findViewById(R.id.help_legal);
		mLegal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				startActivity(new Intent(HelpActivity.this,
						LegalNoticesActivity.class));

			}
		});
	}

}

