package com.appmobileos.freetoiletsrussiatrial.preferences;

public interface OnUserLogActionsListener {
	public void onUserLogActions(String visibleObjects);
	public void onUserDeleteItem(String idDeleteObject);
}
