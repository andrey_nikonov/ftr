/*
package com.appmobileos.freetoiletsrussiatrial.preferences;

*/
/**
 * The class all city from remote database and show it in dialog style.
 * If user select city the send value in CustomMapActivity and it put name city in user settings. 
 *
 * *//*


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.appmobileos.freetoiletsrussiatrial.MainInfoDialog;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import com.appmobileos.freetoiletsrussiatrial.actionbar.ActionBarActivity;
import com.appmobileos.freetoiletsrussiatrial.actionbar.ActionBarHelperBase;
import com.appmobileos.freetoiletsrussiatrial.database.DatabaseConstants;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@SuppressLint("NewApi")
public class SelectCityActivity extends ActionBarActivity implements
        OnItemClickListener {
    public static final String CITY_FROM_LIST_DATABASE = "city_from_list";
    public static final int SHOW_SUB_ACTIVITY_SELECT_CITY = 4;
    private ArrayAdapter<String> mArrayAdapter;
    private ArrayList<String> mArrayCity;
    private ProgressBar loadCity;
    private ListView mListView;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_city);
        loadCity = (ProgressBar) findViewById(R.id.load_city_pb);
        mListView = (ListView) findViewById(R.id.cities_list);
        mListView.setTextFilterEnabled(true);
        if (isOnline()) {
            new ListCityFromDatabase().execute();
        } else {
            Toast.makeText(this, R.string.notification_error_update_data,
                    Toast.LENGTH_SHORT).show();
            loadCity.setVisibility(View.GONE);
            finish();
        }

    }

    private void setupSearch() {
        mSearchView.setQueryHint(getResources().getString(
                R.string.text_hint_search_city));
        mSearchView.setOnQueryTextListener(new OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) { // unused
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    mListView.clearTextFilter();
                } else {
                    mListView.setFilterText(newText.toString());
                }
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            MenuItem menuItem = menu.findItem(R.id.menu_search);
            mSearchView = (SearchView) menuItem.getActionView();
            if (mSearchView != null)
                setupSearch();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                RepeatMethods.goHome(this);
                return true;
            case R.id.menu_search:
                ViewGroup actionBarCompat = getActionBarHelper()
                        .getActionBarCompat();
                if (actionBarCompat != null) {
                    EditText searchRequest = new EditText(this);
                    searchRequest.setHint(R.string.text_hint_search_city);
                    searchRequest.setMaxLines(1);
                    searchRequest.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void onTextChanged(CharSequence s, int start,
                                                  int before, int count) {
                            mArrayAdapter.getFilter().filter(s);

                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start,
                                                      int count, int after) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            // TODO Auto-generated method stub

                        }
                    });
                    View searchButton = getActionBarHelper()
                            .getViewFromIDActionBarCompat(
                                    ActionBarHelperBase.MENU_SEARCH_ID);
                    View titleText = getActionBarHelper()
                            .getViewFromIDActionBarCompat(
                                    ActionBarHelperBase.TITLE_TEXT_ID);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    params.weight = 1;
                    params.topMargin = (int) getResources().getDimension(
                            R.dimen.standart_small_margin);
                    params.rightMargin = (int) getResources().getDimension(
                            R.dimen.standart_margin);
                    ;
                    actionBarCompat.removeView(searchButton);
                    actionBarCompat.removeView(titleText);
                    actionBarCompat.addView(searchRequest, params);
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    */
/**
     * Check internet *
     *//*


    private boolean isOnline() {
        ConnectivityManager connectManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo infoNet = connectManager.getActiveNetworkInfo();
        return (infoNet != null && infoNet.isConnected());
    }

    */
/**
     * If connect true, return
     *//*

    private void addListItems(ArrayList<String> mArrayCity) {
        mArrayAdapter = new ArrayAdapter<String>(SelectCityActivity.this,
                android.R.layout.simple_list_item_1, mArrayCity);
        mListView.setAdapter(mArrayAdapter);
        mListView.setOnItemClickListener(SelectCityActivity.this);
        loadCity.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> adView, View target, int position,
                            long id) {
        Intent intent = new Intent();
        String textCity = ((TextView) target).getText()
                .toString();
        intent.putExtra(CITY_FROM_LIST_DATABASE, textCity);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void showInfoDialog(int idDialog) {
        DialogFragment ds = (DialogFragment) getSupportFragmentManager()
                .findFragmentByTag(MainInfoDialog.DIALOG_TAG);
        if (ds != null)
            ds.dismiss();
        MainInfoDialog info_dialog = MainInfoDialog.newInstance(idDialog, null);
        info_dialog
                .show(getSupportFragmentManager(), MainInfoDialog.DIALOG_TAG);

    }

    class ListCityFromDatabase extends AsyncTask<Void, Void, ArrayList<String>> {
        private Connection mConnection;
        private Statement mStatment;
        private ResultSet mResultSet;

        @Override
        protected ArrayList<String> doInBackground(Void... params) {
            try {
                try {
                    mConnection = ConnectionDatabaseSingleton.getInstance()
                            .getConnection(DatabaseConstants.URL_DATABASE);
                    if (mConnection != null) {
                        mStatment = mConnection.createStatement();
                        mStatment.setQueryTimeout(15);
                    }
                    if (mStatment != null)
                        mResultSet = mStatment
                                .executeQuery(DatabaseConstants.SQL_SELECT_CITY_FROM_LIST);
                    if (mResultSet != null) {
                        mArrayCity = new ArrayList<String>();
                        while (mResultSet.next()) {
                            mArrayCity.add(mResultSet
                                    .getString(DatabaseConstants.CITY));
                        }
                    }

                } finally {
                    if (mConnection != null)
                        mConnection.close();
                    if (mStatment != null)
                        mStatment.close();
                    if (mResultSet != null)
                        mResultSet.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return mArrayCity;
        }

        @Override
        protected void onPostExecute(ArrayList<String> result) {
            if (result != null) {
                addListItems(result);
            } else {
                showInfoDialog(MainInfoDialog.INTERNET_ERROR_FIRST_START);
            }

        }

    }
}
*/
