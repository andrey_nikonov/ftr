package com.appmobileos.freetoiletsrussiatrial.preferences;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import com.google.android.gms.common.GooglePlayServicesUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class LegalNoticesActivity extends FragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_legal_notices);
		TextView mTextView = (TextView) findViewById(R.id.legal);
		mTextView.setText(GooglePlayServicesUtil
				.getOpenSourceSoftwareLicenseInfo(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			RepeatMethods.goHome(this);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}

	}
}
