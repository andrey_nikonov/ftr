package com.appmobileos.freetoiletsrussiatrial.preferences;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class HelpFragment extends Fragment {
	private TextView version;
	private TextView mDescription;
	
	public static HelpFragment newInstance(){
		HelpFragment fragment = new HelpFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_help, container,
				false);
		mDescription = (TextView)rootView.findViewById(R.id.help_description);
		mDescription.setText(mDescription.getText());
		version = (TextView) rootView.findViewById(R.id.help_version);
		version.setText(getResources().getString(R.string.text_version) + " " +getVersionNumberApp());
		return rootView;
	}

	private String getVersionNumberApp(){
		String version = "1.3";
		try {
			PackageInfo info = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
			version = info.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
		
	}
		
}
