package com.appmobileos.freetoiletsrussiatrial.preferences;

import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.RepeatMethods;
import com.appmobileos.freetoiletsrussiatrial.map.GoogleMaps2Activity;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainPreferenceActivity extends FragmentActivity {
	public static final int SHOW_SUB_ACTIVITY_MAIN_PREFERENCE = 3;
	public static final int ACTIVITY_RESULT_DELETE_USER_WC = 4;
    public static final int ACTIVITY_RESULT_UPDATE_USER_WC = 5;
	private PreferenceScreen mCity;
	private Preference mLog;
	private Preference mAbout;
	private SharedPreferences mSharedPreferences;

	// private static final int PREFERENCE_SELECT_CITY = 1;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//addPreferencesFromResource(R.xml.main_preference);
		PreferenceManager.setDefaultValues(this, R.xml.main_preference, false);
		mSharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		//mCity = (PreferenceScreen) findPreference(getString(R.string.preference_key_city));
		/*mCity.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				if(RepeatMethods.isOnline(MainPreferenceActivity.this)){
					Intent intent = new Intent(MainPreferenceActivity.this,
							SelectCityActivity.class);
					mCity.setIntent(intent);
					startActivityForResult(intent,
							SelectCityActivity.SHOW_SUB_ACTIVITY_SELECT_CITY);	
				}else {
					errorConnectMessage();
				}
		
				return true;
			}
		});*/
		//mAbout = (Preference) findPreference(getString(R.string.preference_key_about));
		mAbout.setIntent(new Intent(this, HelpActivity.class));
		//mLog = (Preference) findPreference(getString(R.string.preference_key_log));
		/*mLog.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				if (RepeatMethods.isOnline(MainPreferenceActivity.this)) {
					Intent intent = new Intent(MainPreferenceActivity.this,
							UserLogActivity.class);
					mLog.setIntent(intent);
					startActivityForResult(intent,
							UserLogListFragment.SHOW_SUB_ACTIVITY_USER_LOG);

				} else {
					errorConnectMessage();
				}
				return true;
			}
		});*/

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
    private void errorConnectMessage(){
		Toast.makeText(MainPreferenceActivity.this,
				R.string.toast_error_internet_button_click,
				Toast.LENGTH_SHORT).show();
    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		titleText();

	}

	private void titleText() {
		if (mSharedPreferences == null)
			mSharedPreferences = PreferenceManager
					.getDefaultSharedPreferences(this);
		String userCity = mSharedPreferences.getString(getResources()
				.getString(R.string.preference_key_city), getResources().getString(R.string.preferences_default_city));
		mCity.setTitle(userCity);

	}

	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case SelectCityActivity.SHOW_SUB_ACTIVITY_SELECT_CITY:
			if (resultCode == Activity.RESULT_OK) {
				String newCityName = data
						.getStringExtra(SelectCityActivity.CITY_FROM_LIST_DATABASE);
				if (newCityName != null) {
					RepeatMethods.setPreference(newCityName, this);
                    RepeatMethods.setPreferenceLastUpdate(GoogleMaps2Activity.START_DATA_UPDATE_ADDRESS, this);
					// set result OK from GoogleMaps2Activity
					setResult(Activity.RESULT_OK);
				}

			}

			break;
		case UserLogListFragment.SHOW_SUB_ACTIVITY_USER_LOG:
			if (resultCode == Activity.RESULT_OK) {
                setResult(ACTIVITY_RESULT_UPDATE_USER_WC);
			}else if(resultCode== UserLogListFragment.ACTIVITY_RESULT_USER_SDELETE_WC){
                setResult(ACTIVITY_RESULT_DELETE_USER_WC);
            }
			break;
		default:
			break;
		}
	}*/

	

}
