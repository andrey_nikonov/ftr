/*
package com.appmobileos.freetoiletsrussiatrial.preferences;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.appmobileos.freetoiletsrussiatrial.MainInfoDialog;
import com.appmobileos.freetoiletsrussiatrial.R;
import com.appmobileos.freetoiletsrussiatrial.database.DatabaseConstants;
import com.appmobileos.freetoiletsrussiatrial.database.LocalDatabaseContentProvider;
import com.appmobileos.freetoiletsrussiatrial.database.LocalDatabaseContentProvider.CustomSQLHelper;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class UserLogListFragment extends ListFragment {
	private OnUserLogActionsListener mListener;
	private final String ATTRIBUTE_ADDRESS = "address";
	private final String ATTRIBUTE_STATUS = "status";
	private final String ATTRIBUTE_STATUS_TEXT = "status_text";
	private final String ATTRIBUTE_ADMIN_COMMENT = "admin_comment";
	private final String ATTRIBUTE_ID_MYSQL = "id_my_sql";
	public static final String ALL_USER_OBJECTS = "all_user_objects";
	public static final String MODERATION_USER_OBJECTS = "moderation_user_object";
	public static final String KEY_VISIBLE_OBJECTS = "key_visible_objects";
	public static final int SHOW_SUB_ACTIVITY_USER_LOG = 0;
    public static final int ACTIVITY_RESULT_USER_SDELETE_WC = 1;
	private final int CONTEXT_MENU_DELETE = 1;
	private Map<String, Object> dataItem = null;
	private ArrayList<Map<String, Object>> data;
	private CustomAdapter adapter;
	private final String[] from = { ATTRIBUTE_ADDRESS, ATTRIBUTE_STATUS,
			ATTRIBUTE_STATUS_TEXT, ATTRIBUTE_ADMIN_COMMENT, ATTRIBUTE_ID_MYSQL };
	private final int[] to = { R.id.log_address, R.id.log_color_status,
			R.id.log_status, R.id.log_admin_message, R.id.log_id_mysql };
	private final int positive = R.drawable.green_circle;
	private final int negative = R.drawable.red_circle;
	private final int neutral = R.drawable.orange_circle;
	private ProgressBar mProgress;
	private TextView emptyText;

	public static UserLogListFragment newInstance(String visibleLogInfo) {
		UserLogListFragment fragment = new UserLogListFragment();
		Bundle args = new Bundle();
		args.putString(KEY_VISIBLE_OBJECTS, visibleLogInfo);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		TextView status = (TextView) v.findViewById(R.id.log_status);
		String admin_message = null;
		int idDialog = convertStatus(status.getText().toString().trim());
		TextView admin_messageTv = (TextView) v
				.findViewById(R.id.log_admin_message);

		admin_message = admin_messageTv.getText().toString();
		MainInfoDialog.newInstance(idDialog, admin_message).show(
				getFragmentManager(), MainInfoDialog.DIALOG_TAG);
	}

	private int convertStatus(String status) {
		int statusInt = 0;
		if (status.equals(getString(R.string.dialog_title_moderation))) {
			statusInt = MainInfoDialog.LOG_WC_MODERATION;
		} else if (status.equals(getString(R.string.preference_log_approved))) {
			statusInt = MainInfoDialog.LOG_WC_APPROVEL;
		} else if (status.equals(getString(R.string.dialog_title_rejected))) {
			statusInt = MainInfoDialog.LOG_WC_REJECT;
		}
		return statusInt;

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnUserLogActionsListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnArticleSelectedListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_log, container, false);
		mProgress = (ProgressBar) view.findViewById(R.id.activity_help_pb);
		emptyText = (TextView) view.findViewById(android.R.id.empty);
		return view;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(1, CONTEXT_MENU_DELETE, 1, R.string.dialog_delete);

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		switch (item.getItemId()) {
		case CONTEXT_MENU_DELETE:
			TextView text = (TextView) info.targetView
					.findViewById(R.id.log_id_mysql);
			String id = text.getText().toString();
			new DeleteItem().execute(id);
			// getActivity().setResult(Activity.RESULT_OK);
			return true;

		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

	class DeleteItem extends AsyncTask<String, Void, Integer> {
		private Connection mConnection;
		private PreparedStatement mPreparedStatement;
		private String idDeleteItem = null;

		@Override
		protected Integer doInBackground(String... params) {
			int result = 0;
			try {
				try {
					mConnection = ConnectionDatabaseSingleton.getInstance()
							.getConnection(DatabaseConstants.URL_DATABASE_TEMP);
					mPreparedStatement = mConnection
							.prepareStatement(DatabaseConstants.DELETE_USER_WC);
					idDeleteItem = params[0];
					mPreparedStatement.setString(1, idDeleteItem);
					Activity activity = getActivity();
					if (getActivity() != null) {
						result = mPreparedStatement.executeUpdate();
						if (result > 0)
							activity.getContentResolver()
									.delete(LocalDatabaseContentProvider.CONTENT_URI_ADDRESS_USER,
											CustomSQLHelper.KEY_REMOTE_ID + "="
													+ idDeleteItem, null);
						mListener.onUserDeleteItem(idDeleteItem);
					}
				} finally {
					if (mConnection != null)
						mConnection.close();
					if (mPreparedStatement != null)
						mPreparedStatement.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result > 0) {
				if (getActivity() != null) {
					adapter.notifyDataSetChanged();
					mListener.onUserLogActions(getArguments().getString(
							KEY_VISIBLE_OBJECTS));

				}

			} else {
				if (getActivity() != null)
					Toast.makeText(getActivity(),
							R.string.notification_error_update_data,
							Toast.LENGTH_SHORT).show();
			}

		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		registerForContextMenu(getListView());
		if (getActivity() != null) {
			new LoadUseLog().execute(Identificator.id(getActivity()));

		}

	}

	public class LoadUseLog extends
			AsyncTask<String, Void, ArrayList<Map<String, Object>>> {
		private Connection mConnection;
		private PreparedStatement mPreparedStatment;
		private ResultSet mResultSet = null;
		private int status = 0; // 0 - moderation, 1 - positive , 2 - negative
		private int drawable = 0;
		private String statusText = "неопределенно";
		private String admin_comment;
		private String description;
		private Long id_mysql;
		private String status_moderation;
		private String status_reject;
		private String status_approvl;

		@Override
		protected void onPreExecute() {
			emptyText.setText("");
			mProgress.setVisibility(View.VISIBLE);
			if (getActivity() != null) {
				status_moderation = getActivity().getResources().getString(
						R.string.dialog_title_moderation);
				status_approvl = getActivity().getResources().getString(
						R.string.preference_log_approved);
				status_reject = getActivity().getResources().getString(
						R.string.dialog_title_rejected);
			}

		}

		@Override
		protected ArrayList<Map<String, Object>> doInBackground(
				String... params) {
			try {
				try {
					mConnection = ConnectionDatabaseSingleton.getInstance()
							.getConnection(DatabaseConstants.URL_DATABASE_TEMP);
					if (getArguments().getString(KEY_VISIBLE_OBJECTS) == ALL_USER_OBJECTS) {
						mPreparedStatment = mConnection
								.prepareStatement(DatabaseConstants.SQL_SELECT_TEMP_USER_WC_LOG);
					} else {
						mPreparedStatment = mConnection
								.prepareStatement(DatabaseConstants.SQL_SELECT_TEMP_USER_WC_LOG_MODERATION);
					}
					mPreparedStatment.setString(1, params[0]);
					mResultSet = mPreparedStatment.executeQuery();
					data = new ArrayList<Map<String, Object>>();
					while (mResultSet.next()) {
						dataItem = new HashMap<String, Object>();
						description = mResultSet
								.getString(DatabaseConstants.DESCRIPTION);
						if (TextUtils.isEmpty(description)) {
							dataItem.put(ATTRIBUTE_ADDRESS, mResultSet
									.getString(DatabaseConstants.OBJECT));
						} else {
							dataItem.put(ATTRIBUTE_ADDRESS, description);
						}

						status = mResultSet.getInt(DatabaseConstants.STATUS);
						admin_comment = mResultSet
								.getString(DatabaseConstants.ADMIN_COMMENT);
						dataItem.put(ATTRIBUTE_ADMIN_COMMENT, admin_comment);
						id_mysql = mResultSet
								.getLong(DatabaseConstants.AUTO_ID);
						dataItem.put(ATTRIBUTE_ID_MYSQL, id_mysql);
						if (status == DatabaseConstants.USER_OBJECT_STATUS_MODERATION) {
							drawable = neutral;
							statusText = status_moderation;
						} else if (status == DatabaseConstants.USER_OBJECT_STATUS_APPROVL) {
							drawable = positive;
							statusText = status_approvl;

						} else if (status == DatabaseConstants.USER_OBJECT_STATUS_REJECT) {
							drawable = negative;
							statusText = status_reject;
						}
						if (getActivity() != null
								&& (status == 1 || status == 2)) {
                            getActivity().setResult(Activity.RESULT_OK);
							ContentValues values = new ContentValues();
							values.put(CustomSQLHelper.KEY_STATUS_USER_OBJECT,
									status);
							String whereOpenation = CustomSQLHelper.KEY_REMOTE_ID
									+ "=" + id_mysql;
							getActivity()
									.getContentResolver()
									.update(LocalDatabaseContentProvider.CONTENT_URI_ADDRESS_USER,
											values, whereOpenation, null);
						}

						dataItem.put(ATTRIBUTE_STATUS, drawable);
						dataItem.put(ATTRIBUTE_STATUS_TEXT, statusText);
						data.add(dataItem);

					}
				} finally {
					if (mConnection != null)
						mConnection.close();
					if (mPreparedStatment != null)
						mPreparedStatment.close();
					if (mResultSet != null)
						mResultSet.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			return data;
		}

		@Override
		protected void onPostExecute(ArrayList<Map<String, Object>> result) {
			if (getActivity() != null && result != null) {
				adapter = new CustomAdapter(getActivity(), result,
						R.layout.list_view_user_log, from, to);
				setListAdapter(adapter);

				if (result.size() == 0) {
					emptyText.setText(R.string.preference_log_empty);
				}
			} else {
				emptyText.setText(R.string.dialog_title_error_connect);
			}
			mProgress.setVisibility(View.GONE);
		}
	}

	private class CustomAdapter extends SimpleAdapter {
		private ArrayList<DataSetObserver> observers = new ArrayList<DataSetObserver>();

		public CustomAdapter(Context context,
				List<? extends Map<String, ?>> data, int resource,
				String[] from, int[] to) {
			super(context, data, resource, from, to);
		}

		@Override
		public void setViewImage(ImageView v, int value) {
			super.setViewImage(v, value);
			if (value == neutral) {
				v.setImageResource(neutral);
			} else if (value == positive) {
				v.setImageResource(positive);
			} else if (value == negative) {
				v.setImageResource(negative);
			}
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			observers.add(observer);
		}

		@Override
		public void notifyDataSetChanged() {
			for (DataSetObserver observer : observers) {
				observer.onChanged();
			}
		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {
			observers.remove(observer);
		}

	}

}
*/
