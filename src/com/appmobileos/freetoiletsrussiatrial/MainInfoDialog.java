package com.appmobileos.freetoiletsrussiatrial;

import com.appmobileos.freetoiletsrussiatrial.map.DefinitionCityUserSingleton;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class MainInfoDialog extends DialogFragment implements OnClickListener {
	public static final int FIND_CITY = 0;
	public static final int NO_FIND_CITY = 1;
	public static final int LOG_WC_APPROVEL = 2;
	public static final int LOG_WC_MODERATION = 3;
	public static final int LOG_WC_REJECT = 4;
	public static final int DATABASE_ERROR_CONNECT = 5;
	public static final int DATABASE_NO_RESULT = 6;
	public static final int PREFERENCE_NO_CITY = 7;
	public static final int FIRST_START_NO_CONNECT = 8;
	public static final String KEY_MESSAGE = "key_message";
	public static final String KEY_DINAMIC_MESSAGE = "key_dinamic_message";
	public static final String DIALOG_TAG = "MainInfoDialog";
	public static final int SHOW_SUB_ACTIVITY_SELECT_CITY = 8;
	public static final int CANSEL_SELECT_CITY = 9;
	public static final int INTERNET_ERROR_FIRST_START = 10;
	public static final int ADDRESS_NO_FROM_CITY = 11;

	public static MainInfoDialog newInstance(int idDialog, String dinamicMessage) {
		MainInfoDialog info = new MainInfoDialog();
		Bundle args = new Bundle();
		args.putInt(KEY_MESSAGE, idDialog);
		if (dinamicMessage != null)
			args.putString(KEY_DINAMIC_MESSAGE, dinamicMessage);
		info.setArguments(args);
		return info;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		int key = getArguments().getInt(KEY_MESSAGE);
		if (key == ADDRESS_NO_FROM_CITY) {
			dialog.setTitle(selectTitleMessage(key));
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View rootView = inflater.inflate(R.layout.dialog_no_address, null);
			dialog.setView(rootView);
			CheckBox checkView = (CheckBox) rootView
					.findViewById(R.id.dialog_no_address_checkbox);
			checkView.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					if (isChecked) {
						RepeatMethods.setHideNoAddressHite(true, getActivity());
					} else {
						RepeatMethods
								.setHideNoAddressHite(false, getActivity());
					}

				}
			});
		} else {
			dialog.setTitle(selectTitleMessage(key));
			dialog.setMessage(selectMessage(key));
		}
		dialog.setPositiveButton(selectButtonTextPositive(key), this);
		if (key == FIND_CITY || key == ADDRESS_NO_FROM_CITY) {
			dialog.setNegativeButton(selectButtonTextNegative(key), this);
		}
		return dialog.create();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			actionsButtonPositive(selectButtonTextPositive(getArguments()
					.getInt(KEY_MESSAGE)));
		} else if (which == DialogInterface.BUTTON_NEGATIVE) {
			actionsButtonNegative(selectButtonTextNegative(getArguments()
					.getInt(KEY_MESSAGE)));
		}
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		getActivity().finish();
	}

	private String selectTitleMessage(int idDialog) {
		String titleMessage = getString(R.string.dialog_title_info);
		if (idDialog == DATABASE_ERROR_CONNECT) {
			titleMessage = getString(R.string.dialog_title_error_connect_db);
		} else if (idDialog == INTERNET_ERROR_FIRST_START) {
			titleMessage = getString(R.string.dialog_title_error_connect);
		} else if (idDialog == LOG_WC_MODERATION) {
			titleMessage = getString(R.string.dialog_title_moderation);
		} else if (idDialog == LOG_WC_APPROVEL) {
			titleMessage = getString(R.string.dialog_title_approved);
		} else if (idDialog == LOG_WC_REJECT) {
			titleMessage = getString(R.string.dialog_title_rejected);
		}

		return titleMessage;
	}

	private String selectMessage(int idDialog) {
		String finalText = "no data";
		if (idDialog == LOG_WC_MODERATION) {
			finalText = getResources().getString(
					R.string.dialog_message_moderation);
		} else if (idDialog == LOG_WC_APPROVEL) {
			finalText = getResources().getString(
					R.string.dialog_message_approvel);
		} else if (idDialog == LOG_WC_REJECT) {
			String dinamicMessage = getArguments().getString(
					KEY_DINAMIC_MESSAGE);
			if (dinamicMessage != null) {
				finalText = dinamicMessage;
			}

		} else if (idDialog == DATABASE_ERROR_CONNECT) {
			finalText = getString(R.string.dialog_message_error_connect_db);
		} else if (idDialog == DATABASE_NO_RESULT) {
			finalText = getString(R.string.dialog_message_no_result);

		} else if (idDialog == FIND_CITY) {
			finalText = getString(R.string.dialog_title_select_city) + " "
					+ getArguments().getString(KEY_DINAMIC_MESSAGE);

		} else if (idDialog == NO_FIND_CITY) {
			finalText = getString(R.string.dialog_message_no_city);
		} else if (idDialog == CANSEL_SELECT_CITY) {
			finalText = getString(R.string.dialog_message_no_city_from_list);
		} else if (idDialog == INTERNET_ERROR_FIRST_START) {
			finalText = getString(R.string.dialog_message_error_network_first_start);
		} else if (idDialog == FIRST_START_NO_CONNECT) {
			finalText = getString(R.string.dialog_message_no_connect_first_start);
		}
		return finalText;
	}

	private String selectButtonTextPositive(int idDialog) {
		String buttonText = getString(android.R.string.yes);
		if (idDialog == DATABASE_ERROR_CONNECT) {
			buttonText = getString(R.string.dialog_button_repeat);
		} else if (idDialog == DATABASE_NO_RESULT || idDialog == NO_FIND_CITY
				|| idDialog == ADDRESS_NO_FROM_CITY) {
			buttonText = getString(R.string.dialog_choose);

		} else if (idDialog == FIND_CITY) {
			buttonText = getString(R.string.dialog_true);

		} else if (idDialog == INTERNET_ERROR_FIRST_START
				|| idDialog == FIRST_START_NO_CONNECT) {
			buttonText = getString(R.string.dialog_close);

		}
		return buttonText;
	}

	private String selectButtonTextNegative(int idDialog) {
		String negativeButtonText = " ";
		if (idDialog == FIND_CITY) {
			negativeButtonText = getString(R.string.dialog_choose);
		} else if (idDialog == ADDRESS_NO_FROM_CITY) {
			negativeButtonText = getResources().getString(R.string.dialog_skip);
		}
		return negativeButtonText;
	}

	private void actionsButtonPositive(String buttonText) {
		dismiss();
		if (buttonText.equals(getString(android.R.string.yes))) {

		} else if (buttonText.equals(getString(R.string.dialog_button_repeat))) {
			//((GoogleMaps2Activity) getActivity()).startLoadingObjects();

		} else if (buttonText.equals(getString(R.string.dialog_true))) {
			RepeatMethods.setPreference(
					getArguments().getString(KEY_DINAMIC_MESSAGE),
					getActivity());
			//((GoogleMaps2Activity) getActivity()).startLoadingObjects();

		} else if (buttonText.equals(getString(R.string.dialog_choose))) {
			//startActivityCustom();

		} else if (buttonText.equals(getString(R.string.dialog_close))) {
			getActivity().finish();
		}
	}

/*	private void startActivityCustom() {
		DefinitionCityUserSingleton.getInstance().setVisibleDialog(false);
		getActivity().startActivityForResult(
				new Intent(getActivity(), SelectCityActivity.class),
				SHOW_SUB_ACTIVITY_SELECT_CITY);
	}*/

	private void actionsButtonNegative(String buttonText) {
		dismiss();
		if (buttonText.equals(getString(R.string.dialog_choose))) {
			//startActivityCustom();

		}
	}

}
